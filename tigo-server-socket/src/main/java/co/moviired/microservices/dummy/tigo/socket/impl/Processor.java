package co.moviired.microservices.dummy.tigo.socket.impl;

import co.moviired.microservices.dummy.tigo.service.ITigoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;

public class Processor implements Runnable {
    private final Logger LOGGER = LogManager.getLogger((Processor.class.getName()));

    private ITigoService tigoService;
    private Socket socket;


    public Processor(Socket socket, ITigoService tigoService) {
        this.socket = socket;
        this.tigoService = tigoService;
    }

    public void run() {
        BufferedReader input = null;
        OutputStream output;
        String responseString = "";
        try {
            // 1 - Se obtiene el mensaje del SOCKET
            input = new BufferedReader(new java.io.InputStreamReader(socket.getInputStream()));
            char[] in = new char['Ĭ'];
            input.read(in);
            output = socket.getOutputStream();
            String request = new String(in).trim();
            LOGGER.info("request => "+request);
            String response = tigoService.processBuyPackage(request);
            LOGGER.info("response => "+response);
            output.write(response.getBytes());

            /*
            if (!request.trim().equals("")) {
                String[] requestArray = response.split("\\|");

                for (int i=0; i< requestArray.length; i++){
                    output.write(requestArray[i].getBytes());
                }
                LOGGER.info("Transaccion normal");

                // 6- Escribe repsuesta en el SOCKET
               // output.write(responseString.getBytes());
                //output.write("ESTA ES UNA RESPUEST SERIA".getBytes());
            }
            */

        } catch (Exception ex) {
            //ex.printStackTrace();
            LOGGER.error("Error Gateway Exito -Error de conexión con el socket Cliente(IOException): " + ex);
            LOGGER.error(ex);
        } finally {
            try {
                if (input != null)
                    input.close();
                if (socket != null)
                    socket.close();
            } catch (IOException ex) {
                LOGGER.error("Error Gateway Exito -Error de conexión con el Socket Cliente(IOException): " + ex);
            }
        }
    }
}

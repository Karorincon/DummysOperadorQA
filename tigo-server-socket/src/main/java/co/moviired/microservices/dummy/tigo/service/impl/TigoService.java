package co.moviired.microservices.dummy.tigo.service.impl;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.entities.Subscriber;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.conexion.repository.ISubscriberRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import co.moviired.microservices.dummy.tigo.service.ITigoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class TigoService implements ITigoService{

    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private ISubscriberRepository subscriberRepository;
    @Autowired
    private IErrorCodeRepository errorCodeRepository;

    private static final Logger LOGGER = LogManager.getLogger(TigoService.class.getName());

    @Override
    public String processBuyPackage(String message) throws Exception {
        try {


            ErrorTypeEnum errorTypeEnum = null;
            LOGGER.info("trama envidada => " + message);

            String[] parts = message.split("\\|");

            try {
                /**
                 * Validamos que el campo cuente con la cantidad
                 */
                if (parts[0] == null)
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_AMOUNT);


                Subscriber subscriber = this.subscriberRepository
                        .findByThirdPartyIdAndMsisdn(ThirdPartyEnum.TIGO.getId(), parts[5]);

                LOGGER.info("-----------------------------CODIGO PRODUCTO: "+parts[8]);
                
                Product product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.TIGO.getId(), parts[8]);
                
                if (product == null)
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_PRODUCT);
                
                /**
                 * Esta validación indica que si no se encontró subscriber, pero el 
                 * producto tiene uno configurado entonces se lanza una excepción
                 */
                if ((subscriber == null && product.getSubscriberId()!=null) 
                		|| (subscriber!=null && !subscriber.getId().equals(product.getSubscriberId())))
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_ACCOUNT);
                
                LOGGER.info("ThirdPartyEnum.TIGO.getId()= " + ThirdPartyEnum.TIGO.getId());
                LOGGER.info("product= " + product);

                Double amount = new Double(parts[0]);

                if (product.getMaxValue() < amount)
                    throw new ErrorTypeException(ErrorTypeEnum.GREATER_MAXIMUM_VALUE);

                if (product.getMinValue() > amount)
                    throw new ErrorTypeException(ErrorTypeEnum.LOWER_MINOR_VALUE);


                if (parts[1] == null || parts[1].isEmpty())
                    throw new ErrorTypeException(ErrorTypeEnum.EMPTY_MSISDN);


                if (product.getRegex() != null && !product.getRegex().isEmpty()
                        && !Pattern.compile(product.getRegex()).matcher(parts[1]).matches())
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_MSISDN);


                if (subscriber!=null && subscriber.getTotalNet() < amount)
                    throw new ErrorTypeException(ErrorTypeEnum.NO_CREDIT);

                //this.subscriberRepository.updateTotalNet(subscriber.getTotalNet() - amount, subscriber.getId());

            } catch (ErrorTypeException ex) {
                LOGGER.info("--error1");
                LOGGER.error("ErrorTypeException: " + ex.getMessage());
                errorTypeEnum = ex.getErrorTypeEnum();

            } catch (Exception ex) {
                LOGGER.info("--error2");
                LOGGER.error("Exception : " + ex.getMessage());
                errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;
            }
            LOGGER.info("------------hwa-------------");

            if (errorTypeEnum == null)
                errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;

            ErrorCode error = this.errorCodeRepository
                    .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.TIGO.getId(),
                            errorTypeEnum.getId());

            LOGGER.info(error.getMessage());

            return error.getCode() + "|" + "12334343";


        }catch (Exception ex){
            LOGGER.error(ex);
            return  "99|12334343";
        }
    }


}

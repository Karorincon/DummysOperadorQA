package co.moviired.microservices.dummy.tigo.socket;

public interface ITigoSocket {
    void startSocket();
}

package co.moviired.microservices.dummy.tigo.socket.impl;

import co.moviired.microservices.dummy.tigo.configuration.WsConfiguration;
import co.moviired.microservices.dummy.tigo.service.ITigoService;
import co.moviired.microservices.dummy.tigo.service.impl.TigoService;
import co.moviired.microservices.dummy.tigo.socket.ITigoSocket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
public class TigoSocket {

    private final Logger LOGGER = LogManager.getLogger((TigoSocket.class.getName()));

    private static Executor threadPool;
    private static ServerSocket ss;

    private final ITigoService tigoService;
    private final WsConfiguration wsConfiguration;

    @Autowired
    public TigoSocket(ITigoService service,WsConfiguration configuration){
        this.tigoService = service;
        this.wsConfiguration= configuration;
        this.startSocket();
    }


        private void startSocket() {
            try {
                LOGGER.info("TIGO - SOCKET: iniciando por el puerto:" + this.wsConfiguration.getLOCAL_PORT());
                ss = new ServerSocket(this.wsConfiguration.getLOCAL_PORT());
                threadPool = Executors.newFixedThreadPool(this.wsConfiguration.getPROCESSORS());
                LOGGER.info("TIGO - SOCKET: Iniciado correctamente.");
            } catch (IOException ex) {
                LOGGER.fatal("ECxIOE01 -Error al abrir el puerto ", ex);
                System.err.println("[" + new Date().toString() + "] - ECxIOE01 - Error al abrir el puerto,Consulte el log para mayor información ");
                System.exit(-1);
            }
            while (true) {
                Socket s = null;
                try {
                    s = ss.accept();
                    LOGGER.info("---> Llego trama al socket ");
                    threadPool.execute(new Processor(s, tigoService));
                } catch (IOException ex) {
                    LOGGER.fatal("ECxIOE01 -Error al abrir el puerto ", ex);
                    System.err.println("[" + new Date().toString() + "] - ECxIOE01 -Error al abrir el puerto,Consulte el log para mayor información ");
                    try {
                        if (s != null)
                            s.close();
                    } catch (NullPointerException | IOException ex1) {
                        LOGGER.fatal("ECxIOE02 -Error Cerrando el socket ", ex1);
                        System.err.println("[" + new Date().toString() + "] - ECxIOE02 -Error Cerrando el socket,Consulte el log para mayor información ");
                    }
                } catch (NullPointerException ex) {
                    LOGGER.fatal("ECxNPE01 -Error obteniendo recursos para nueva conexión ", ex);
                    System.err.println("[" + new Date().toString() + "] - ECxNPE01 - Error obteniendo recursos para neuva conexión,Consulte el log para mayor información ");
                    try {
                        if (s != null)
                            s.close();
                    } catch (NullPointerException | IOException ex1) {
                        LOGGER.fatal("ECxIOE02 -Error Cerrando el socket ", ex1);
                        System.err.println("[" + new Date().toString() + "] - ECxIOE02 - Error Cerrando el socket,Consulte el log para mayor información ");
                    }
                } catch (Exception ex) {
                    LOGGER.fatal("ECxE01 -Error procesando petición ", ex);
                    System.err.println("[" + new Date().toString() + "] - ECxE01 - Error procesando petición,Consulte el log para mayor información ");
                    try {
                        if (s != null)
                            s.close();
                    } catch (NullPointerException | IOException ex1) {
                        LOGGER.fatal("ECxIOE02 -Error Cerrando el socket ", ex1);
                        System.err.println("[" + new Date().toString() + "] - ECxIOE02 - Error Cerrando el socket,Consulte el log para mayor información ");
                    }
                }
            }
        }
}



package co.moviired.microservices.dummy.tigo.service;

public interface ITigoService {

    String processBuyPackage(String message) throws Exception;

}

package co.moviired.microservices.dummy.tigo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "file:${configuration_external_path}/participant.properties", ignoreResourceNotFound = true)
public class WsConfiguration {
    @Value("${LOCAL_PORT}")
    private int LOCAL_PORT;

    @Value("${PROCESSORS}")
    private  int PROCESSORS;

    public int getLOCAL_PORT() {
        return LOCAL_PORT;
    }

    public int getPROCESSORS() {
        return PROCESSORS;
    }
}

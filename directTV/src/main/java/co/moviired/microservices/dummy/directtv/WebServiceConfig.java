package co.moviired.microservices.dummy.directtv;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;

import java.io.File;

@EnableWs
@Configuration
@PropertySource(value = "file:${configuration_external_path}/participant.properties", ignoreResourceNotFound = true)
public class WebServiceConfig extends WsConfigurerAdapter {

    @Value("${configuration_external_path}")
    private String configurationExternalPath;

    @Value("${SERVICE_NAME}")
    private String contextPath;

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/"+this.contextPath+"/*");
    }

    @Bean(name = "wsPrepago")
    public Wsdl11Definition defaultWsdl11Definition(){
        SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
        File f= new File(this.configurationExternalPath+"/wsdl/wsPrepago.WSDL");
        wsdl11Definition.setWsdl(new FileSystemResource(f));

        return wsdl11Definition;
    }




}
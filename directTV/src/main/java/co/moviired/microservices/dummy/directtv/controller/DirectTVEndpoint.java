package co.moviired.microservices.dummy.directtv.controller;

import co.moviired.microservices.dummy.directtv.service.IDirectTVService;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.tempuri.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;

@Endpoint
public class DirectTVEndpoint {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(DirectTVEndpoint.class);
    private IDirectTVService directTVService;
    private static final String TARGET_NAMESPACE ="http://tempuri.org/";

    @Autowired()
    public DirectTVEndpoint(IDirectTVService directTVService) {
        this.directTVService = directTVService;
    }

    													  
    @PayloadRoot(namespace = TARGET_NAMESPACE, localPart = "SolicitudRecarga")
    @ResponsePayload
    public  JAXBElement<SolicitudRecargaResponse> solicitudRecarga(@RequestPayload  JAXBElement<SolicitudRecarga> request) throws Exception{
        ObjectFactory factory = new ObjectFactory();
        SolicitudRecargaResponse response = factory.createSolicitudRecargaResponse();
        SolicitudRecargaResponse.SolicitudRecargaResult result=null;
        LOGGER.info("request => "+request.getValue().getStrSerialSC());

        try{

            result = this.directTVService.solicitudRecarga(request.getValue());

        }catch (Exception ex){
            result = factory.createSolicitudRecargaResponseSolicitudRecargaResult();
            LOGGER.error("error = > ", ex);
        }

        response.setSolicitudRecargaResult(result);
        
        
        JAXBElement<SolicitudRecargaResponse> jaxbElement =  new JAXBElement( 
                new QName(TARGET_NAMESPACE,SolicitudRecargaResponse.class.getSimpleName()), SolicitudRecargaResponse.class, response);

        
        return jaxbElement;
      
        
        //return response;
        //return responseToXml(response);
    }

    @PayloadRoot(namespace = TARGET_NAMESPACE, localPart = "SolicitudReverso")
    @ResponsePayload
    public JAXBElement<SolicitudReversoResponse> solicitudReverso(@RequestPayload SolicitudReverso request) throws Exception {
        ObjectFactory factory = new ObjectFactory();
        SolicitudReversoResponse response = factory.createSolicitudReversoResponse();
        SolicitudReversoResponse.SolicitudReversoResult result=null;
        LOGGER.info("request => "+request.getStrSerialSC());

        try{

            result = this.directTVService.solicitudReverso(request);

        }catch (Exception ex){
            result = factory.createSolicitudReversoResponseSolicitudReversoResult();
            LOGGER.error("error = > ", ex);
        }

        response.setSolicitudReversoResult(result);
        
        JAXBElement<SolicitudReversoResponse> jaxbElement =  new JAXBElement( 
                new QName(SolicitudReversoResponse.class.getSimpleName()), SolicitudReversoResponse.class, response);
        
        return jaxbElement;

    }

    /**
     * @param response Elemento XML <BooksInfoRequest ...>
     * @return Genera una instancia de BooksInfoRequest a partir de un elemento xml
     */
    private Element responseToXml(SolicitudRecargaResponse response) {
    	try {
    		
    		LOGGER.info("response= "+response);
    		LOGGER.info("response.getClass()= "+response.getClass());
    		JAXBContext jc = JAXBContext.newInstance(org.tempuri.SolicitudRecargaResponse.class);
    		Marshaller marshaller2 = jc.createMarshaller();
    		LOGGER.info("jc= "+jc);
    		
    		StringWriter sw = new StringWriter();
    		marshaller2.marshal(response, sw);
    		String xmlString = sw.toString();
    		
    		LOGGER.info("xmlString= "+xmlString);

    	    Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    	    Marshaller marshaller = jc.createMarshaller();
    	    marshaller.marshal(response, document);
    	    
    	    LOGGER.info("document.getDocumentElement() => ");
    	    LOGGER.info(document.getDocumentElement());
    	    LOGGER.info("=======================================");

        	return document.getDocumentElement();
    	}catch (Exception e) {
    		LOGGER.error("Ha ocurrido un error=> ",e);
		}
    	
    	LOGGER.info("====================ni modo null============");
    	return null;
	 	
    	
    	/*
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();
        Element	root	= document.createElementNS(TARGET_NAMESPACE, "SolicitudRecargaResponse");
        return root;
        */
    }
    
    /**
     * @param response Elemento XML <BooksInfoRequest ...>
     * @return Genera una instancia de BooksInfoRequest a partir de un elemento xml
     */
    private Element responseToXml2(SolicitudReversoResponse response) {
    	try {
    		
    		LOGGER.info("response= "+response);
    		LOGGER.info("response.getClass()= "+response.getClass());
    		JAXBContext jc = JAXBContext.newInstance(SolicitudReversoResponse.class);
    		LOGGER.info("jc= "+jc);

    	    Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    	    Marshaller marshaller = jc.createMarshaller();
    	    marshaller.marshal(response, document);
    	    
    	    LOGGER.info("document.getDocumentElement() => ");
    	    LOGGER.info(document.getDocumentElement());
    	    LOGGER.info("=======================================");

        	return document.getDocumentElement();
    	}catch (Exception e) {
    		LOGGER.error("Ha ocurrido un error=> ",e);
		}
    	
    	LOGGER.info("====================ni modo null============");
    	return null;
	 	
    	
    	/*
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();
        Element	root	= document.createElementNS(TARGET_NAMESPACE, "SolicitudRecargaResponse");
        return root;
        */
    }


    /*
    private Element bookToXml(Document document, Libro libro){
        Element domLibro = document.createElement("libro");

        this.addHijo(document, domLibro, "editorial", libro.getEditorial());
        this.addHijo(document, domLibro, "titulo",	libro.getTitulo());
        this.addHijo(document, domLibro, "paginas",	String.valueOf(libro.getPaginas()));
        this.addHijo(document, domLibro, "precio",	String.valueOf(libro.getPrecio()));

        return domLibro;
    }

    /
    private void addHijo(Document document, Element padre, String tag, String valor){
        Element domNombre = document.createElement(tag);
        Text	domValor  = document.createTextNode(valor);

        domNombre.appendChild(domValor);
        padre.appendChild(domNombre);
    }
    */

}
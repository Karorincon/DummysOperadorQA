package co.moviired.microservices.dummy.directtv.service.impl;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import co.moviired.microservices.dummy.directtv.service.IDirectTVService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tempuri.ObjectFactory;
import org.tempuri.SolicitudRecarga;
import org.tempuri.SolicitudRecargaResponse;
import org.tempuri.SolicitudRecargaResponse.SolicitudRecargaResult;
import org.tempuri.SolicitudReverso;
import org.tempuri.SolicitudReversoResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

@Service
public class DirectTVService implements IDirectTVService{ 

    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IErrorCodeRepository errorCodeRepository;

    private static final Logger LOGGER = LogManager.getLogger(DirectTVService.class.getName());


    public SolicitudRecargaResult solicitudRecarga(SolicitudRecarga request)throws  Exception {
        return this.validateParams(request);
    }

    @Override
    public SolicitudReversoResponse.SolicitudReversoResult solicitudReverso(SolicitudReverso reques) throws  Exception {
        ObjectFactory factory = new ObjectFactory();
        SolicitudReversoResponse.SolicitudReversoResult result =factory.createSolicitudReversoResponseSolicitudReversoResult();

        ErrorCode error = this.errorCodeRepository
                .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.DIRECT_TV.getId(), ErrorTypeEnum.SUCCESSFULL.getId());

                		
                		
		 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	     DocumentBuilder db = dbf.newDocumentBuilder();
	     Document document = db.newDocument();
	     
	
	     Element recharge = document.createElement("any");
	     document.appendChild(recharge);
	    
	     Element element = document.createElement("dsSolicitudReversos");
	     recharge.appendChild(element); 
	     
	     Element elements = document.createElement("elements");
         element.appendChild(elements);
         
         Element codeTransaction = document.createElement("codeTransaction");
         codeTransaction.appendChild(document.createTextNode(this.getAutorizationNumber()));
         elements.appendChild(codeTransaction);
        
         
         Element codeResult = document.createElement("codeResult");
         codeResult.appendChild(document.createTextNode(error.getCode()));
         elements.appendChild(codeResult);
        
         
         Element comments = document.createElement("comments");
         comments.appendChild(document.createTextNode(error.getMessage()));
         elements.appendChild(comments);
         
         
        result.setAny(recharge);
      
        return result;
    }

    private SolicitudRecargaResult validateParams(SolicitudRecarga request) throws  Exception{
        ErrorTypeEnum errorTypeEnum=null;

        try{

            /**
             * Validamos que el campo cuente con la cantidad
             */
            if(request.getIntValorRecarga()==0)
                throw  new ErrorTypeException(ErrorTypeEnum.INVALID_AMOUNT);


            Product product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.DIRECT_TV.getId(),"1");

            if(product==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_PRODUCT);


            if (request.getIntValorRecarga()<product.getMinValue())
                throw  new ErrorTypeException (ErrorTypeEnum.LOWER_MINOR_VALUE);

            if (request.getIntValorRecarga() > product.getMaxValue())
                throw  new ErrorTypeException (ErrorTypeEnum.GREATER_MAXIMUM_VALUE);

            if (request.getStrSerialSC()==null || request.getStrSerialSC().isEmpty())
                throw  new ErrorTypeException (ErrorTypeEnum.EMPTY_MSISDN);

            if (product.getRegex() != null && !product.getRegex().isEmpty()
                    && !Pattern.compile(product.getRegex()).matcher(request.getStrSerialSC()).matches())
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_MSISDN);


        }catch (ErrorTypeException ex){
            LOGGER.error("ErrorTypeException: "+ex.getMessage());
            errorTypeEnum = ex.getErrorTypeEnum();

        }catch (Exception ex){
            LOGGER.error("Exception : "+ex.getMessage());
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;

        }finally {
            if (errorTypeEnum == null)
                errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;

            ErrorCode error = this.errorCodeRepository
                    .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.DIRECT_TV.getId(),
                            errorTypeEnum.getId());

            ObjectFactory factory = new ObjectFactory();

            SolicitudRecargaResponse.SolicitudRecargaResult result =factory.createSolicitudRecargaResponseSolicitudRecargaResult();

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.newDocument();
            

            Element recharge = document.createElement("any");
            document.appendChild(recharge);
           
            Element element = document.createElement("dsSolicitdsSolicitudRecargasudReversos");
            recharge.appendChild(element);
            
            Element elements = document.createElement("elements");
            element.appendChild(elements);
            
            Element codeTransaction = document.createElement("codeTransaction");
            codeTransaction.appendChild(document.createTextNode(this.getAutorizationNumber()));
            elements.appendChild(codeTransaction);
           
            
            Element codeResult = document.createElement("codeResult");
            codeResult.appendChild(document.createTextNode(error.getCode()));
            elements.appendChild(codeResult);
            
            Element amount = document.createElement("value");
            amount.appendChild(document.createTextNode(request.getIntValorRecarga()+""));
            elements.appendChild(amount);

            Element days = document.createElement("days");
            days.appendChild(document.createTextNode("5"));
            elements.appendChild(days);

            Element bonus = document.createElement("bonus");
            bonus.appendChild(document.createTextNode(request.getIntValorRecarga()+""));
            elements.appendChild(bonus);
            
            Element comments = document.createElement("comments");
            comments.appendChild(document.createTextNode(error.getMessage()));
            elements.appendChild(comments);

            Element other = document.createElement("other");
            other.appendChild(document.createTextNode("error others"));
            elements.appendChild(comments);
            
            result.setAny(recharge);
            
            LOGGER.info("recarga: "+error.getMessage());

            return result;

        }
    }


    /**
     * Método que genera un número de autorizado
     * @return
     */
    private String getAutorizationNumber(){
        Calendar cal = Calendar.getInstance();
        String year = (cal.get(Calendar.YEAR)+"").substring(1);
        String month= (cal.get(Calendar.MONTH)+1)+"";
        if (month.length()<2)month= "0"+month;
        String day= cal.get(Calendar.DAY_OF_MONTH)+"";
        if (day.length()<2)day= "0"+day;


        String hour= cal.get(Calendar.HOUR_OF_DAY)+"";
        if (hour.length()<2)hour= "0"+hour;

        String minute= cal.get(Calendar.MINUTE)+"";
        if (minute.length()<2)minute= "0"+minute;

        Random ran = new Random();
        int x = ran.nextInt(999) + 999;

        String resp= year+month+day
                +hour+minute
                +x;

        return resp;
    }
}
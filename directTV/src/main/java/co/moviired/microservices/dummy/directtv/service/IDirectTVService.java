package  co.moviired.microservices.dummy.directtv.service;

import org.tempuri.SolicitudRecarga;
import org.tempuri.SolicitudRecargaResponse;
import org.tempuri.SolicitudReverso;
import org.tempuri.SolicitudReversoResponse;

public interface IDirectTVService{

    SolicitudRecargaResponse.SolicitudRecargaResult solicitudRecarga(SolicitudRecarga request) throws  Exception;
    SolicitudReversoResponse.SolicitudReversoResult solicitudReverso(SolicitudReverso request)throws  Exception ;

}
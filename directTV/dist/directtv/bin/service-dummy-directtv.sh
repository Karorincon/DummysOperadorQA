#!/usr/bin/env bash
#CAMBIAR POR LA RUTA ESPECIFICA
SYSTEM_PATH=/app/dummyservices/directtv
BIN_PATH=${SYSTEM_PATH}/bin
LIB_PATH=${SYSTEM_PATH}/lib
CONFIG_PATH=${SYSTEM_PATH}/conf
LOG_PATH=${SYSTEM_PATH}/log
LOG_FILE_NAME="service-dummy-directtv.log"
PID_FILE="service-dummy-directtv.pid"
JAR_NAME="service-dummy-directtv.jar"

# SE SETAN LOS PARAMETROS DE MEMORIA Y PARAMETROS DE JVM
JVM_ARGS="-Xms512m -Xmx1G -XX:MaxMetaspaceSize=1G -XX:+CMSClassUnloadingEnabled -XX:+UseG1GC -XX:+OptimizeStringConcat -XX:+UseStringDeduplication -XX:+UseLWPSynchronization -Dlog4j2.contextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector -Djavax.xml.accessExternalDTD=all"
JVM_OPTIONS="-server -Dfile.encoding=UTF-8 -Dspring.output.ansi.enabled=always -Dlogging.config=${CONFIG_PATH}/log4j2.xml -Dspring.config.location=${CONFIG_PATH}/application.yml"

# CAMBIAR POR LA RUTA DE JAVA
JAVA_HOME=/u01/apps/wildfly/jdk1.8.0_151

function log(){
  echo $1 >> ${LOG_PATH}/${LOG_FILE_NAME}
}
# OBTENEMOS LA OPCION DEL SERVICIO A EJECUTAR (stop; start; status)
OPTION=$1
case ${OPTION} in
  start)
    if [ ! -f ${CONFIG_PATH}/${PID_FILE} ]
    then
      if [ "${JAVA_HOME}" == "" ]
      then
        echo "JAVA_HOME environment variable must be declared"
        exit 1
      else
        ${JAVA_HOME}/bin/java ${JVM_OPTIONS} ${JVM_ARGS} -jar ${LIB_PATH}/${JAR_NAME}  --configuration_external_path=${CONFIG_PATH} 2>&1 > /dev/null &
        echo $! > ${CONFIG_PATH}/${PID_FILE}
        echo "Service started OK"
      fi
    else
      PID=$(cat ${CONFIG_PATH}/${PID_FILE})
      echo "Socket process is already running - PID: ${PID}"
      exit 1
    fi
  ;;
  stop)
    PID=$(cat ${CONFIG_PATH}/${PID_FILE})
    kill -9 ${PID}
    rm -rf ${CONFIG_PATH}/${PID_FILE}
    log "Process ${PID} killed"
    echo "Socket process stopped OK"
  ;;
  status)
    if [ -f ${CONFIG_PATH}/${PID_FILE} ]
    then
      PID=$(cat ${CONFIG_PATH}/${PID_FILE})
      echo "Process Status: Service ON-LINE"
      echo "PID: ${PID}"
    else
      echo "Process Status: SHUTDOWN"
    fi
  ;;
esac
package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adjustmentRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="adjustmentRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="realAdjustment" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceAdjustment" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjustmentRequestDTO", propOrder = {
        "information",
        "mobileNumber",
        "realAdjustment",
        "resourceAdjustment",
        "transactionId"
})
public class AdjustmentRequestDTO {

    protected String information;
    protected String mobileNumber;
    protected MoneyDTO realAdjustment;
    protected ResourceDTO resourceAdjustment;
    protected String transactionId;

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the realAdjustment property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealAdjustment() {
        return realAdjustment;
    }

    /**
     * Sets the value of the realAdjustment property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealAdjustment(MoneyDTO value) {
        this.realAdjustment = value;
    }

    /**
     * Gets the value of the resourceAdjustment property.
     *
     * @return possible object is
     * {@link ResourceDTO }
     */
    public ResourceDTO getResourceAdjustment() {
        return resourceAdjustment;
    }

    /**
     * Sets the value of the resourceAdjustment property.
     *
     * @param value allowed object is
     *              {@link ResourceDTO }
     */
    public void setResourceAdjustment(ResourceDTO value) {
        this.resourceAdjustment = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

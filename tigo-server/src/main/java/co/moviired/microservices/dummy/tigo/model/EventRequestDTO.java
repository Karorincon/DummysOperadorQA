package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for eventRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eventRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cellId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="eventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="eventType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="location" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}locationDTO" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="networkBearerId" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}networkBearerIDDTO" minOccurs="0"/>
 *         &lt;element name="numberOfEvents" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="origination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="volume" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventRequestDTO", propOrder = {
        "addressType",
        "cellId",
        "destType",
        "destination",
        "direction",
        "duration",
        "eventTime",
        "eventType",
        "information",
        "location",
        "mobileNumber",
        "networkBearerId",
        "numberOfEvents",
        "origination",
        "transactionId",
        "transactionType",
        "volume"
})
public class EventRequestDTO {

    protected String addressType;
    protected String cellId;
    protected String destType;
    protected String destination;
    protected String direction;
    protected Integer duration;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    protected String eventType;
    protected String information;
    protected LocationDTO location;
    protected String mobileNumber;
    protected NetworkBearerIDDTO networkBearerId;
    protected Integer numberOfEvents;
    protected String origination;
    protected String transactionId;
    protected String transactionType;
    protected Integer volume;

    /**
     * Gets the value of the addressType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAddressType(String value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the cellId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCellId() {
        return cellId;
    }

    /**
     * Sets the value of the cellId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCellId(String value) {
        this.cellId = value;
    }

    /**
     * Gets the value of the destType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestType() {
        return destType;
    }

    /**
     * Sets the value of the destType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestType(String value) {
        this.destType = value;
    }

    /**
     * Gets the value of the destination property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the direction property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDirection(String value) {
        this.direction = value;
    }

    /**
     * Gets the value of the duration property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setDuration(Integer value) {
        this.duration = value;
    }

    /**
     * Gets the value of the eventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the eventType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEventType(String value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the location property.
     *
     * @return possible object is
     * {@link LocationDTO }
     */
    public LocationDTO getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     *
     * @param value allowed object is
     *              {@link LocationDTO }
     */
    public void setLocation(LocationDTO value) {
        this.location = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the networkBearerId property.
     *
     * @return possible object is
     * {@link NetworkBearerIDDTO }
     */
    public NetworkBearerIDDTO getNetworkBearerId() {
        return networkBearerId;
    }

    /**
     * Sets the value of the networkBearerId property.
     *
     * @param value allowed object is
     *              {@link NetworkBearerIDDTO }
     */
    public void setNetworkBearerId(NetworkBearerIDDTO value) {
        this.networkBearerId = value;
    }

    /**
     * Gets the value of the numberOfEvents property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getNumberOfEvents() {
        return numberOfEvents;
    }

    /**
     * Sets the value of the numberOfEvents property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setNumberOfEvents(Integer value) {
        this.numberOfEvents = value;
    }

    /**
     * Gets the value of the origination property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOrigination() {
        return origination;
    }

    /**
     * Sets the value of the origination property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOrigination(String value) {
        this.origination = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the transactionType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the volume property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getVolume() {
        return volume;
    }

    /**
     * Sets the value of the volume property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setVolume(Integer value) {
        this.volume = value;
    }

}

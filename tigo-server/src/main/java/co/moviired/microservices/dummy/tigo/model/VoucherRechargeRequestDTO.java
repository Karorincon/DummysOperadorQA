package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for voucherRechargeRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="voucherRechargeRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originatingSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retailerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voucherId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voucherNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "voucherRechargeRequestDTO", propOrder = {
        "eventTime",
        "information",
        "msisdn",
        "originatingSource",
        "retailerName",
        "transactionId",
        "voucherId",
        "voucherNumber"
})
public class VoucherRechargeRequestDTO {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    protected String information;
    protected String msisdn;
    protected String originatingSource;
    protected String retailerName;
    protected String transactionId;
    protected String voucherId;
    protected String voucherNumber;

    /**
     * Gets the value of the eventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the msisdn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the originatingSource property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOriginatingSource() {
        return originatingSource;
    }

    /**
     * Sets the value of the originatingSource property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOriginatingSource(String value) {
        this.originatingSource = value;
    }

    /**
     * Gets the value of the retailerName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRetailerName() {
        return retailerName;
    }

    /**
     * Sets the value of the retailerName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRetailerName(String value) {
        this.retailerName = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the voucherId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVoucherId() {
        return voucherId;
    }

    /**
     * Sets the value of the voucherId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVoucherId(String value) {
        this.voucherId = value;
    }

    /**
     * Gets the value of the voucherNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVoucherNumber() {
        return voucherNumber;
    }

    /**
     * Sets the value of the voucherNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVoucherNumber(String value) {
        this.voucherNumber = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reservationRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="overrideOption" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="reservationGeneralInfo" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationGeneralRequestDTO" minOccurs="0"/>
 *         &lt;element name="validUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationRequestDTO", propOrder = {
        "overrideOption",
        "reservationGeneralInfo",
        "validUntil"
})
public class ReservationRequestDTO {

    protected boolean overrideOption;
    protected co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO reservationGeneralInfo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validUntil;

    /**
     * Gets the value of the overrideOption property.
     */
    public boolean isOverrideOption() {
        return overrideOption;
    }

    /**
     * Sets the value of the overrideOption property.
     */
    public void setOverrideOption(boolean value) {
        this.overrideOption = value;
    }

    /**
     * Gets the value of the reservationGeneralInfo property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO getReservationGeneralInfo() {
        return reservationGeneralInfo;
    }

    /**
     * Sets the value of the reservationGeneralInfo property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO }
     */
    public void setReservationGeneralInfo(ReservationGeneralRequestDTO value) {
        this.reservationGeneralInfo = value;
    }

    /**
     * Gets the value of the validUntil property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getValidUntil() {
        return validUntil;
    }

    /**
     * Sets the value of the validUntil property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setValidUntil(XMLGregorianCalendar value) {
        this.validUntil = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for creditRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="creditRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusCredit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyDTO" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="realCredit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceCredit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "creditRequestDTO", propOrder = {
        "bonusCredit",
        "dedicatedMoneyDTO",
        "information",
        "mobileNumber",
        "realCredit",
        "resourceCredit",
        "transactionId",
        "transactionType"
})
public class CreditRequestDTO {

    protected MoneyDTO bonusCredit;
    protected DedicatedMoneyDTO dedicatedMoneyDTO;
    protected String information;
    protected String mobileNumber;
    protected MoneyDTO realCredit;
    protected ResourceDTO resourceCredit;
    protected String transactionId;
    protected String transactionType;

    /**
     * Gets the value of the bonusCredit property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusCredit() {
        return bonusCredit;
    }

    /**
     * Sets the value of the bonusCredit property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusCredit(MoneyDTO value) {
        this.bonusCredit = value;
    }

    /**
     * Gets the value of the dedicatedMoneyDTO property.
     *
     * @return possible object is
     * {@link DedicatedMoneyDTO }
     */
    public DedicatedMoneyDTO getDedicatedMoneyDTO() {
        return dedicatedMoneyDTO;
    }

    /**
     * Sets the value of the dedicatedMoneyDTO property.
     *
     * @param value allowed object is
     *              {@link DedicatedMoneyDTO }
     */
    public void setDedicatedMoneyDTO(DedicatedMoneyDTO value) {
        this.dedicatedMoneyDTO = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the realCredit property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealCredit() {
        return realCredit;
    }

    /**
     * Sets the value of the realCredit property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealCredit(MoneyDTO value) {
        this.realCredit = value;
    }

    /**
     * Gets the value of the resourceCredit property.
     *
     * @return possible object is
     * {@link ResourceDTO }
     */
    public ResourceDTO getResourceCredit() {
        return resourceCredit;
    }

    /**
     * Sets the value of the resourceCredit property.
     *
     * @param value allowed object is
     *              {@link ResourceDTO }
     */
    public void setResourceCredit(ResourceDTO value) {
        this.resourceCredit = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the transactionType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

}

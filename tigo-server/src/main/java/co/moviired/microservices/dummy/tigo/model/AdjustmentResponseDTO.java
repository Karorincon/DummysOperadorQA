package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adjustmentResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="adjustmentResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="balanceResponse" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}balanceResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjustmentResponseDTO", propOrder = {
        "balanceResponse"
})
public class AdjustmentResponseDTO {

    protected BalanceResponseDTO balanceResponse;

    /**
     * Gets the value of the balanceResponse property.
     *
     * @return possible object is
     * {@link BalanceResponseDTO }
     */
    public BalanceResponseDTO getBalanceResponse() {
        return balanceResponse;
    }

    /**
     * Sets the value of the balanceResponse property.
     *
     * @param value allowed object is
     *              {@link BalanceResponseDTO }
     */
    public void setBalanceResponse(BalanceResponseDTO value) {
        this.balanceResponse = value;
    }

}

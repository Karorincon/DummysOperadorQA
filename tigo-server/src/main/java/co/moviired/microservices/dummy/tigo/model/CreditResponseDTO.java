package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for creditResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="creditResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusCredit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoney" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realCredit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceCredit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "creditResponseDTO", propOrder = {
        "authorizationNumber",
        "bonusBalance",
        "bonusCredit",
        "dedicatedMoney",
        "dedicatedMoneyBalance",
        "mobileNumber",
        "realBalance",
        "realCredit",
        "resourceBalance",
        "resourceCredit",
        "transactionId"
})
public class CreditResponseDTO {

    protected String authorizationNumber;
    protected MoneyDTO bonusBalance;
    protected MoneyDTO bonusCredit;
    @XmlElement(nillable = true)
    protected List<DedicatedMoneyDTO> dedicatedMoney;
    @XmlElement(nillable = true)
    protected List<DedicatedMoneyDTO> dedicatedMoneyBalance;
    protected String mobileNumber;
    protected MoneyDTO realBalance;
    protected MoneyDTO realCredit;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceBalance;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceCredit;
    protected String transactionId;

    /**
     * Gets the value of the authorizationNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    /**
     * Sets the value of the authorizationNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthorizationNumber(String value) {
        this.authorizationNumber = value;
    }

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusCredit property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusCredit() {
        return bonusCredit;
    }

    /**
     * Sets the value of the bonusCredit property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusCredit(MoneyDTO value) {
        this.bonusCredit = value;
    }

    /**
     * Gets the value of the dedicatedMoney property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoney property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoney().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DedicatedMoneyDTO }
     */
    public List<DedicatedMoneyDTO> getDedicatedMoney() {
        if (dedicatedMoney == null) {
            dedicatedMoney = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoney;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DedicatedMoneyDTO }
     */
    public List<DedicatedMoneyDTO> getDedicatedMoneyBalance() {
        if (dedicatedMoneyBalance == null) {
            dedicatedMoneyBalance = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyBalance;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realCredit property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealCredit() {
        return realCredit;
    }

    /**
     * Sets the value of the realCredit property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealCredit(MoneyDTO value) {
        this.realCredit = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<ResourceDTO>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the resourceCredit property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceCredit property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceCredit().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceCredit() {
        if (resourceCredit == null) {
            resourceCredit = new ArrayList<ResourceDTO>();
        }
        return this.resourceCredit;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

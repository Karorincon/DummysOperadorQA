package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.DebitResponseDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eRechargeResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eRechargeResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusStatus" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="debitResponse" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}debitResponseDTO" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rechargeResponse" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}rechargeResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eRechargeResponseDTO", propOrder = {
        "bonusStatus",
        "debitResponse",
        "message",
        "rechargeResponse"
})
public class ERechargeResponseDTO {

    protected IdDTO bonusStatus;
    protected co.moviired.microservices.dummy.tigo.model.DebitResponseDTO debitResponse;
    protected String message;
    protected RechargeResponseDTO rechargeResponse;

    @Override
    public String toString() {
        return "ERechargeResponseDTO{" +
                "bonusStatus=" + bonusStatus +
                ", debitResponse=" + debitResponse +
                ", message='" + message + '\'' +
                ", rechargeResponse=" + rechargeResponse +
                '}';
    }

    /**
     * Gets the value of the bonusStatus property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getBonusStatus() {
        return bonusStatus;
    }

    /**
     * Sets the value of the bonusStatus property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setBonusStatus(IdDTO value) {
        this.bonusStatus = value;
    }

    /**
     * Gets the value of the debitResponse property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.DebitResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.DebitResponseDTO getDebitResponse() {
        return debitResponse;
    }

    /**
     * Sets the value of the debitResponse property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.DebitResponseDTO }
     */
    public void setDebitResponse(DebitResponseDTO value) {
        this.debitResponse = value;
    }

    /**
     * Gets the value of the message property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the rechargeResponse property.
     *
     * @return possible object is
     * {@link RechargeResponseDTO }
     */
    public RechargeResponseDTO getRechargeResponse() {
        return rechargeResponse;
    }

    /**
     * Sets the value of the rechargeResponse property.
     *
     * @param value allowed object is
     *              {@link RechargeResponseDTO }
     */
    public void setRechargeResponse(RechargeResponseDTO value) {
        this.rechargeResponse = value;
    }

}

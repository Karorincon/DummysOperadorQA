package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for provisioningUsersRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="provisioningUsersRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usersPromotionCollection" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}userPromotionDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "provisioningUsersRequestDTO", propOrder = {
        "usersPromotionCollection"
})
public class ProvisioningUsersRequestDTO {

    @XmlElement(nillable = true)
    protected List<UserPromotionDTO> usersPromotionCollection;

    /**
     * Gets the value of the usersPromotionCollection property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usersPromotionCollection property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsersPromotionCollection().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserPromotionDTO }
     */
    public List<UserPromotionDTO> getUsersPromotionCollection() {
        if (usersPromotionCollection == null) {
            usersPromotionCollection = new ArrayList<UserPromotionDTO>();
        }
        return this.usersPromotionCollection;
    }

}

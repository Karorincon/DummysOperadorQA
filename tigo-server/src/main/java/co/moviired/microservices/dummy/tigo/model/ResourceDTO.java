package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for resourceDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="resourceDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="offPeak" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partitionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="partitionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partitionUnit" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="peak" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promotional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promotionalExpireDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="serviceRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resourceDTO", propOrder = {
        "activationDate",
        "expirationDate",
        "offPeak",
        "partitionId",
        "partitionName",
        "partitionUnit",
        "peak",
        "promotional",
        "promotionalExpireDate",
        "serviceRate"
})
public class ResourceDTO {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activationDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    protected String offPeak;
    protected Integer partitionId;
    protected String partitionName;
    protected Integer partitionUnit;
    protected String peak;
    protected String promotional;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar promotionalExpireDate;
    protected String serviceRate;

    /**
     * Gets the value of the activationDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setActivationDate(XMLGregorianCalendar value) {
        this.activationDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the offPeak property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOffPeak() {
        return offPeak;
    }

    /**
     * Sets the value of the offPeak property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOffPeak(String value) {
        this.offPeak = value;
    }

    /**
     * Gets the value of the partitionId property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPartitionId() {
        return partitionId;
    }

    /**
     * Sets the value of the partitionId property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPartitionId(Integer value) {
        this.partitionId = value;
    }

    /**
     * Gets the value of the partitionName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPartitionName() {
        return partitionName;
    }

    /**
     * Sets the value of the partitionName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPartitionName(String value) {
        this.partitionName = value;
    }

    /**
     * Gets the value of the partitionUnit property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPartitionUnit() {
        return partitionUnit;
    }

    /**
     * Sets the value of the partitionUnit property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPartitionUnit(Integer value) {
        this.partitionUnit = value;
    }

    /**
     * Gets the value of the peak property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPeak() {
        return peak;
    }

    /**
     * Sets the value of the peak property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPeak(String value) {
        this.peak = value;
    }

    /**
     * Gets the value of the promotional property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPromotional() {
        return promotional;
    }

    /**
     * Sets the value of the promotional property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPromotional(String value) {
        this.promotional = value;
    }

    /**
     * Gets the value of the promotionalExpireDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getPromotionalExpireDate() {
        return promotionalExpireDate;
    }

    /**
     * Sets the value of the promotionalExpireDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setPromotionalExpireDate(XMLGregorianCalendar value) {
        this.promotionalExpireDate = value;
    }

    /**
     * Gets the value of the serviceRate property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getServiceRate() {
        return serviceRate;
    }

    /**
     * Sets the value of the serviceRate property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setServiceRate(String value) {
        this.serviceRate = value;
    }

}

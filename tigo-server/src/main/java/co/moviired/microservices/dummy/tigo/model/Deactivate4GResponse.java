package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deactivate4GResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="deactivate4GResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}deactivate4GResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deactivate4GResponse", propOrder = {
        "_return"
})
public class Deactivate4GResponse {

    @XmlElement(name = "return")
    protected Deactivate4GResponseDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link Deactivate4GResponseDTO }
     */
    public Deactivate4GResponseDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link Deactivate4GResponseDTO }
     */
    public void setReturn(Deactivate4GResponseDTO value) {
        this._return = value;
    }

}

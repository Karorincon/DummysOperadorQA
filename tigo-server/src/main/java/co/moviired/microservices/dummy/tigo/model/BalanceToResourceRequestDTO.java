package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for balanceToResourceRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="balanceToResourceRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amountResources" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resourceId" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "balanceToResourceRequestDTO", propOrder = {
        "amountResources",
        "information",
        "resourceId",
        "transactionId"
})
public class BalanceToResourceRequestDTO {

    protected Long amountResources;
    protected String information;
    protected IdDTO resourceId;
    protected String transactionId;

    /**
     * Gets the value of the amountResources property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getAmountResources() {
        return amountResources;
    }

    /**
     * Sets the value of the amountResources property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setAmountResources(Long value) {
        this.amountResources = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the resourceId property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setResourceId(IdDTO value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for groupMvnoDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="groupMvnoDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fnum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imsiRange" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}rangeDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originatingSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdpcpActivateBB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdpcpCancelBB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="profileHlr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "groupMvnoDTO", propOrder = {
        "fnum",
        "imsiRange",
        "name",
        "originatingSource",
        "pdpcpActivateBB",
        "pdpcpCancelBB",
        "profileHlr",
        "spid"
})
public class GroupMvnoDTO {

    protected String fnum;
    protected List<RangeDTO> imsiRange;
    protected String name;
    protected String originatingSource;
    protected String pdpcpActivateBB;
    protected String pdpcpCancelBB;
    protected String profileHlr;
    protected String spid;

    /**
     * Gets the value of the fnum property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFnum() {
        return fnum;
    }

    /**
     * Sets the value of the fnum property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFnum(String value) {
        this.fnum = value;
    }

    /**
     * Gets the value of the imsiRange property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the imsiRange property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImsiRange().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RangeDTO }
     */
    public List<RangeDTO> getImsiRange() {
        if (imsiRange == null) {
            imsiRange = new ArrayList<RangeDTO>();
        }
        return this.imsiRange;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the originatingSource property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOriginatingSource() {
        return originatingSource;
    }

    /**
     * Sets the value of the originatingSource property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOriginatingSource(String value) {
        this.originatingSource = value;
    }

    /**
     * Gets the value of the pdpcpActivateBB property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPdpcpActivateBB() {
        return pdpcpActivateBB;
    }

    /**
     * Sets the value of the pdpcpActivateBB property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPdpcpActivateBB(String value) {
        this.pdpcpActivateBB = value;
    }

    /**
     * Gets the value of the pdpcpCancelBB property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPdpcpCancelBB() {
        return pdpcpCancelBB;
    }

    /**
     * Sets the value of the pdpcpCancelBB property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPdpcpCancelBB(String value) {
        this.pdpcpCancelBB = value;
    }

    /**
     * Gets the value of the profileHlr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getProfileHlr() {
        return profileHlr;
    }

    /**
     * Sets the value of the profileHlr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setProfileHlr(String value) {
        this.profileHlr = value;
    }

    /**
     * Gets the value of the spid property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSpid() {
        return spid;
    }

    /**
     * Sets the value of the spid property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSpid(String value) {
        this.spid = value;
    }

}

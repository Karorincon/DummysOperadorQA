package co.moviired.microservices.dummy.tigo.service.impl;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.conexion.repository.ISubscriberRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import co.moviired.microservices.dummy.tigo.model.*;
import co.moviired.microservices.dummy.tigo.service.ITigoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

@Service
public class TigoService implements ITigoService{

    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private ISubscriberRepository subscriberRepository;
    @Autowired
    private IErrorCodeRepository errorCodeRepository;

    private static final Logger LOGGER = LogManager.getLogger(TigoService.class.getName());

    /**
     * Método utilizado para validar las peticiones de los servicios
     *
     * @param request petición
     * @return
     */
    private ERechargeResponseDTO validateParams(ERechargeRequestDTO request) throws  BusinessException_Exception{
        ErrorTypeEnum errorTypeEnum=null;
        ObjectFactory factory = new ObjectFactory();
        ERechargeResponseDTO response = factory.createERechargeResponseDTO();
        //String date = this.simpleDateFormat.format(new Date());

        try{
            /**
             * Validamos que el campo cuente con la cantidad
             */
            if(request.getAmount()==null)
                throw  new ErrorTypeException(ErrorTypeEnum.INVALID_AMOUNT);

            /*
            Subscriber subscriber = this.subscriberRepository
                    .findByThirdPartyIdAndMsisdn(ThirdPartyEnum.TIGO.getId(),request.getTarget());


            if(subscriber==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_ACCOUNT);

            */


            Product product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.TIGO.getId(), "1");

            LOGGER.info("ThirdPartyEnum.TIGO.getId()= "+ThirdPartyEnum.TIGO.getId());
            LOGGER.info("se envia 1");
            LOGGER.info("product= "+product);

            if(product==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_PRODUCT);

            Double amount = request.getAmount().getAmount().doubleValue();

            if (amount<product.getMinValue())
                throw  new ErrorTypeException (ErrorTypeEnum.LOWER_MINOR_VALUE);

            if (amount > product.getMaxValue())
                throw  new ErrorTypeException (ErrorTypeEnum.GREATER_MAXIMUM_VALUE);


            if (request.getTarget()==null || request.getTarget().isEmpty())
                throw  new ErrorTypeException (ErrorTypeEnum.EMPTY_MSISDN);

            if (product.getRegex() != null && !product.getRegex().isEmpty()
                    && !Pattern.compile(product.getRegex()).matcher(request.getTarget()).matches())
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_MSISDN);

            /*

            if (subscriber.getTotalNet()< amount)
                throw  new ErrorTypeException(ErrorTypeEnum.NO_CREDIT);
            */

        }catch (ErrorTypeException ex){
            LOGGER.error("ErrorTypeException: "+ex.getMessage());
            errorTypeEnum = ex.getErrorTypeEnum();

        }catch (Exception ex){
            LOGGER.error("Exception : "+ex.getMessage());
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;


        }

        if (errorTypeEnum==null)
            errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;

        ErrorCode error = this.errorCodeRepository
                .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.TIGO.getId(),
                        errorTypeEnum.getId());


        DebitResponseDTO debit = factory.createDebitResponseDTO();
        debit.setAuthorizationNumber(this.getAutorizationNumber());
        IdDTO id = factory.createIdDTO();

        if (errorTypeEnum.equals(ErrorTypeEnum.SUCCESSFULL)){
            debit.setBonusBalance(request.getAmount());
            debit.setBonusDebited(request.getAmount());
            debit.setRealDebited(request.getAmount());
            debit.setMobileNumber(request.getTarget());
            debit.setTransactionId(request.getTransactionId());
        }

        id.setId(error.getStatus());
        id.setName(error.getMessage());
        debit.setProduct(id);

        RechargeResponseDTO dto = factory.createRechargeResponseDTO();
        dto.setAuthorizationNumber(debit.getAuthorizationNumber());
        dto.setBonusBalance(request.getAmount());
        dto.setBonusCredited(request.getAmount());
        dto.setMobileNumber(request.getTarget());
        dto.setRealBalance(request.getAmount());
        dto.setRealCredited(request.getAmount());
        dto.setTransactionId(request.getTransactionId());

        response.setDebitResponse(debit);
        response.setBonusStatus(id);
        response.setMessage(error.getMessage());
        response.setRechargeResponse(dto);

        return response;


    }

    /**
     * Método que genera un número de autorizado
     * @return
     */
    private String getAutorizationNumber(){
        Calendar cal = Calendar.getInstance();
        String year = (cal.get(Calendar.YEAR)+"").substring(1);
        String month= (cal.get(Calendar.MONTH)+1)+"";
        if (month.length()<2)month= "0"+month;
        String day= cal.get(Calendar.DAY_OF_MONTH)+"";
        if (day.length()<2)day= "0"+day;


        String hour= cal.get(Calendar.HOUR_OF_DAY)+"";
        if (hour.length()<2)hour= "0"+hour;

        String minute= cal.get(Calendar.MINUTE)+"";
        if (minute.length()<2)minute= "0"+minute;

        Random ran = new Random();
        int x = ran.nextInt(999) + 999;

        String resp= year+month+day+"."
                +hour+minute+"."
                +x;

        return resp;
    }

    @Override
    public ERechargeResponseDTO processRecharge(ERechargeRequestDTO request) throws  BusinessException_Exception{
        return  this.validateParams(request);
    }

    @Override
    public ERechargeRevResponseDTO processReversal(ERechargeRevRequestDTO request) throws BusinessException_Exception {

        ErrorCode error = this.errorCodeRepository
                .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.TIGO.getId(), ErrorTypeEnum.FAILED_TRANSACTION.getId());

        ObjectFactory factory = new ObjectFactory();
        ERechargeRevResponseDTO  dto = factory.createERechargeRevResponseDTO();

        IdDTO id = factory.createIdDTO();
        id.setId(error.getCode());
        id.setName(error.getMessage());
        dto.setMessage(error.getMessage());
        dto.setBonusStatus(id);


        CreditResponseDTO credit = factory.createCreditResponseDTO();
        credit.setAuthorizationNumber(this.getAutorizationNumber());
        credit.setBonusBalance(request.getAmount());
        credit.setRealCredit(request.getAmount());
        credit.setMobileNumber(request.getSource());
        credit.setRealCredit(request.getAmount());
        credit.setTransactionId(request.getTransactionId());

        dto.setCreditResponse(credit);

        return dto;

    }



}

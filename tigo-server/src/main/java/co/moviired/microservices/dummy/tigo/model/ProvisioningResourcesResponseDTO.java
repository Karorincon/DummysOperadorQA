package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for provisioningResourcesResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="provisioningResourcesResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sellersColl" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}sellerDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "provisioningResourcesResponseDTO", propOrder = {
        "sellersColl"
})
public class ProvisioningResourcesResponseDTO {

    @XmlElement(nillable = true)
    protected List<SellerDTO> sellersColl;

    /**
     * Gets the value of the sellersColl property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sellersColl property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSellersColl().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SellerDTO }
     */
    public List<SellerDTO> getSellersColl() {
        if (sellersColl == null) {
            sellersColl = new ArrayList<SellerDTO>();
        }
        return this.sellersColl;
    }

}

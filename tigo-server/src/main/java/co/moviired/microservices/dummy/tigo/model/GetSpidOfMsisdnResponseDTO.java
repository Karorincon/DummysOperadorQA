package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSpidOfMsisdnResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getSpidOfMsisdnResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="spid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSpidOfMsisdnResponseDTO", propOrder = {
        "spid"
})
public class GetSpidOfMsisdnResponseDTO {

    protected String spid;

    /**
     * Gets the value of the spid property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSpid() {
        return spid;
    }

    /**
     * Sets the value of the spid property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSpid(String value) {
        this.spid = value;
    }

}

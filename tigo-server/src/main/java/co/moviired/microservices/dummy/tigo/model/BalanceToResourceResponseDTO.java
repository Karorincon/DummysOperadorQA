package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for balanceToResourceResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="balanceToResourceResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="creditResponseDTO" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}creditResponseDTO" minOccurs="0"/>
 *         &lt;element name="debitResponseDTO" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}debitResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "balanceToResourceResponseDTO", propOrder = {
        "creditResponseDTO",
        "debitResponseDTO"
})
public class BalanceToResourceResponseDTO {

    protected CreditResponseDTO creditResponseDTO;
    protected DebitResponseDTO debitResponseDTO;

    /**
     * Gets the value of the creditResponseDTO property.
     *
     * @return possible object is
     * {@link CreditResponseDTO }
     */
    public CreditResponseDTO getCreditResponseDTO() {
        return creditResponseDTO;
    }

    /**
     * Sets the value of the creditResponseDTO property.
     *
     * @param value allowed object is
     *              {@link CreditResponseDTO }
     */
    public void setCreditResponseDTO(CreditResponseDTO value) {
        this.creditResponseDTO = value;
    }

    /**
     * Gets the value of the debitResponseDTO property.
     *
     * @return possible object is
     * {@link DebitResponseDTO }
     */
    public DebitResponseDTO getDebitResponseDTO() {
        return debitResponseDTO;
    }

    /**
     * Sets the value of the debitResponseDTO property.
     *
     * @param value allowed object is
     *              {@link DebitResponseDTO }
     */
    public void setDebitResponseDTO(DebitResponseDTO value) {
        this.debitResponseDTO = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO;
import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for reservationQueryResponseItemDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationQueryResponseItemDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="controlledAccountBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}controlledAccountBalanceDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="eventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="reservationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reservationTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="validUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationQueryResponseItemDTO", propOrder = {
        "bonusBalance",
        "controlledAccountBalance",
        "dedicatedMoneyBalance",
        "eventTime",
        "realBalance",
        "reservationId",
        "reservationTransactionId",
        "resourceBalance",
        "validUntil"
})
public class ReservationQueryResponseItemDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusBalance;
    protected co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO controlledAccountBalance;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyBalance;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realBalance;
    protected String reservationId;
    protected String reservationTransactionId;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceBalance;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validUntil;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the controlledAccountBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO getControlledAccountBalance() {
        return controlledAccountBalance;
    }

    /**
     * Sets the value of the controlledAccountBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO }
     */
    public void setControlledAccountBalance(ControlledAccountBalanceDTO value) {
        this.controlledAccountBalance = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyBalance() {
        if (dedicatedMoneyBalance == null) {
            dedicatedMoneyBalance = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyBalance;
    }

    /**
     * Gets the value of the eventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the reservationId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReservationId() {
        return reservationId;
    }

    /**
     * Sets the value of the reservationId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReservationId(String value) {
        this.reservationId = value;
    }

    /**
     * Gets the value of the reservationTransactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReservationTransactionId() {
        return reservationTransactionId;
    }

    /**
     * Sets the value of the reservationTransactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReservationTransactionId(String value) {
        this.reservationTransactionId = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<ResourceDTO>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the validUntil property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getValidUntil() {
        return validUntil;
    }

    /**
     * Sets the value of the validUntil property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setValidUntil(XMLGregorianCalendar value) {
        this.validUntil = value;
    }

}

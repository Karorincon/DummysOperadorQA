package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for debitRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="debitRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="debitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyDebit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneyDebit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceDebit" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "debitRequestDTO", propOrder = {
        "debitType",
        "dedicatedMoneyDebit",
        "information",
        "mobileNumber",
        "moneyDebit",
        "resourceDebit",
        "transactionId",
        "transactionType"
})
public class DebitRequestDTO {

    protected String debitType;
    protected DedicatedMoneyDTO dedicatedMoneyDebit;
    protected String information;
    protected String mobileNumber;
    protected MoneyDTO moneyDebit;
    protected ResourceDTO resourceDebit;
    protected String transactionId;
    protected String transactionType;

    /**
     * Gets the value of the debitType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDebitType() {
        return debitType;
    }

    /**
     * Sets the value of the debitType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDebitType(String value) {
        this.debitType = value;
    }

    /**
     * Gets the value of the dedicatedMoneyDebit property.
     *
     * @return possible object is
     * {@link DedicatedMoneyDTO }
     */
    public DedicatedMoneyDTO getDedicatedMoneyDebit() {
        return dedicatedMoneyDebit;
    }

    /**
     * Sets the value of the dedicatedMoneyDebit property.
     *
     * @param value allowed object is
     *              {@link DedicatedMoneyDTO }
     */
    public void setDedicatedMoneyDebit(DedicatedMoneyDTO value) {
        this.dedicatedMoneyDebit = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the moneyDebit property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getMoneyDebit() {
        return moneyDebit;
    }

    /**
     * Sets the value of the moneyDebit property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setMoneyDebit(MoneyDTO value) {
        this.moneyDebit = value;
    }

    /**
     * Gets the value of the resourceDebit property.
     *
     * @return possible object is
     * {@link ResourceDTO }
     */
    public ResourceDTO getResourceDebit() {
        return resourceDebit;
    }

    /**
     * Sets the value of the resourceDebit property.
     *
     * @param value allowed object is
     *              {@link ResourceDTO }
     */
    public void setResourceDebit(ResourceDTO value) {
        this.resourceDebit = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the transactionType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

}

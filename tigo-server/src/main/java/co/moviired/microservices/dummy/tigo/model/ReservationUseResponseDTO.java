package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;
import co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for reservationUseResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationUseResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusUsed" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyUsed" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numberOfEvents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="realReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realUsed" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="reservationGeneralInfo" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationGeneralResponseDTO" minOccurs="0"/>
 *         &lt;element name="resourceReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceUsed" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="usageType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationUseResponseDTO", propOrder = {
        "bonusReserved",
        "bonusReturned",
        "bonusUsed",
        "dedicatedMoneyReserved",
        "dedicatedMoneyReturned",
        "dedicatedMoneyUsed",
        "numberOfEvents",
        "realReserved",
        "realReturned",
        "realUsed",
        "reservationGeneralInfo",
        "resourceReserved",
        "resourceReturned",
        "resourceUsed",
        "usageType"
})
public class ReservationUseResponseDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusReserved;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusReturned;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusUsed;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyReserved;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyReturned;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyUsed;
    protected int numberOfEvents;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realReserved;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realReturned;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realUsed;
    protected co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO reservationGeneralInfo;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceReserved;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceReturned;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceUsed;
    protected int usageType;

    /**
     * Gets the value of the bonusReserved property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusReserved() {
        return bonusReserved;
    }

    /**
     * Sets the value of the bonusReserved property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusReserved(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusReserved = value;
    }

    /**
     * Gets the value of the bonusReturned property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusReturned() {
        return bonusReturned;
    }

    /**
     * Sets the value of the bonusReturned property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusReturned(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusReturned = value;
    }

    /**
     * Gets the value of the bonusUsed property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusUsed() {
        return bonusUsed;
    }

    /**
     * Sets the value of the bonusUsed property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusUsed(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusUsed = value;
    }

    /**
     * Gets the value of the dedicatedMoneyReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyReserved() {
        if (dedicatedMoneyReserved == null) {
            dedicatedMoneyReserved = new ArrayList<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyReserved;
    }

    /**
     * Gets the value of the dedicatedMoneyReturned property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyReturned property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyReturned().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyReturned() {
        if (dedicatedMoneyReturned == null) {
            dedicatedMoneyReturned = new ArrayList<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyReturned;
    }

    /**
     * Gets the value of the dedicatedMoneyUsed property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyUsed property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyUsed().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyUsed() {
        if (dedicatedMoneyUsed == null) {
            dedicatedMoneyUsed = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyUsed;
    }

    /**
     * Gets the value of the numberOfEvents property.
     */
    public int getNumberOfEvents() {
        return numberOfEvents;
    }

    /**
     * Sets the value of the numberOfEvents property.
     */
    public void setNumberOfEvents(int value) {
        this.numberOfEvents = value;
    }

    /**
     * Gets the value of the realReserved property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealReserved() {
        return realReserved;
    }

    /**
     * Sets the value of the realReserved property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealReserved(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realReserved = value;
    }

    /**
     * Gets the value of the realReturned property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealReturned() {
        return realReturned;
    }

    /**
     * Sets the value of the realReturned property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealReturned(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realReturned = value;
    }

    /**
     * Gets the value of the realUsed property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealUsed() {
        return realUsed;
    }

    /**
     * Sets the value of the realUsed property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealUsed(MoneyDTO value) {
        this.realUsed = value;
    }

    /**
     * Gets the value of the reservationGeneralInfo property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO getReservationGeneralInfo() {
        return reservationGeneralInfo;
    }

    /**
     * Sets the value of the reservationGeneralInfo property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO }
     */
    public void setReservationGeneralInfo(ReservationGeneralResponseDTO value) {
        this.reservationGeneralInfo = value;
    }

    /**
     * Gets the value of the resourceReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceReserved() {
        if (resourceReserved == null) {
            resourceReserved = new ArrayList<ResourceDTO>();
        }
        return this.resourceReserved;
    }

    /**
     * Gets the value of the resourceReturned property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceReturned property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceReturned().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceReturned() {
        if (resourceReturned == null) {
            resourceReturned = new ArrayList<ResourceDTO>();
        }
        return this.resourceReturned;
    }

    /**
     * Gets the value of the resourceUsed property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceUsed property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceUsed().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceUsed() {
        if (resourceUsed == null) {
            resourceUsed = new ArrayList<ResourceDTO>();
        }
        return this.resourceUsed;
    }

    /**
     * Gets the value of the usageType property.
     */
    public int getUsageType() {
        return usageType;
    }

    /**
     * Sets the value of the usageType property.
     */
    public void setUsageType(int value) {
        this.usageType = value;
    }

}

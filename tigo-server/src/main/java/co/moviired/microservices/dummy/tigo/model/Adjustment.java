package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adjustment complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="adjustment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}adjustmentRequestDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjustment", propOrder = {
        "arg0"
})
public class Adjustment {

    protected AdjustmentRequestDTO arg0;

    /**
     * Gets the value of the arg0 property.
     *
     * @return possible object is
     * {@link AdjustmentRequestDTO }
     */
    public AdjustmentRequestDTO getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     *
     * @param value allowed object is
     *              {@link AdjustmentRequestDTO }
     */
    public void setArg0(AdjustmentRequestDTO value) {
        this.arg0 = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reservationUseRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationUseRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reservationGeneralInfo" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationGeneralRequestDTO" minOccurs="0"/>
 *         &lt;element name="reservationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usageType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationUseRequestDTO", propOrder = {
        "reservationGeneralInfo",
        "reservationId",
        "usageType"
})
public class ReservationUseRequestDTO {

    protected co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO reservationGeneralInfo;
    protected String reservationId;
    protected int usageType;

    /**
     * Gets the value of the reservationGeneralInfo property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO getReservationGeneralInfo() {
        return reservationGeneralInfo;
    }

    /**
     * Sets the value of the reservationGeneralInfo property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralRequestDTO }
     */
    public void setReservationGeneralInfo(ReservationGeneralRequestDTO value) {
        this.reservationGeneralInfo = value;
    }

    /**
     * Gets the value of the reservationId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReservationId() {
        return reservationId;
    }

    /**
     * Sets the value of the reservationId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReservationId(String value) {
        this.reservationId = value;
    }

    /**
     * Gets the value of the usageType property.
     */
    public int getUsageType() {
        return usageType;
    }

    /**
     * Sets the value of the usageType property.
     */
    public void setUsageType(int value) {
        this.usageType = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rangeDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="rangeDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="maxValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="minValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="profileHlrValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rangeDTO", propOrder = {
        "maxValue",
        "minValue",
        "profileHlrValue"
})
public class RangeDTO {

    protected Long maxValue;
    protected Long minValue;
    protected String profileHlrValue;

    /**
     * Gets the value of the maxValue property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the value of the maxValue property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setMaxValue(Long value) {
        this.maxValue = value;
    }

    /**
     * Gets the value of the minValue property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getMinValue() {
        return minValue;
    }

    /**
     * Sets the value of the minValue property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setMinValue(Long value) {
        this.minValue = value;
    }

    /**
     * Gets the value of the profileHlrValue property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getProfileHlrValue() {
        return profileHlrValue;
    }

    /**
     * Sets the value of the profileHlrValue property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setProfileHlrValue(String value) {
        this.profileHlrValue = value;
    }

}

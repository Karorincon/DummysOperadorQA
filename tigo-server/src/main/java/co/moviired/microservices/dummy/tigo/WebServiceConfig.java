package co.moviired.microservices.dummy.tigo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.io.File;

@EnableWs
@Configuration
@PropertySource(value = "file:${configuration_external_path}/participant.properties", ignoreResourceNotFound = true)
public class WebServiceConfig extends WsConfigurerAdapter {

    @Value("${configuration_external_path}")
    private String configurationExternalPath;

    @Value("${SERVICE_NAME}")
    private String contextPath;

    private static final String TARGET_NAMESPACE ="http://ws.webscp.gatewaytigo.tigo.com.co/";

    private static final Logger LOGGER = LogManager.getLogger(WebServiceConfig.class.getName());


    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/"+contextPath+"/*");
    }

    @Bean(name = "GatewayTigoScpWSMvno")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema gatewayTigoScpWSMvnoSchema) {
        LOGGER.info("-----------------------GatewayTigoScpWSMvno-----------------");
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("GatewayTigoPortTypeService");
        definition.setTargetNamespace(TARGET_NAMESPACE);
        definition.setLocationUri("/"+contextPath);
        definition.setSchema(gatewayTigoScpWSMvnoSchema);
        return definition;
    }

    /*
    @Bean(name = "GatewayTigoScpWSMvno")
    public Wsdl11Definition defaultWsdl11Definition(XsdSchema customersSchema){

        SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
        wsdl11Definition.
        File f= new File(this.configurationExternalPath+"/wsdl/GatewayTigoScpWSMvno.wsdl");
        LOGGER.info("f => "+f.getAbsolutePath());
        wsdl11Definition.setWsdl(new FileSystemResource(f));

        return wsdl11Definition;

    }
    */

    @Bean
    public XsdSchema gatewayTigoScpWSMvnoSchema() {
        File f= new File(this.configurationExternalPath+"/wsdl/GatewayTigoScpWSMvno.xsd");
        return new SimpleXsdSchema(new FileSystemResource(f));
    }

}
package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java class for bonusDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="bonusDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusValue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="highValue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="lowValue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="vigency" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bonusDTO", propOrder = {
        "bonusValue",
        "highValue",
        "lowValue",
        "vigency"
})
public class BonusDTO {

    protected BigDecimal bonusValue;
    protected BigDecimal highValue;
    protected BigDecimal lowValue;
    protected int vigency;

    /**
     * Gets the value of the bonusValue property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getBonusValue() {
        return bonusValue;
    }

    /**
     * Sets the value of the bonusValue property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setBonusValue(BigDecimal value) {
        this.bonusValue = value;
    }

    /**
     * Gets the value of the highValue property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getHighValue() {
        return highValue;
    }

    /**
     * Sets the value of the highValue property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setHighValue(BigDecimal value) {
        this.highValue = value;
    }

    /**
     * Gets the value of the lowValue property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getLowValue() {
        return lowValue;
    }

    /**
     * Sets the value of the lowValue property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setLowValue(BigDecimal value) {
        this.lowValue = value;
    }

    /**
     * Gets the value of the vigency property.
     */
    public int getVigency() {
        return vigency;
    }

    /**
     * Sets the value of the vigency property.
     */
    public void setVigency(int value) {
        this.vigency = value;
    }

}

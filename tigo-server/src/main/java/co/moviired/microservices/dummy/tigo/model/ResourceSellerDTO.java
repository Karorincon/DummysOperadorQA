package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.ExpirationDTO;
import co.moviired.microservices.dummy.tigo.model.IdDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for resourceSellerDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="resourceSellerDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amountBuyer" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="expirationColl" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}expirationDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="expirationDaysSeller" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="giveResource" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="partitionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="partitionIdBuyer" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="partitionSeller" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partitionTypeSeller" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="resourceId" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="service" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}serviceResourceDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resourceSellerDTO", propOrder = {
        "amountBuyer",
        "expirationColl",
        "expirationDaysSeller",
        "giveResource",
        "partitionId",
        "partitionIdBuyer",
        "partitionSeller",
        "partitionTypeSeller",
        "price",
        "resourceId",
        "service"
})
public class ResourceSellerDTO {

    protected BigDecimal amountBuyer;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.ExpirationDTO> expirationColl;
    protected Long expirationDaysSeller;
    protected Boolean giveResource;
    protected Integer partitionId;
    protected Integer partitionIdBuyer;
    protected String partitionSeller;
    protected String partitionTypeSeller;
    protected BigDecimal price;
    protected co.moviired.microservices.dummy.tigo.model.IdDTO resourceId;
    protected ServiceResourceDTO service;

    /**
     * Gets the value of the amountBuyer property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getAmountBuyer() {
        return amountBuyer;
    }

    /**
     * Sets the value of the amountBuyer property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setAmountBuyer(BigDecimal value) {
        this.amountBuyer = value;
    }

    /**
     * Gets the value of the expirationColl property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expirationColl property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpirationColl().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.ExpirationDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.ExpirationDTO> getExpirationColl() {
        if (expirationColl == null) {
            expirationColl = new ArrayList<ExpirationDTO>();
        }
        return this.expirationColl;
    }

    /**
     * Gets the value of the expirationDaysSeller property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getExpirationDaysSeller() {
        return expirationDaysSeller;
    }

    /**
     * Sets the value of the expirationDaysSeller property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setExpirationDaysSeller(Long value) {
        this.expirationDaysSeller = value;
    }

    /**
     * Gets the value of the giveResource property.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isGiveResource() {
        return giveResource;
    }

    /**
     * Sets the value of the giveResource property.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setGiveResource(Boolean value) {
        this.giveResource = value;
    }

    /**
     * Gets the value of the partitionId property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPartitionId() {
        return partitionId;
    }

    /**
     * Sets the value of the partitionId property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPartitionId(Integer value) {
        this.partitionId = value;
    }

    /**
     * Gets the value of the partitionIdBuyer property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPartitionIdBuyer() {
        return partitionIdBuyer;
    }

    /**
     * Sets the value of the partitionIdBuyer property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPartitionIdBuyer(Integer value) {
        this.partitionIdBuyer = value;
    }

    /**
     * Gets the value of the partitionSeller property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPartitionSeller() {
        return partitionSeller;
    }

    /**
     * Sets the value of the partitionSeller property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPartitionSeller(String value) {
        this.partitionSeller = value;
    }

    /**
     * Gets the value of the partitionTypeSeller property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPartitionTypeSeller() {
        return partitionTypeSeller;
    }

    /**
     * Sets the value of the partitionTypeSeller property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPartitionTypeSeller(String value) {
        this.partitionTypeSeller = value;
    }

    /**
     * Gets the value of the price property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    /**
     * Gets the value of the resourceId property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.IdDTO getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public void setResourceId(IdDTO value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the service property.
     *
     * @return possible object is
     * {@link ServiceResourceDTO }
     */
    public ServiceResourceDTO getService() {
        return service;
    }

    /**
     * Sets the value of the service property.
     *
     * @param value allowed object is
     *              {@link ServiceResourceDTO }
     */
    public void setService(ServiceResourceDTO value) {
        this.service = value;
    }

}

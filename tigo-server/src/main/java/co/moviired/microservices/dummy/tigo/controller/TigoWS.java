package co.moviired.microservices.dummy.tigo.controller;

import co.moviired.microservices.dummy.tigo.model.*;
import co.moviired.microservices.dummy.tigo.service.ITigoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.bind.JAXBElement;

@Endpoint
public class TigoWS {

    private static final String TARGET_NAMESPACE ="http://ws.webscp.gatewaytigo.tigo.com.co/";
    private static final Logger LOGGER = LogManager.getLogger(TigoWS.class.getName());
    private ITigoService tigoService;

    @Autowired
    public TigoWS(ITigoService tigoService){
        this.tigoService = tigoService;
    }

    @PayloadRoot(namespace = TARGET_NAMESPACE, localPart = "eRecharge")
    @ResponsePayload
    public JAXBElement<ERechargeResponse> eRecharge(@RequestPayload JAXBElement<ERecharge> request) throws BusinessException_Exception,
            GatewayPrepayWSException_Exception{

        ObjectFactory factory = new ObjectFactory();
        ERechargeResponse response = factory.createERechargeResponse();
        ERechargeResponseDTO  dto;

        try{
            LOGGER.info("request1 => "+request);
            LOGGER.info("request1.getValue => "+request.getValue());
            LOGGER.info("request1.getValue().getArg0 => "+request.getValue().getArg0());

            dto = this.tigoService.processRecharge(request.getValue().getArg0());

        }catch (Exception ex){
            dto = factory.createERechargeResponseDTO();
            IdDTO id = factory.createIdDTO();
            id.setId("99");
            id.setName("TRANSACCION RECHAZADA");
            dto.setMessage("Ha ocurrido un error");
            dto.setBonusStatus(id);

            LOGGER.error("error = > ", ex);
        }

        response.setReturn(dto);
        return factory.createERechargeResponse(response) ;


    }


    @PayloadRoot(namespace = TARGET_NAMESPACE, localPart = "eRechargeReversal")
    @ResponsePayload
    public JAXBElement<ERechargeReversalResponse> eRechargeReversal(@RequestPayload JAXBElement<ERechargeReversal> request) throws BusinessException_Exception,
            GatewayPrepayWSException_Exception{

        ObjectFactory factory = new ObjectFactory();
        ERechargeReversalResponse response = factory.createERechargeReversalResponse();
        ERechargeRevResponseDTO  dto = factory.createERechargeRevResponseDTO();

        try{
            LOGGER.info("request1 => "+request);
            LOGGER.info("request1.getValue => "+request.getValue());
            LOGGER.info("request1.getValue().getArg0 => "+request.getValue().getArg0());

            dto = this.tigoService.processReversal(request.getValue().getArg0());

        }catch (Exception ex){
            dto = factory.createERechargeRevResponseDTO();
            IdDTO id = factory.createIdDTO();
            id.setId("99");
            id.setName("TRANSACCION RECHAZADA");
            dto.setMessage("Ha ocurrido un error");
            dto.setBonusStatus(id);

            LOGGER.error("error = > ", ex);
        }

        response.setReturn(dto);
        return factory.createERechargeReversalResponse(response) ;


    }

}

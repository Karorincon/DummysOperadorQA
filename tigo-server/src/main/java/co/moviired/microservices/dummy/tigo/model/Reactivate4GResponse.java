package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reactivate4GResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reactivate4GResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reactivate4GResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reactivate4GResponse", propOrder = {
        "_return"
})
public class Reactivate4GResponse {

    @XmlElement(name = "return")
    protected Reactivate4GResponseDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link Reactivate4GResponseDTO }
     */
    public Reactivate4GResponseDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link Reactivate4GResponseDTO }
     */
    public void setReturn(Reactivate4GResponseDTO value) {
        this._return = value;
    }

}

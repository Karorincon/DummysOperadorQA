package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO;
import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eTransferResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eTransferResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="balanceClient" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}balanceResponseDTO" minOccurs="0"/>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eTransferResponseDTO", propOrder = {
        "authorizationNumber",
        "balanceClient",
        "bonusBalance",
        "dedicatedMoneyBalance",
        "realBalance",
        "resourceBalance",
        "transactionId"
})
public class ETransferResponseDTO {

    protected String authorizationNumber;
    protected co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO balanceClient;
    protected MoneyDTO bonusBalance;
    protected co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO dedicatedMoneyBalance;
    protected MoneyDTO realBalance;
    protected ResourceDTO resourceBalance;
    protected String transactionId;

    /**
     * Gets the value of the authorizationNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    /**
     * Sets the value of the authorizationNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthorizationNumber(String value) {
        this.authorizationNumber = value;
    }

    /**
     * Gets the value of the balanceClient property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO getBalanceClient() {
        return balanceClient;
    }

    /**
     * Sets the value of the balanceClient property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO }
     */
    public void setBalanceClient(BalanceResponseDTO value) {
        this.balanceClient = value;
    }

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO getDedicatedMoneyBalance() {
        return dedicatedMoneyBalance;
    }

    /**
     * Sets the value of the dedicatedMoneyBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public void setDedicatedMoneyBalance(DedicatedMoneyDTO value) {
        this.dedicatedMoneyBalance = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * @return possible object is
     * {@link ResourceDTO }
     */
    public ResourceDTO getResourceBalance() {
        return resourceBalance;
    }

    /**
     * Sets the value of the resourceBalance property.
     *
     * @param value allowed object is
     *              {@link ResourceDTO }
     */
    public void setResourceBalance(ResourceDTO value) {
        this.resourceBalance = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

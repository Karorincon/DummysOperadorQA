package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.*;
import co.moviired.microservices.dummy.tigo.model.AdjustmentRequestDTO;
import co.moviired.microservices.dummy.tigo.model.AdjustmentResponseDTO;
import co.moviired.microservices.dummy.tigo.model.AdviceOfChargeRequestDTO;
import co.moviired.microservices.dummy.tigo.model.AdviceOfChargeResponseDTO;
import co.moviired.microservices.dummy.tigo.model.BalanceRequestDTO;
import co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO;
import co.moviired.microservices.dummy.tigo.model.BalanceToResourceRequestDTO;
import co.moviired.microservices.dummy.tigo.model.BalanceToResourceResponseDTO;
import co.moviired.microservices.dummy.tigo.model.BonusDTO;
import co.moviired.microservices.dummy.tigo.model.BusinessException;
import co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO;
import co.moviired.microservices.dummy.tigo.model.CreditRequestDTO;
import co.moviired.microservices.dummy.tigo.model.CreditResponseDTO;
import co.moviired.microservices.dummy.tigo.model.DebitRequestDTO;
import co.moviired.microservices.dummy.tigo.model.DebitResponseDTO;
import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.ERechargeResponseDTO;
import co.moviired.microservices.dummy.tigo.model.ERechargeRevRequestDTO;
import co.moviired.microservices.dummy.tigo.model.ERechargeRevResponseDTO;
import co.moviired.microservices.dummy.tigo.model.ETransferRequestDTO;
import co.moviired.microservices.dummy.tigo.model.ETransferResponseDTO;
import co.moviired.microservices.dummy.tigo.model.EventRequestDTO;
import co.moviired.microservices.dummy.tigo.model.EventResponseDTO;
import co.moviired.microservices.dummy.tigo.model.ExpirationDTO;
import co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException;
import co.moviired.microservices.dummy.tigo.model.IdDTO;
import co.moviired.microservices.dummy.tigo.model.LocationDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;
import co.moviired.microservices.dummy.tigo.model.NetworkBearerIDDTO;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the co.moviired.microservices.dummy.tigo.model package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProvideResources_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideResources");
    private final static QName _VoucherRechargeResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "voucherRechargeResponse");
    private final static QName _AdjustmentResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "adjustmentResponse");
    private final static QName _ERechargeReversal_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eRechargeReversal");
    private final static QName _ETransferResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eTransferResponse");
    private final static QName _ReservationCancel_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationCancel");
    private final static QName _Balance_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "balance");
    private final static QName _ProvideProperties_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideProperties");
    private final static QName _GetSpidOfMsisdn_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "getSpidOfMsisdn");
    private final static QName _ProvideBonus_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideBonus");
    private final static QName _BusinessException_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "BusinessException");
    private final static QName _ProvideUsers_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideUsers");
    private final static QName _ReservationUseResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationUseResponse");
    private final static QName _Credit_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "credit");
    private final static QName _GetXmlMvnoGroups_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "getXmlMvnoGroups");
    private final static QName _ERechargeResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eRechargeResponse");
    private final static QName _ProvideUsersResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideUsersResponse");
    private final static QName _Reservation_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservation");
    private final static QName _ReservationQuery_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationQuery");
    private final static QName _ProvidePropertiesResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "providePropertiesResponse");
    private final static QName _ReservationModify_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationModify");
    private final static QName _ReservationModifyResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationModifyResponse");
    private final static QName _ApplicationEventResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "applicationEventResponse");
    private final static QName _RechargeReversalResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "rechargeReversalResponse");
    private final static QName _ConsultPropertiesResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultPropertiesResponse");
    private final static QName _AdviceOfChargeResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "adviceOfChargeResponse");
    private final static QName _ApplicationEvent_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "applicationEvent");
    private final static QName _ProvideXmlMvnoGroups_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideXmlMvnoGroups");
    private final static QName _Transfer_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "transfer");
    private final static QName _EventAuthorizationResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eventAuthorizationResponse");
    private final static QName _GetXmlMvnoGroupsResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "getXmlMvnoGroupsResponse");
    private final static QName _GetSpidOfMsisdnResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "getSpidOfMsisdnResponse");
    private final static QName _Reactivate4G_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reactivate4G");
    private final static QName _GatewayPrepayWSException_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "GatewayPrepayWSException");
    private final static QName _Activate4GResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "activate4GResponse");
    private final static QName _Adjustment_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "adjustment");
    private final static QName _ReservationUse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationUse");
    private final static QName _AdviceOfCharge_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "adviceOfCharge");
    private final static QName _ETransfer_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eTransfer");
    private final static QName _ProvideResourcesResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideResourcesResponse");
    private final static QName _RechargeResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "rechargeResponse");
    private final static QName _BalanceToResource_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "balanceToResource");
    private final static QName _Deactivate4G_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "deactivate4G");
    private final static QName _ERechargeReversalResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eRechargeReversalResponse");
    private final static QName _DebitResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "debitResponse");
    private final static QName _EventAuthorization_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eventAuthorization");
    private final static QName _ConsultBonusResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultBonusResponse");
    private final static QName _Recharge_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "recharge");
    private final static QName _ReservationResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationResponse");
    private final static QName _ProvideXmlMvnoGroupsResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideXmlMvnoGroupsResponse");
    private final static QName _ConsultProperties_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultProperties");
    private final static QName _ConsultEUsersPromotionsResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultEUsersPromotionsResponse");
    private final static QName _Debit_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "debit");
    private final static QName _ConsultResourcesResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultResourcesResponse");
    private final static QName _ReservationCancelResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationCancelResponse");
    private final static QName _TransferResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "transferResponse");
    private final static QName _ConsultEUsersPromotions_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultEUsersPromotions");
    private final static QName _RechargeReversal_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "rechargeReversal");
    private final static QName _CreditResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "creditResponse");
    private final static QName _BalanceResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "balanceResponse");
    private final static QName _ConsultUsersResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultUsersResponse");
    private final static QName _EBalance_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eBalance");
    private final static QName _ERecharge_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eRecharge");
    private final static QName _EBalanceResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "eBalanceResponse");
    private final static QName _Activate4G_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "activate4G");
    private final static QName _Groups_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "groups");
    private final static QName _ConsultBonus_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultBonus");
    private final static QName _ProvideBonusResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "provideBonusResponse");
    private final static QName _ConsultUsers_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultUsers");
    private final static QName _ReservationQueryResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reservationQueryResponse");
    private final static QName _BalanceToResourceResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "balanceToResourceResponse");
    private final static QName _Reactivate4GResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "reactivate4GResponse");
    private final static QName _ConsultResources_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "consultResources");
    private final static QName _VoucherRecharge_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "voucherRecharge");
    private final static QName _Deactivate4GResponse_QNAME = new QName("http://ws.webscp.gatewaytigo.tigo.com.co/", "deactivate4GResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.moviired.microservices.dummy.tigo.model
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AdjustmentResponse }
     */
    public AdjustmentResponse createAdjustmentResponse() {
        return new AdjustmentResponse();
    }

    /**
     * Create an instance of {@link ERechargeReversal }
     */
    public ERechargeReversal createERechargeReversal() {
        return new ERechargeReversal();
    }

    /**
     * Create an instance of {@link ProvideResources }
     */
    public ProvideResources createProvideResources() {
        return new ProvideResources();
    }

    /**
     * Create an instance of {@link VoucherRechargeResponse }
     */
    public VoucherRechargeResponse createVoucherRechargeResponse() {
        return new VoucherRechargeResponse();
    }

    /**
     * Create an instance of {@link ReservationCancel }
     */
    public ReservationCancel createReservationCancel() {
        return new ReservationCancel();
    }

    /**
     * Create an instance of {@link ETransferResponse }
     */
    public ETransferResponse createETransferResponse() {
        return new ETransferResponse();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.BusinessException }
     */
    public co.moviired.microservices.dummy.tigo.model.BusinessException createBusinessException() {
        return new co.moviired.microservices.dummy.tigo.model.BusinessException();
    }

    /**
     * Create an instance of {@link GetSpidOfMsisdn }
     */
    public GetSpidOfMsisdn createGetSpidOfMsisdn() {
        return new GetSpidOfMsisdn();
    }

    /**
     * Create an instance of {@link ProvideBonus }
     */
    public ProvideBonus createProvideBonus() {
        return new ProvideBonus();
    }

    /**
     * Create an instance of {@link Balance }
     */
    public Balance createBalance() {
        return new Balance();
    }

    /**
     * Create an instance of {@link ProvideProperties }
     */
    public ProvideProperties createProvideProperties() {
        return new ProvideProperties();
    }

    /**
     * Create an instance of {@link ProvidePropertiesResponse }
     */
    public ProvidePropertiesResponse createProvidePropertiesResponse() {
        return new ProvidePropertiesResponse();
    }

    /**
     * Create an instance of {@link ReservationModify }
     */
    public ReservationModify createReservationModify() {
        return new ReservationModify();
    }

    /**
     * Create an instance of {@link ProvideUsersResponse }
     */
    public ProvideUsersResponse createProvideUsersResponse() {
        return new ProvideUsersResponse();
    }

    /**
     * Create an instance of {@link Reservation }
     */
    public Reservation createReservation() {
        return new Reservation();
    }

    /**
     * Create an instance of {@link ReservationQuery }
     */
    public ReservationQuery createReservationQuery() {
        return new ReservationQuery();
    }

    /**
     * Create an instance of {@link Credit }
     */
    public Credit createCredit() {
        return new Credit();
    }

    /**
     * Create an instance of {@link GetXmlMvnoGroups }
     */
    public GetXmlMvnoGroups createGetXmlMvnoGroups() {
        return new GetXmlMvnoGroups();
    }

    /**
     * Create an instance of {@link ProvideUsers }
     */
    public ProvideUsers createProvideUsers() {
        return new ProvideUsers();
    }

    /**
     * Create an instance of {@link ReservationUseResponse }
     */
    public ReservationUseResponse createReservationUseResponse() {
        return new ReservationUseResponse();
    }

    /**
     * Create an instance of {@link ERechargeResponse }
     */
    public ERechargeResponse createERechargeResponse() {
        return new ERechargeResponse();
    }

    /**
     * Create an instance of {@link ApplicationEventResponse }
     */
    public ApplicationEventResponse createApplicationEventResponse() {
        return new ApplicationEventResponse();
    }

    /**
     * Create an instance of {@link ReservationModifyResponse }
     */
    public ReservationModifyResponse createReservationModifyResponse() {
        return new ReservationModifyResponse();
    }

    /**
     * Create an instance of {@link ConsultPropertiesResponse }
     */
    public ConsultPropertiesResponse createConsultPropertiesResponse() {
        return new ConsultPropertiesResponse();
    }

    /**
     * Create an instance of {@link RechargeReversalResponse }
     */
    public RechargeReversalResponse createRechargeReversalResponse() {
        return new RechargeReversalResponse();
    }

    /**
     * Create an instance of {@link ProvideXmlMvnoGroups }
     */
    public ProvideXmlMvnoGroups createProvideXmlMvnoGroups() {
        return new ProvideXmlMvnoGroups();
    }

    /**
     * Create an instance of {@link Transfer }
     */
    public Transfer createTransfer() {
        return new Transfer();
    }

    /**
     * Create an instance of {@link AdviceOfChargeResponse }
     */
    public AdviceOfChargeResponse createAdviceOfChargeResponse() {
        return new AdviceOfChargeResponse();
    }

    /**
     * Create an instance of {@link ApplicationEvent }
     */
    public ApplicationEvent createApplicationEvent() {
        return new ApplicationEvent();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException }
     */
    public co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException createGatewayPrepayWSException() {
        return new co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException();
    }

    /**
     * Create an instance of {@link Activate4GResponse }
     */
    public Activate4GResponse createActivate4GResponse() {
        return new Activate4GResponse();
    }

    /**
     * Create an instance of {@link Reactivate4G }
     */
    public Reactivate4G createReactivate4G() {
        return new Reactivate4G();
    }

    /**
     * Create an instance of {@link Adjustment }
     */
    public Adjustment createAdjustment() {
        return new Adjustment();
    }

    /**
     * Create an instance of {@link EventAuthorizationResponse }
     */
    public EventAuthorizationResponse createEventAuthorizationResponse() {
        return new EventAuthorizationResponse();
    }

    /**
     * Create an instance of {@link GetSpidOfMsisdnResponse }
     */
    public GetSpidOfMsisdnResponse createGetSpidOfMsisdnResponse() {
        return new GetSpidOfMsisdnResponse();
    }

    /**
     * Create an instance of {@link GetXmlMvnoGroupsResponse }
     */
    public GetXmlMvnoGroupsResponse createGetXmlMvnoGroupsResponse() {
        return new GetXmlMvnoGroupsResponse();
    }

    /**
     * Create an instance of {@link AdviceOfCharge }
     */
    public AdviceOfCharge createAdviceOfCharge() {
        return new AdviceOfCharge();
    }

    /**
     * Create an instance of {@link ETransfer }
     */
    public ETransfer createETransfer() {
        return new ETransfer();
    }

    /**
     * Create an instance of {@link ProvideResourcesResponse }
     */
    public ProvideResourcesResponse createProvideResourcesResponse() {
        return new ProvideResourcesResponse();
    }

    /**
     * Create an instance of {@link RechargeResponse }
     */
    public RechargeResponse createRechargeResponse() {
        return new RechargeResponse();
    }

    /**
     * Create an instance of {@link ReservationUse }
     */
    public ReservationUse createReservationUse() {
        return new ReservationUse();
    }

    /**
     * Create an instance of {@link EventAuthorization }
     */
    public EventAuthorization createEventAuthorization() {
        return new EventAuthorization();
    }

    /**
     * Create an instance of {@link DebitResponse }
     */
    public DebitResponse createDebitResponse() {
        return new DebitResponse();
    }

    /**
     * Create an instance of {@link BalanceToResource }
     */
    public BalanceToResource createBalanceToResource() {
        return new BalanceToResource();
    }

    /**
     * Create an instance of {@link Deactivate4G }
     */
    public Deactivate4G createDeactivate4G() {
        return new Deactivate4G();
    }

    /**
     * Create an instance of {@link ERechargeReversalResponse }
     */
    public ERechargeReversalResponse createERechargeReversalResponse() {
        return new ERechargeReversalResponse();
    }

    /**
     * Create an instance of {@link ReservationResponse }
     */
    public ReservationResponse createReservationResponse() {
        return new ReservationResponse();
    }

    /**
     * Create an instance of {@link ConsultProperties }
     */
    public ConsultProperties createConsultProperties() {
        return new ConsultProperties();
    }

    /**
     * Create an instance of {@link ProvideXmlMvnoGroupsResponse }
     */
    public ProvideXmlMvnoGroupsResponse createProvideXmlMvnoGroupsResponse() {
        return new ProvideXmlMvnoGroupsResponse();
    }

    /**
     * Create an instance of {@link ConsultBonusResponse }
     */
    public ConsultBonusResponse createConsultBonusResponse() {
        return new ConsultBonusResponse();
    }

    /**
     * Create an instance of {@link Recharge }
     */
    public Recharge createRecharge() {
        return new Recharge();
    }

    /**
     * Create an instance of {@link ConsultResourcesResponse }
     */
    public ConsultResourcesResponse createConsultResourcesResponse() {
        return new ConsultResourcesResponse();
    }

    /**
     * Create an instance of {@link ReservationCancelResponse }
     */
    public ReservationCancelResponse createReservationCancelResponse() {
        return new ReservationCancelResponse();
    }

    /**
     * Create an instance of {@link TransferResponse }
     */
    public TransferResponse createTransferResponse() {
        return new TransferResponse();
    }

    /**
     * Create an instance of {@link RechargeReversal }
     */
    public RechargeReversal createRechargeReversal() {
        return new RechargeReversal();
    }

    /**
     * Create an instance of {@link ConsultEUsersPromotions }
     */
    public ConsultEUsersPromotions createConsultEUsersPromotions() {
        return new ConsultEUsersPromotions();
    }

    /**
     * Create an instance of {@link ConsultEUsersPromotionsResponse }
     */
    public ConsultEUsersPromotionsResponse createConsultEUsersPromotionsResponse() {
        return new ConsultEUsersPromotionsResponse();
    }

    /**
     * Create an instance of {@link Debit }
     */
    public Debit createDebit() {
        return new Debit();
    }

    /**
     * Create an instance of {@link EBalance }
     */
    public EBalance createEBalance() {
        return new EBalance();
    }

    /**
     * Create an instance of {@link ERecharge }
     */
    public ERecharge createERecharge() {
        return new ERecharge();
    }

    /**
     * Create an instance of {@link EBalanceResponse }
     */
    public EBalanceResponse createEBalanceResponse() {
        return new EBalanceResponse();
    }

    /**
     * Create an instance of {@link BalanceResponse }
     */
    public BalanceResponse createBalanceResponse() {
        return new BalanceResponse();
    }

    /**
     * Create an instance of {@link ConsultUsersResponse }
     */
    public ConsultUsersResponse createConsultUsersResponse() {
        return new ConsultUsersResponse();
    }

    /**
     * Create an instance of {@link CreditResponse }
     */
    public CreditResponse createCreditResponse() {
        return new CreditResponse();
    }

    /**
     * Create an instance of {@link ConsultBonus }
     */
    public ConsultBonus createConsultBonus() {
        return new ConsultBonus();
    }

    /**
     * Create an instance of {@link ProvideBonusResponse }
     */
    public ProvideBonusResponse createProvideBonusResponse() {
        return new ProvideBonusResponse();
    }

    /**
     * Create an instance of {@link GroupsMvnoDTO }
     */
    public GroupsMvnoDTO createGroupsMvnoDTO() {
        return new GroupsMvnoDTO();
    }

    /**
     * Create an instance of {@link Activate4G }
     */
    public Activate4G createActivate4G() {
        return new Activate4G();
    }

    /**
     * Create an instance of {@link BalanceToResourceResponse }
     */
    public BalanceToResourceResponse createBalanceToResourceResponse() {
        return new BalanceToResourceResponse();
    }

    /**
     * Create an instance of {@link Reactivate4GResponse }
     */
    public Reactivate4GResponse createReactivate4GResponse() {
        return new Reactivate4GResponse();
    }

    /**
     * Create an instance of {@link ConsultUsers }
     */
    public ConsultUsers createConsultUsers() {
        return new ConsultUsers();
    }

    /**
     * Create an instance of {@link ReservationQueryResponse }
     */
    public ReservationQueryResponse createReservationQueryResponse() {
        return new ReservationQueryResponse();
    }

    /**
     * Create an instance of {@link Deactivate4GResponse }
     */
    public Deactivate4GResponse createDeactivate4GResponse() {
        return new Deactivate4GResponse();
    }

    /**
     * Create an instance of {@link ConsultResources }
     */
    public ConsultResources createConsultResources() {
        return new ConsultResources();
    }

    /**
     * Create an instance of {@link VoucherRecharge }
     */
    public VoucherRecharge createVoucherRecharge() {
        return new VoucherRecharge();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.LocationDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.LocationDTO createLocationDTO() {
        return new LocationDTO();
    }

    /**
     * Create an instance of {@link GroupMvnoDTO }
     */
    public GroupMvnoDTO createGroupMvnoDTO() {
        return new GroupMvnoDTO();
    }

    /**
     * Create an instance of {@link ProvisioningUsersResponseDTO }
     */
    public ProvisioningUsersResponseDTO createProvisioningUsersResponseDTO() {
        return new ProvisioningUsersResponseDTO();
    }

    /**
     * Create an instance of {@link TransferRequestDTO }
     */
    public TransferRequestDTO createTransferRequestDTO() {
        return new TransferRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ExpirationDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ExpirationDTO createExpirationDTO() {
        return new ExpirationDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.AdviceOfChargeResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.AdviceOfChargeResponseDTO createAdviceOfChargeResponseDTO() {
        return new AdviceOfChargeResponseDTO();
    }

    /**
     * Create an instance of {@link Reactivate4GRequestDTO }
     */
    public Reactivate4GRequestDTO createReactivate4GRequestDTO() {
        return new Reactivate4GRequestDTO();
    }

    /**
     * Create an instance of {@link ReservationUseRequestDTO }
     */
    public ReservationUseRequestDTO createReservationUseRequestDTO() {
        return new ReservationUseRequestDTO();
    }

    /**
     * Create an instance of {@link ReservationUseResponseDTO }
     */
    public ReservationUseResponseDTO createReservationUseResponseDTO() {
        return new ReservationUseResponseDTO();
    }

    /**
     * Create an instance of {@link ReservationResponseDTO }
     */
    public ReservationResponseDTO createReservationResponseDTO() {
        return new ReservationResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO createBalanceResponseDTO() {
        return new BalanceResponseDTO();
    }

    /**
     * Create an instance of {@link ReservationRequestDTO }
     */
    public ReservationRequestDTO createReservationRequestDTO() {
        return new ReservationRequestDTO();
    }

    /**
     * Create an instance of {@link PropertyDTO }
     */
    public PropertyDTO createPropertyDTO() {
        return new PropertyDTO();
    }

    /**
     * Create an instance of {@link Activate4GRequestDTO }
     */
    public Activate4GRequestDTO createActivate4GRequestDTO() {
        return new Activate4GRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.BonusDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BonusDTO createBonusDTO() {
        return new BonusDTO();
    }

    /**
     * Create an instance of {@link SellerDTO }
     */
    public SellerDTO createSellerDTO() {
        return new SellerDTO();
    }

    /**
     * Create an instance of {@link ProvisioningResourcesResponseDTO }
     */
    public ProvisioningResourcesResponseDTO createProvisioningResourcesResponseDTO() {
        return new ProvisioningResourcesResponseDTO();
    }

    /**
     * Create an instance of {@link RechargeResponseDTO }
     */
    public RechargeResponseDTO createRechargeResponseDTO() {
        return new RechargeResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.IdDTO createIdDTO() {
        return new IdDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.DebitRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.DebitRequestDTO createDebitRequestDTO() {
        return new DebitRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ERechargeResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ERechargeResponseDTO createERechargeResponseDTO() {
        return new ERechargeResponseDTO();
    }

    /**
     * Create an instance of {@link VoucherRechargeResponseDTO }
     */
    public VoucherRechargeResponseDTO createVoucherRechargeResponseDTO() {
        return new VoucherRechargeResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.AdjustmentResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.AdjustmentResponseDTO createAdjustmentResponseDTO() {
        return new AdjustmentResponseDTO();
    }

    /**
     * Create an instance of {@link GetSpidOfMsisdnResponseDTO }
     */
    public GetSpidOfMsisdnResponseDTO createGetSpidOfMsisdnResponseDTO() {
        return new GetSpidOfMsisdnResponseDTO();
    }

    /**
     * Create an instance of {@link Activate4GResponseDTO }
     */
    public Activate4GResponseDTO createActivate4GResponseDTO() {
        return new Activate4GResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ETransferResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ETransferResponseDTO createETransferResponseDTO() {
        return new ETransferResponseDTO();
    }

    /**
     * Create an instance of {@link ProvisioningUsersRequestDTO }
     */
    public ProvisioningUsersRequestDTO createProvisioningUsersRequestDTO() {
        return new ProvisioningUsersRequestDTO();
    }

    /**
     * Create an instance of {@link ReservationGeneralResponseDTO }
     */
    public ReservationGeneralResponseDTO createReservationGeneralResponseDTO() {
        return new ReservationGeneralResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.BalanceToResourceResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BalanceToResourceResponseDTO createBalanceToResourceResponseDTO() {
        return new BalanceToResourceResponseDTO();
    }

    /**
     * Create an instance of {@link ProvisioningBonusRequestDTO }
     */
    public ProvisioningBonusRequestDTO createProvisioningBonusRequestDTO() {
        return new ProvisioningBonusRequestDTO();
    }

    /**
     * Create an instance of {@link ReturnCodeDTO }
     */
    public ReturnCodeDTO createReturnCodeDTO() {
        return new ReturnCodeDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ERechargeRevRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ERechargeRevRequestDTO createERechargeRevRequestDTO() {
        return new ERechargeRevRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.EventResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.EventResponseDTO createEventResponseDTO() {
        return new EventResponseDTO();
    }

    /**
     * Create an instance of {@link ResourceDTO }
     */
    public ResourceDTO createResourceDTO() {
        return new ResourceDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ERechargeRevResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ERechargeRevResponseDTO createERechargeRevResponseDTO() {
        return new ERechargeRevResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.DebitResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.DebitResponseDTO createDebitResponseDTO() {
        return new DebitResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.EventRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.EventRequestDTO createEventRequestDTO() {
        return new EventRequestDTO();
    }

    /**
     * Create an instance of {@link RechargeRequestDTO }
     */
    public RechargeRequestDTO createRechargeRequestDTO() {
        return new RechargeRequestDTO();
    }

    /**
     * Create an instance of {@link RangeDTO }
     */
    public RangeDTO createRangeDTO() {
        return new RangeDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ETransferRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ETransferRequestDTO createETransferRequestDTO() {
        return new ETransferRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO createControlledAccountBalanceDTO() {
        return new ControlledAccountBalanceDTO();
    }

    /**
     * Create an instance of {@link UserPromotionDTO }
     */
    public UserPromotionDTO createUserPromotionDTO() {
        return new UserPromotionDTO();
    }

    /**
     * Create an instance of {@link ReservationModifyRequestDTO }
     */
    public ReservationModifyRequestDTO createReservationModifyRequestDTO() {
        return new ReservationModifyRequestDTO();
    }

    /**
     * Create an instance of {@link TransferResponseDTO }
     */
    public TransferResponseDTO createTransferResponseDTO() {
        return new TransferResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.BalanceToResourceRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BalanceToResourceRequestDTO createBalanceToResourceRequestDTO() {
        return new BalanceToResourceRequestDTO();
    }

    /**
     * Create an instance of {@link ProvisioningBonusResponseDTO }
     */
    public ProvisioningBonusResponseDTO createProvisioningBonusResponseDTO() {
        return new ProvisioningBonusResponseDTO();
    }

    /**
     * Create an instance of {@link Deactivate4GRequestDTO }
     */
    public Deactivate4GRequestDTO createDeactivate4GRequestDTO() {
        return new Deactivate4GRequestDTO();
    }

    /**
     * Create an instance of {@link ServiceResourceDTO }
     */
    public ServiceResourceDTO createServiceResourceDTO() {
        return new ServiceResourceDTO();
    }

    /**
     * Create an instance of {@link ReservationModifyResponseDTO }
     */
    public ReservationModifyResponseDTO createReservationModifyResponseDTO() {
        return new ReservationModifyResponseDTO();
    }

    /**
     * Create an instance of {@link Reactivate4GResponseDTO }
     */
    public Reactivate4GResponseDTO createReactivate4GResponseDTO() {
        return new Reactivate4GResponseDTO();
    }

    /**
     * Create an instance of {@link ReservationCancelResponseDTO }
     */
    public ReservationCancelResponseDTO createReservationCancelResponseDTO() {
        return new ReservationCancelResponseDTO();
    }

    /**
     * Create an instance of {@link ReservationGeneralRequestDTO }
     */
    public ReservationGeneralRequestDTO createReservationGeneralRequestDTO() {
        return new ReservationGeneralRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.CreditResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.CreditResponseDTO createCreditResponseDTO() {
        return new CreditResponseDTO();
    }

    /**
     * Create an instance of {@link ERechargeRequestDTO }
     */
    public ERechargeRequestDTO createERechargeRequestDTO() {
        return new ERechargeRequestDTO();
    }

    /**
     * Create an instance of {@link VoucherRechargeRequestDTO }
     */
    public VoucherRechargeRequestDTO createVoucherRechargeRequestDTO() {
        return new VoucherRechargeRequestDTO();
    }

    /**
     * Create an instance of {@link PromotionDTO }
     */
    public PromotionDTO createPromotionDTO() {
        return new PromotionDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.NetworkBearerIDDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.NetworkBearerIDDTO createNetworkBearerIDDTO() {
        return new NetworkBearerIDDTO();
    }

    /**
     * Create an instance of {@link RechargeRevResponseDTO }
     */
    public RechargeRevResponseDTO createRechargeRevResponseDTO() {
        return new RechargeRevResponseDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO createMoneyDTO() {
        return new MoneyDTO();
    }

    /**
     * Create an instance of {@link RechargeRevRequestDTO }
     */
    public RechargeRevRequestDTO createRechargeRevRequestDTO() {
        return new RechargeRevRequestDTO();
    }

    /**
     * Create an instance of {@link ReservationQueryRequestDTO }
     */
    public ReservationQueryRequestDTO createReservationQueryRequestDTO() {
        return new ReservationQueryRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.BalanceRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BalanceRequestDTO createBalanceRequestDTO() {
        return new BalanceRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.AdjustmentRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.AdjustmentRequestDTO createAdjustmentRequestDTO() {
        return new AdjustmentRequestDTO();
    }

    /**
     * Create an instance of {@link GetSpidOfMsisdnRequestDTO }
     */
    public GetSpidOfMsisdnRequestDTO createGetSpidOfMsisdnRequestDTO() {
        return new GetSpidOfMsisdnRequestDTO();
    }

    /**
     * Create an instance of {@link PointOfSaleDTO }
     */
    public PointOfSaleDTO createPointOfSaleDTO() {
        return new PointOfSaleDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.AdviceOfChargeRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.AdviceOfChargeRequestDTO createAdviceOfChargeRequestDTO() {
        return new AdviceOfChargeRequestDTO();
    }

    /**
     * Create an instance of {@link ReservationQueryResponseDTO }
     */
    public ReservationQueryResponseDTO createReservationQueryResponseDTO() {
        return new ReservationQueryResponseDTO();
    }

    /**
     * Create an instance of {@link Deactivate4GResponseDTO }
     */
    public Deactivate4GResponseDTO createDeactivate4GResponseDTO() {
        return new Deactivate4GResponseDTO();
    }

    /**
     * Create an instance of {@link ReservationCancelRequestDTO }
     */
    public ReservationCancelRequestDTO createReservationCancelRequestDTO() {
        return new ReservationCancelRequestDTO();
    }

    /**
     * Create an instance of {@link ReservationQueryResponseItemDTO }
     */
    public ReservationQueryResponseItemDTO createReservationQueryResponseItemDTO() {
        return new ReservationQueryResponseItemDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.CreditRequestDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.CreditRequestDTO createCreditRequestDTO() {
        return new CreditRequestDTO();
    }

    /**
     * Create an instance of {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO createDedicatedMoneyDTO() {
        return new DedicatedMoneyDTO();
    }

    /**
     * Create an instance of {@link ResourceSellerDTO }
     */
    public ResourceSellerDTO createResourceSellerDTO() {
        return new ResourceSellerDTO();
    }

    /**
     * Create an instance of {@link ProvisioningResourcesRequestDTO }
     */
    public ProvisioningResourcesRequestDTO createProvisioningResourcesRequestDTO() {
        return new ProvisioningResourcesRequestDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideResources }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideResources")
    public JAXBElement<ProvideResources> createProvideResources(ProvideResources value) {
        return new JAXBElement<ProvideResources>(_ProvideResources_QNAME, ProvideResources.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherRechargeResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "voucherRechargeResponse")
    public JAXBElement<VoucherRechargeResponse> createVoucherRechargeResponse(VoucherRechargeResponse value) {
        return new JAXBElement<VoucherRechargeResponse>(_VoucherRechargeResponse_QNAME, VoucherRechargeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustmentResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "adjustmentResponse")
    public JAXBElement<AdjustmentResponse> createAdjustmentResponse(AdjustmentResponse value) {
        return new JAXBElement<AdjustmentResponse>(_AdjustmentResponse_QNAME, AdjustmentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ERechargeReversal }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eRechargeReversal")
    public JAXBElement<ERechargeReversal> createERechargeReversal(ERechargeReversal value) {
        return new JAXBElement<ERechargeReversal>(_ERechargeReversal_QNAME, ERechargeReversal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETransferResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eTransferResponse")
    public JAXBElement<ETransferResponse> createETransferResponse(ETransferResponse value) {
        return new JAXBElement<ETransferResponse>(_ETransferResponse_QNAME, ETransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationCancel }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationCancel")
    public JAXBElement<ReservationCancel> createReservationCancel(ReservationCancel value) {
        return new JAXBElement<ReservationCancel>(_ReservationCancel_QNAME, ReservationCancel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Balance }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "balance")
    public JAXBElement<Balance> createBalance(Balance value) {
        return new JAXBElement<Balance>(_Balance_QNAME, Balance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideProperties }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideProperties")
    public JAXBElement<ProvideProperties> createProvideProperties(ProvideProperties value) {
        return new JAXBElement<ProvideProperties>(_ProvideProperties_QNAME, ProvideProperties.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSpidOfMsisdn }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "getSpidOfMsisdn")
    public JAXBElement<GetSpidOfMsisdn> createGetSpidOfMsisdn(GetSpidOfMsisdn value) {
        return new JAXBElement<GetSpidOfMsisdn>(_GetSpidOfMsisdn_QNAME, GetSpidOfMsisdn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideBonus }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideBonus")
    public JAXBElement<ProvideBonus> createProvideBonus(ProvideBonus value) {
        return new JAXBElement<ProvideBonus>(_ProvideBonus_QNAME, ProvideBonus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link co.moviired.microservices.dummy.tigo.model.BusinessException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "BusinessException")
    public JAXBElement<co.moviired.microservices.dummy.tigo.model.BusinessException> createBusinessException(co.moviired.microservices.dummy.tigo.model.BusinessException value) {
        return new JAXBElement<co.moviired.microservices.dummy.tigo.model.BusinessException>(_BusinessException_QNAME, BusinessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideUsers }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideUsers")
    public JAXBElement<ProvideUsers> createProvideUsers(ProvideUsers value) {
        return new JAXBElement<ProvideUsers>(_ProvideUsers_QNAME, ProvideUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationUseResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationUseResponse")
    public JAXBElement<ReservationUseResponse> createReservationUseResponse(ReservationUseResponse value) {
        return new JAXBElement<ReservationUseResponse>(_ReservationUseResponse_QNAME, ReservationUseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Credit }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "credit")
    public JAXBElement<Credit> createCredit(Credit value) {
        return new JAXBElement<Credit>(_Credit_QNAME, Credit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetXmlMvnoGroups }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "getXmlMvnoGroups")
    public JAXBElement<GetXmlMvnoGroups> createGetXmlMvnoGroups(GetXmlMvnoGroups value) {
        return new JAXBElement<GetXmlMvnoGroups>(_GetXmlMvnoGroups_QNAME, GetXmlMvnoGroups.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ERechargeResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eRechargeResponse")
    public JAXBElement<ERechargeResponse> createERechargeResponse(ERechargeResponse value) {
        return new JAXBElement<ERechargeResponse>(_ERechargeResponse_QNAME, ERechargeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideUsersResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideUsersResponse")
    public JAXBElement<ProvideUsersResponse> createProvideUsersResponse(ProvideUsersResponse value) {
        return new JAXBElement<ProvideUsersResponse>(_ProvideUsersResponse_QNAME, ProvideUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reservation }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservation")
    public JAXBElement<Reservation> createReservation(Reservation value) {
        return new JAXBElement<Reservation>(_Reservation_QNAME, Reservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationQuery }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationQuery")
    public JAXBElement<ReservationQuery> createReservationQuery(ReservationQuery value) {
        return new JAXBElement<ReservationQuery>(_ReservationQuery_QNAME, ReservationQuery.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvidePropertiesResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "providePropertiesResponse")
    public JAXBElement<ProvidePropertiesResponse> createProvidePropertiesResponse(ProvidePropertiesResponse value) {
        return new JAXBElement<ProvidePropertiesResponse>(_ProvidePropertiesResponse_QNAME, ProvidePropertiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationModify }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationModify")
    public JAXBElement<ReservationModify> createReservationModify(ReservationModify value) {
        return new JAXBElement<ReservationModify>(_ReservationModify_QNAME, ReservationModify.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationModifyResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationModifyResponse")
    public JAXBElement<ReservationModifyResponse> createReservationModifyResponse(ReservationModifyResponse value) {
        return new JAXBElement<ReservationModifyResponse>(_ReservationModifyResponse_QNAME, ReservationModifyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationEventResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "applicationEventResponse")
    public JAXBElement<ApplicationEventResponse> createApplicationEventResponse(ApplicationEventResponse value) {
        return new JAXBElement<ApplicationEventResponse>(_ApplicationEventResponse_QNAME, ApplicationEventResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RechargeReversalResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "rechargeReversalResponse")
    public JAXBElement<RechargeReversalResponse> createRechargeReversalResponse(RechargeReversalResponse value) {
        return new JAXBElement<RechargeReversalResponse>(_RechargeReversalResponse_QNAME, RechargeReversalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultPropertiesResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultPropertiesResponse")
    public JAXBElement<ConsultPropertiesResponse> createConsultPropertiesResponse(ConsultPropertiesResponse value) {
        return new JAXBElement<ConsultPropertiesResponse>(_ConsultPropertiesResponse_QNAME, ConsultPropertiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdviceOfChargeResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "adviceOfChargeResponse")
    public JAXBElement<AdviceOfChargeResponse> createAdviceOfChargeResponse(AdviceOfChargeResponse value) {
        return new JAXBElement<AdviceOfChargeResponse>(_AdviceOfChargeResponse_QNAME, AdviceOfChargeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationEvent }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "applicationEvent")
    public JAXBElement<ApplicationEvent> createApplicationEvent(ApplicationEvent value) {
        return new JAXBElement<ApplicationEvent>(_ApplicationEvent_QNAME, ApplicationEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideXmlMvnoGroups }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideXmlMvnoGroups")
    public JAXBElement<ProvideXmlMvnoGroups> createProvideXmlMvnoGroups(ProvideXmlMvnoGroups value) {
        return new JAXBElement<ProvideXmlMvnoGroups>(_ProvideXmlMvnoGroups_QNAME, ProvideXmlMvnoGroups.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Transfer }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "transfer")
    public JAXBElement<Transfer> createTransfer(Transfer value) {
        return new JAXBElement<Transfer>(_Transfer_QNAME, Transfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventAuthorizationResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eventAuthorizationResponse")
    public JAXBElement<EventAuthorizationResponse> createEventAuthorizationResponse(EventAuthorizationResponse value) {
        return new JAXBElement<EventAuthorizationResponse>(_EventAuthorizationResponse_QNAME, EventAuthorizationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetXmlMvnoGroupsResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "getXmlMvnoGroupsResponse")
    public JAXBElement<GetXmlMvnoGroupsResponse> createGetXmlMvnoGroupsResponse(GetXmlMvnoGroupsResponse value) {
        return new JAXBElement<GetXmlMvnoGroupsResponse>(_GetXmlMvnoGroupsResponse_QNAME, GetXmlMvnoGroupsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSpidOfMsisdnResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "getSpidOfMsisdnResponse")
    public JAXBElement<GetSpidOfMsisdnResponse> createGetSpidOfMsisdnResponse(GetSpidOfMsisdnResponse value) {
        return new JAXBElement<GetSpidOfMsisdnResponse>(_GetSpidOfMsisdnResponse_QNAME, GetSpidOfMsisdnResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reactivate4G }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reactivate4G")
    public JAXBElement<Reactivate4G> createReactivate4G(Reactivate4G value) {
        return new JAXBElement<Reactivate4G>(_Reactivate4G_QNAME, Reactivate4G.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "GatewayPrepayWSException")
    public JAXBElement<co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException> createGatewayPrepayWSException(co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException value) {
        return new JAXBElement<co.moviired.microservices.dummy.tigo.model.GatewayPrepayWSException>(_GatewayPrepayWSException_QNAME, GatewayPrepayWSException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Activate4GResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "activate4GResponse")
    public JAXBElement<Activate4GResponse> createActivate4GResponse(Activate4GResponse value) {
        return new JAXBElement<Activate4GResponse>(_Activate4GResponse_QNAME, Activate4GResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Adjustment }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "adjustment")
    public JAXBElement<Adjustment> createAdjustment(Adjustment value) {
        return new JAXBElement<Adjustment>(_Adjustment_QNAME, Adjustment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationUse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationUse")
    public JAXBElement<ReservationUse> createReservationUse(ReservationUse value) {
        return new JAXBElement<ReservationUse>(_ReservationUse_QNAME, ReservationUse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdviceOfCharge }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "adviceOfCharge")
    public JAXBElement<AdviceOfCharge> createAdviceOfCharge(AdviceOfCharge value) {
        return new JAXBElement<AdviceOfCharge>(_AdviceOfCharge_QNAME, AdviceOfCharge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETransfer }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eTransfer")
    public JAXBElement<ETransfer> createETransfer(ETransfer value) {
        return new JAXBElement<ETransfer>(_ETransfer_QNAME, ETransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideResourcesResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideResourcesResponse")
    public JAXBElement<ProvideResourcesResponse> createProvideResourcesResponse(ProvideResourcesResponse value) {
        return new JAXBElement<ProvideResourcesResponse>(_ProvideResourcesResponse_QNAME, ProvideResourcesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RechargeResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "rechargeResponse")
    public JAXBElement<RechargeResponse> createRechargeResponse(RechargeResponse value) {
        return new JAXBElement<RechargeResponse>(_RechargeResponse_QNAME, RechargeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceToResource }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "balanceToResource")
    public JAXBElement<BalanceToResource> createBalanceToResource(BalanceToResource value) {
        return new JAXBElement<BalanceToResource>(_BalanceToResource_QNAME, BalanceToResource.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Deactivate4G }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "deactivate4G")
    public JAXBElement<Deactivate4G> createDeactivate4G(Deactivate4G value) {
        return new JAXBElement<Deactivate4G>(_Deactivate4G_QNAME, Deactivate4G.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ERechargeReversalResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eRechargeReversalResponse")
    public JAXBElement<ERechargeReversalResponse> createERechargeReversalResponse(ERechargeReversalResponse value) {
        return new JAXBElement<ERechargeReversalResponse>(_ERechargeReversalResponse_QNAME, ERechargeReversalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DebitResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "debitResponse")
    public JAXBElement<DebitResponse> createDebitResponse(DebitResponse value) {
        return new JAXBElement<DebitResponse>(_DebitResponse_QNAME, DebitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventAuthorization }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eventAuthorization")
    public JAXBElement<EventAuthorization> createEventAuthorization(EventAuthorization value) {
        return new JAXBElement<EventAuthorization>(_EventAuthorization_QNAME, EventAuthorization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultBonusResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultBonusResponse")
    public JAXBElement<ConsultBonusResponse> createConsultBonusResponse(ConsultBonusResponse value) {
        return new JAXBElement<ConsultBonusResponse>(_ConsultBonusResponse_QNAME, ConsultBonusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Recharge }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "recharge")
    public JAXBElement<Recharge> createRecharge(Recharge value) {
        return new JAXBElement<Recharge>(_Recharge_QNAME, Recharge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationResponse")
    public JAXBElement<ReservationResponse> createReservationResponse(ReservationResponse value) {
        return new JAXBElement<ReservationResponse>(_ReservationResponse_QNAME, ReservationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideXmlMvnoGroupsResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideXmlMvnoGroupsResponse")
    public JAXBElement<ProvideXmlMvnoGroupsResponse> createProvideXmlMvnoGroupsResponse(ProvideXmlMvnoGroupsResponse value) {
        return new JAXBElement<ProvideXmlMvnoGroupsResponse>(_ProvideXmlMvnoGroupsResponse_QNAME, ProvideXmlMvnoGroupsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultProperties }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultProperties")
    public JAXBElement<ConsultProperties> createConsultProperties(ConsultProperties value) {
        return new JAXBElement<ConsultProperties>(_ConsultProperties_QNAME, ConsultProperties.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultEUsersPromotionsResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultEUsersPromotionsResponse")
    public JAXBElement<ConsultEUsersPromotionsResponse> createConsultEUsersPromotionsResponse(ConsultEUsersPromotionsResponse value) {
        return new JAXBElement<ConsultEUsersPromotionsResponse>(_ConsultEUsersPromotionsResponse_QNAME, ConsultEUsersPromotionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Debit }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "debit")
    public JAXBElement<Debit> createDebit(Debit value) {
        return new JAXBElement<Debit>(_Debit_QNAME, Debit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultResourcesResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultResourcesResponse")
    public JAXBElement<ConsultResourcesResponse> createConsultResourcesResponse(ConsultResourcesResponse value) {
        return new JAXBElement<ConsultResourcesResponse>(_ConsultResourcesResponse_QNAME, ConsultResourcesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationCancelResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationCancelResponse")
    public JAXBElement<ReservationCancelResponse> createReservationCancelResponse(ReservationCancelResponse value) {
        return new JAXBElement<ReservationCancelResponse>(_ReservationCancelResponse_QNAME, ReservationCancelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "transferResponse")
    public JAXBElement<TransferResponse> createTransferResponse(TransferResponse value) {
        return new JAXBElement<TransferResponse>(_TransferResponse_QNAME, TransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultEUsersPromotions }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultEUsersPromotions")
    public JAXBElement<ConsultEUsersPromotions> createConsultEUsersPromotions(ConsultEUsersPromotions value) {
        return new JAXBElement<ConsultEUsersPromotions>(_ConsultEUsersPromotions_QNAME, ConsultEUsersPromotions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RechargeReversal }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "rechargeReversal")
    public JAXBElement<RechargeReversal> createRechargeReversal(RechargeReversal value) {
        return new JAXBElement<RechargeReversal>(_RechargeReversal_QNAME, RechargeReversal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "creditResponse")
    public JAXBElement<CreditResponse> createCreditResponse(CreditResponse value) {
        return new JAXBElement<CreditResponse>(_CreditResponse_QNAME, CreditResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "balanceResponse")
    public JAXBElement<BalanceResponse> createBalanceResponse(BalanceResponse value) {
        return new JAXBElement<BalanceResponse>(_BalanceResponse_QNAME, BalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultUsersResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultUsersResponse")
    public JAXBElement<ConsultUsersResponse> createConsultUsersResponse(ConsultUsersResponse value) {
        return new JAXBElement<ConsultUsersResponse>(_ConsultUsersResponse_QNAME, ConsultUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EBalance }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eBalance")
    public JAXBElement<EBalance> createEBalance(EBalance value) {
        return new JAXBElement<EBalance>(_EBalance_QNAME, EBalance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ERecharge }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eRecharge")
    public JAXBElement<ERecharge> createERecharge(ERecharge value) {
        return new JAXBElement<ERecharge>(_ERecharge_QNAME, ERecharge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EBalanceResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "eBalanceResponse")
    public JAXBElement<EBalanceResponse> createEBalanceResponse(EBalanceResponse value) {
        return new JAXBElement<EBalanceResponse>(_EBalanceResponse_QNAME, EBalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Activate4G }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "activate4G")
    public JAXBElement<Activate4G> createActivate4G(Activate4G value) {
        return new JAXBElement<Activate4G>(_Activate4G_QNAME, Activate4G.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupsMvnoDTO }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "groups")
    public JAXBElement<GroupsMvnoDTO> createGroups(GroupsMvnoDTO value) {
        return new JAXBElement<GroupsMvnoDTO>(_Groups_QNAME, GroupsMvnoDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultBonus }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultBonus")
    public JAXBElement<ConsultBonus> createConsultBonus(ConsultBonus value) {
        return new JAXBElement<ConsultBonus>(_ConsultBonus_QNAME, ConsultBonus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvideBonusResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "provideBonusResponse")
    public JAXBElement<ProvideBonusResponse> createProvideBonusResponse(ProvideBonusResponse value) {
        return new JAXBElement<ProvideBonusResponse>(_ProvideBonusResponse_QNAME, ProvideBonusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultUsers }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultUsers")
    public JAXBElement<ConsultUsers> createConsultUsers(ConsultUsers value) {
        return new JAXBElement<ConsultUsers>(_ConsultUsers_QNAME, ConsultUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationQueryResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reservationQueryResponse")
    public JAXBElement<ReservationQueryResponse> createReservationQueryResponse(ReservationQueryResponse value) {
        return new JAXBElement<ReservationQueryResponse>(_ReservationQueryResponse_QNAME, ReservationQueryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceToResourceResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "balanceToResourceResponse")
    public JAXBElement<BalanceToResourceResponse> createBalanceToResourceResponse(BalanceToResourceResponse value) {
        return new JAXBElement<BalanceToResourceResponse>(_BalanceToResourceResponse_QNAME, BalanceToResourceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reactivate4GResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "reactivate4GResponse")
    public JAXBElement<Reactivate4GResponse> createReactivate4GResponse(Reactivate4GResponse value) {
        return new JAXBElement<Reactivate4GResponse>(_Reactivate4GResponse_QNAME, Reactivate4GResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultResources }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "consultResources")
    public JAXBElement<ConsultResources> createConsultResources(ConsultResources value) {
        return new JAXBElement<ConsultResources>(_ConsultResources_QNAME, ConsultResources.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherRecharge }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "voucherRecharge")
    public JAXBElement<VoucherRecharge> createVoucherRecharge(VoucherRecharge value) {
        return new JAXBElement<VoucherRecharge>(_VoucherRecharge_QNAME, VoucherRecharge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Deactivate4GResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.webscp.gatewaytigo.tigo.com.co/", name = "deactivate4GResponse")
    public JAXBElement<Deactivate4GResponse> createDeactivate4GResponse(Deactivate4GResponse value) {
        return new JAXBElement<Deactivate4GResponse>(_Deactivate4GResponse_QNAME, Deactivate4GResponse.class, null, value);
    }

}

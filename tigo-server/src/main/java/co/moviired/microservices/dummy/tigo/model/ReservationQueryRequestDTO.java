package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reservationQueryRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationQueryRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="maxEventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="maxValidUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="minEventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="minValidUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nextReservationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originatingSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reservationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reservationMSISDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reservationTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationQueryRequestDTO", propOrder = {
        "maxEventTime",
        "maxValidUntil",
        "minEventTime",
        "minValidUntil",
        "nextReservationId",
        "originatingSource",
        "reservationId",
        "reservationMSISDN",
        "reservationTransactionId",
        "transactionId"
})
public class ReservationQueryRequestDTO {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar maxEventTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar maxValidUntil;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar minEventTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar minValidUntil;
    protected String nextReservationId;
    protected String originatingSource;
    protected String reservationId;
    protected String reservationMSISDN;
    protected String reservationTransactionId;
    protected String transactionId;

    /**
     * Gets the value of the maxEventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getMaxEventTime() {
        return maxEventTime;
    }

    /**
     * Sets the value of the maxEventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setMaxEventTime(XMLGregorianCalendar value) {
        this.maxEventTime = value;
    }

    /**
     * Gets the value of the maxValidUntil property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getMaxValidUntil() {
        return maxValidUntil;
    }

    /**
     * Sets the value of the maxValidUntil property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setMaxValidUntil(XMLGregorianCalendar value) {
        this.maxValidUntil = value;
    }

    /**
     * Gets the value of the minEventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getMinEventTime() {
        return minEventTime;
    }

    /**
     * Sets the value of the minEventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setMinEventTime(XMLGregorianCalendar value) {
        this.minEventTime = value;
    }

    /**
     * Gets the value of the minValidUntil property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getMinValidUntil() {
        return minValidUntil;
    }

    /**
     * Sets the value of the minValidUntil property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setMinValidUntil(XMLGregorianCalendar value) {
        this.minValidUntil = value;
    }

    /**
     * Gets the value of the nextReservationId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNextReservationId() {
        return nextReservationId;
    }

    /**
     * Sets the value of the nextReservationId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNextReservationId(String value) {
        this.nextReservationId = value;
    }

    /**
     * Gets the value of the originatingSource property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOriginatingSource() {
        return originatingSource;
    }

    /**
     * Sets the value of the originatingSource property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOriginatingSource(String value) {
        this.originatingSource = value;
    }

    /**
     * Gets the value of the reservationId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReservationId() {
        return reservationId;
    }

    /**
     * Sets the value of the reservationId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReservationId(String value) {
        this.reservationId = value;
    }

    /**
     * Gets the value of the reservationMSISDN property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReservationMSISDN() {
        return reservationMSISDN;
    }

    /**
     * Sets the value of the reservationMSISDN property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReservationMSISDN(String value) {
        this.reservationMSISDN = value;
    }

    /**
     * Gets the value of the reservationTransactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReservationTransactionId() {
        return reservationTransactionId;
    }

    /**
     * Sets the value of the reservationTransactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReservationTransactionId(String value) {
        this.reservationTransactionId = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

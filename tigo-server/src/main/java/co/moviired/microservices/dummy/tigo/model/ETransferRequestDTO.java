package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eTransferRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eTransferRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amountResource" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="destinationMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resourceType" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eTransferRequestDTO", propOrder = {
        "amountResource",
        "destinationMobile",
        "information",
        "resourceType",
        "transactionId"
})
public class ETransferRequestDTO {

    protected Long amountResource;
    protected String destinationMobile;
    protected String information;
    protected IdDTO resourceType;
    protected String transactionId;

    /**
     * Gets the value of the amountResource property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getAmountResource() {
        return amountResource;
    }

    /**
     * Sets the value of the amountResource property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setAmountResource(Long value) {
        this.amountResource = value;
    }

    /**
     * Gets the value of the destinationMobile property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestinationMobile() {
        return destinationMobile;
    }

    /**
     * Sets the value of the destinationMobile property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestinationMobile(String value) {
        this.destinationMobile = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the resourceType property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setResourceType(IdDTO value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

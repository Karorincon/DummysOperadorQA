package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adviceOfChargeResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="adviceOfChargeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}adviceOfChargeResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adviceOfChargeResponse", propOrder = {
        "_return"
})
public class AdviceOfChargeResponse {

    @XmlElement(name = "return")
    protected AdviceOfChargeResponseDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link AdviceOfChargeResponseDTO }
     */
    public AdviceOfChargeResponseDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link AdviceOfChargeResponseDTO }
     */
    public void setReturn(AdviceOfChargeResponseDTO value) {
        this._return = value;
    }

}

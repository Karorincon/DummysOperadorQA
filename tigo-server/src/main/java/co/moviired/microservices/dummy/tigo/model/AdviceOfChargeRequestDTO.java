package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for adviceOfChargeRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="adviceOfChargeRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="allowAdditionalStates" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cellId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="direction" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="eventSubType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="eventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="eventType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="location" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}locationDTO" minOccurs="0"/>
 *         &lt;element name="locationAreaId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="networkBearerId" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}networkBearerIDDTO" minOccurs="0"/>
 *         &lt;element name="numberOfEvents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="origAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="origType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="preAnswerDuration" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="predeterminedCommunity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="queryType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="volume" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adviceOfChargeRequestDTO", propOrder = {
        "addressType",
        "allowAdditionalStates",
        "cellId",
        "destAddress",
        "destType",
        "direction",
        "duration",
        "eventSubType",
        "eventTime",
        "eventType",
        "location",
        "locationAreaId",
        "msisdn",
        "networkBearerId",
        "numberOfEvents",
        "origAddress",
        "origType",
        "preAnswerDuration",
        "predeterminedCommunity",
        "queryType",
        "transactionId",
        "volume"
})
public class AdviceOfChargeRequestDTO {

    protected int addressType;
    protected boolean allowAdditionalStates;
    protected String cellId;
    protected String destAddress;
    protected int destType;
    protected int direction;
    protected int duration;
    protected int eventSubType;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    protected int eventType;
    protected LocationDTO location;
    protected String locationAreaId;
    protected String msisdn;
    protected NetworkBearerIDDTO networkBearerId;
    protected int numberOfEvents;
    protected String origAddress;
    protected int origType;
    protected int preAnswerDuration;
    protected String predeterminedCommunity;
    protected int queryType;
    protected String transactionId;
    protected int volume;

    /**
     * Gets the value of the addressType property.
     */
    public int getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     */
    public void setAddressType(int value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the allowAdditionalStates property.
     */
    public boolean isAllowAdditionalStates() {
        return allowAdditionalStates;
    }

    /**
     * Sets the value of the allowAdditionalStates property.
     */
    public void setAllowAdditionalStates(boolean value) {
        this.allowAdditionalStates = value;
    }

    /**
     * Gets the value of the cellId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCellId() {
        return cellId;
    }

    /**
     * Sets the value of the cellId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCellId(String value) {
        this.cellId = value;
    }

    /**
     * Gets the value of the destAddress property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestAddress() {
        return destAddress;
    }

    /**
     * Sets the value of the destAddress property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestAddress(String value) {
        this.destAddress = value;
    }

    /**
     * Gets the value of the destType property.
     */
    public int getDestType() {
        return destType;
    }

    /**
     * Sets the value of the destType property.
     */
    public void setDestType(int value) {
        this.destType = value;
    }

    /**
     * Gets the value of the direction property.
     */
    public int getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     */
    public void setDirection(int value) {
        this.direction = value;
    }

    /**
     * Gets the value of the duration property.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     */
    public void setDuration(int value) {
        this.duration = value;
    }

    /**
     * Gets the value of the eventSubType property.
     */
    public int getEventSubType() {
        return eventSubType;
    }

    /**
     * Sets the value of the eventSubType property.
     */
    public void setEventSubType(int value) {
        this.eventSubType = value;
    }

    /**
     * Gets the value of the eventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the eventType property.
     */
    public int getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     */
    public void setEventType(int value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the location property.
     *
     * @return possible object is
     * {@link LocationDTO }
     */
    public LocationDTO getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     *
     * @param value allowed object is
     *              {@link LocationDTO }
     */
    public void setLocation(LocationDTO value) {
        this.location = value;
    }

    /**
     * Gets the value of the locationAreaId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLocationAreaId() {
        return locationAreaId;
    }

    /**
     * Sets the value of the locationAreaId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLocationAreaId(String value) {
        this.locationAreaId = value;
    }

    /**
     * Gets the value of the msisdn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the networkBearerId property.
     *
     * @return possible object is
     * {@link NetworkBearerIDDTO }
     */
    public NetworkBearerIDDTO getNetworkBearerId() {
        return networkBearerId;
    }

    /**
     * Sets the value of the networkBearerId property.
     *
     * @param value allowed object is
     *              {@link NetworkBearerIDDTO }
     */
    public void setNetworkBearerId(NetworkBearerIDDTO value) {
        this.networkBearerId = value;
    }

    /**
     * Gets the value of the numberOfEvents property.
     */
    public int getNumberOfEvents() {
        return numberOfEvents;
    }

    /**
     * Sets the value of the numberOfEvents property.
     */
    public void setNumberOfEvents(int value) {
        this.numberOfEvents = value;
    }

    /**
     * Gets the value of the origAddress property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOrigAddress() {
        return origAddress;
    }

    /**
     * Sets the value of the origAddress property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOrigAddress(String value) {
        this.origAddress = value;
    }

    /**
     * Gets the value of the origType property.
     */
    public int getOrigType() {
        return origType;
    }

    /**
     * Sets the value of the origType property.
     */
    public void setOrigType(int value) {
        this.origType = value;
    }

    /**
     * Gets the value of the preAnswerDuration property.
     */
    public int getPreAnswerDuration() {
        return preAnswerDuration;
    }

    /**
     * Sets the value of the preAnswerDuration property.
     */
    public void setPreAnswerDuration(int value) {
        this.preAnswerDuration = value;
    }

    /**
     * Gets the value of the predeterminedCommunity property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPredeterminedCommunity() {
        return predeterminedCommunity;
    }

    /**
     * Sets the value of the predeterminedCommunity property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPredeterminedCommunity(String value) {
        this.predeterminedCommunity = value;
    }

    /**
     * Gets the value of the queryType property.
     */
    public int getQueryType() {
        return queryType;
    }

    /**
     * Sets the value of the queryType property.
     */
    public void setQueryType(int value) {
        this.queryType = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the volume property.
     */
    public int getVolume() {
        return volume;
    }

    /**
     * Sets the value of the volume property.
     */
    public void setVolume(int value) {
        this.volume = value;
    }

}

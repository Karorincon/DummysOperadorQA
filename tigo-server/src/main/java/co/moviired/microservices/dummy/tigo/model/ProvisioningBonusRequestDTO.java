package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.PromotionDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for provisioningBonusRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="provisioningBonusRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="promotionCollection" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}promotionDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "provisioningBonusRequestDTO", propOrder = {
        "promotionCollection"
})
public class ProvisioningBonusRequestDTO {

    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.PromotionDTO> promotionCollection;

    /**
     * Gets the value of the promotionCollection property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the promotionCollection property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPromotionCollection().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.PromotionDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.PromotionDTO> getPromotionCollection() {
        if (promotionCollection == null) {
            promotionCollection = new ArrayList<PromotionDTO>();
        }
        return this.promotionCollection;
    }

}

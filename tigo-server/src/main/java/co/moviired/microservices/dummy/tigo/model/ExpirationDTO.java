package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for expirationDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="expirationDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="expirationDays" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="maxResources" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="minResources" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "expirationDTO", propOrder = {
        "expirationDays",
        "maxResources",
        "minResources"
})
public class ExpirationDTO {

    protected Long expirationDays;
    protected Long maxResources;
    protected Long minResources;

    /**
     * Gets the value of the expirationDays property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getExpirationDays() {
        return expirationDays;
    }

    /**
     * Sets the value of the expirationDays property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setExpirationDays(Long value) {
        this.expirationDays = value;
    }

    /**
     * Gets the value of the maxResources property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getMaxResources() {
        return maxResources;
    }

    /**
     * Sets the value of the maxResources property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setMaxResources(Long value) {
        this.maxResources = value;
    }

    /**
     * Gets the value of the minResources property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getMinResources() {
        return minResources;
    }

    /**
     * Sets the value of the minResources property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setMinResources(Long value) {
        this.minResources = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for propertyDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="propertyDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="propertyFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="propertyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="propertyValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "propertyDTO", propOrder = {
        "propertyFile",
        "propertyName",
        "propertyValue"
})
public class PropertyDTO {

    protected String propertyFile;
    protected String propertyName;
    protected String propertyValue;

    /**
     * Gets the value of the propertyFile property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPropertyFile() {
        return propertyFile;
    }

    /**
     * Sets the value of the propertyFile property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPropertyFile(String value) {
        this.propertyFile = value;
    }

    /**
     * Gets the value of the propertyName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Sets the value of the propertyName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPropertyName(String value) {
        this.propertyName = value;
    }

    /**
     * Gets the value of the propertyValue property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPropertyValue() {
        return propertyValue;
    }

    /**
     * Sets the value of the propertyValue property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPropertyValue(String value) {
        this.propertyValue = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSpidOfMsisdnResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getSpidOfMsisdnResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}getSpidOfMsisdnResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSpidOfMsisdnResponse", propOrder = {
        "_return"
})
public class GetSpidOfMsisdnResponse {

    @XmlElement(name = "return")
    protected GetSpidOfMsisdnResponseDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link GetSpidOfMsisdnResponseDTO }
     */
    public GetSpidOfMsisdnResponseDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link GetSpidOfMsisdnResponseDTO }
     */
    public void setReturn(GetSpidOfMsisdnResponseDTO value) {
        this._return = value;
    }

}

package co.moviired.microservices.dummy.tigo.service;

import co.moviired.microservices.dummy.tigo.model.*;

public interface ITigoService {

    ERechargeResponseDTO processRecharge(ERechargeRequestDTO request)  throws BusinessException_Exception;

    ERechargeRevResponseDTO processReversal(ERechargeRevRequestDTO request) throws BusinessException_Exception;

}

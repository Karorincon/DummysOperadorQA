package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for eventResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eventResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusCost" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="chargeUnits" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberOfEvents" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realCost" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceCostColl" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceDTOColl" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="totalChargeUnits" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventResponseDTO", propOrder = {
        "bonusBalance",
        "bonusCost",
        "chargeUnits",
        "mobileNumber",
        "numberOfEvents",
        "realBalance",
        "realCost",
        "resourceCostColl",
        "resourceDTOColl",
        "totalChargeUnits"
})
public class EventResponseDTO {

    protected MoneyDTO bonusBalance;
    protected MoneyDTO bonusCost;
    protected Integer chargeUnits;
    protected String mobileNumber;
    protected Integer numberOfEvents;
    protected MoneyDTO realBalance;
    protected MoneyDTO realCost;
    @XmlElement(nillable = true)
    protected List<Object> resourceCostColl;
    @XmlElement(nillable = true)
    protected List<Object> resourceDTOColl;
    protected Integer totalChargeUnits;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusCost property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusCost() {
        return bonusCost;
    }

    /**
     * Sets the value of the bonusCost property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusCost(MoneyDTO value) {
        this.bonusCost = value;
    }

    /**
     * Gets the value of the chargeUnits property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getChargeUnits() {
        return chargeUnits;
    }

    /**
     * Sets the value of the chargeUnits property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setChargeUnits(Integer value) {
        this.chargeUnits = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the numberOfEvents property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getNumberOfEvents() {
        return numberOfEvents;
    }

    /**
     * Sets the value of the numberOfEvents property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setNumberOfEvents(Integer value) {
        this.numberOfEvents = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realCost property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealCost() {
        return realCost;
    }

    /**
     * Sets the value of the realCost property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealCost(MoneyDTO value) {
        this.realCost = value;
    }

    /**
     * Gets the value of the resourceCostColl property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceCostColl property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceCostColl().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getResourceCostColl() {
        if (resourceCostColl == null) {
            resourceCostColl = new ArrayList<Object>();
        }
        return this.resourceCostColl;
    }

    /**
     * Gets the value of the resourceDTOColl property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceDTOColl property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceDTOColl().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getResourceDTOColl() {
        if (resourceDTOColl == null) {
            resourceDTOColl = new ArrayList<Object>();
        }
        return this.resourceDTOColl;
    }

    /**
     * Gets the value of the totalChargeUnits property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getTotalChargeUnits() {
        return totalChargeUnits;
    }

    /**
     * Sets the value of the totalChargeUnits property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setTotalChargeUnits(Integer value) {
        this.totalChargeUnits = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.CreditResponseDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eRechargeRevResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eRechargeRevResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusStatus" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="creditResponse" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}creditResponseDTO" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rechargeRevResponse" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}rechargeRevResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eRechargeRevResponseDTO", propOrder = {
        "bonusStatus",
        "creditResponse",
        "message",
        "rechargeRevResponse"
})
public class ERechargeRevResponseDTO {

    protected IdDTO bonusStatus;
    protected co.moviired.microservices.dummy.tigo.model.CreditResponseDTO creditResponse;
    protected String message;
    protected RechargeRevResponseDTO rechargeRevResponse;

    /**
     * Gets the value of the bonusStatus property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getBonusStatus() {
        return bonusStatus;
    }

    /**
     * Sets the value of the bonusStatus property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setBonusStatus(IdDTO value) {
        this.bonusStatus = value;
    }

    /**
     * Gets the value of the creditResponse property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.CreditResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.CreditResponseDTO getCreditResponse() {
        return creditResponse;
    }

    /**
     * Sets the value of the creditResponse property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.CreditResponseDTO }
     */
    public void setCreditResponse(CreditResponseDTO value) {
        this.creditResponse = value;
    }

    /**
     * Gets the value of the message property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the rechargeRevResponse property.
     *
     * @return possible object is
     * {@link RechargeRevResponseDTO }
     */
    public RechargeRevResponseDTO getRechargeRevResponse() {
        return rechargeRevResponse;
    }

    /**
     * Sets the value of the rechargeRevResponse property.
     *
     * @param value allowed object is
     *              {@link RechargeRevResponseDTO }
     */
    public void setRechargeRevResponse(RechargeRevResponseDTO value) {
        this.rechargeRevResponse = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for debitResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="debitResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusDebited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyDebited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" minOccurs="0"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="product" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realDebited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceDebited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="serviceProvider" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="state" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "debitResponseDTO", propOrder = {
        "authorizationNumber",
        "bonusBalance",
        "bonusDebited",
        "dedicatedMoneyBalance",
        "dedicatedMoneyDebited",
        "expirationDate",
        "mobileNumber",
        "product",
        "realBalance",
        "realDebited",
        "resourceBalance",
        "resourceDebited",
        "serviceProvider",
        "state",
        "transactionId"
})
public class DebitResponseDTO {

    protected String authorizationNumber;
    protected MoneyDTO bonusBalance;
    protected MoneyDTO bonusDebited;
    protected DedicatedMoneyDTO dedicatedMoneyBalance;
    protected DedicatedMoneyDTO dedicatedMoneyDebited;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    protected String mobileNumber;
    protected IdDTO product;
    protected MoneyDTO realBalance;
    protected MoneyDTO realDebited;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceBalance;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceDebited;
    protected IdDTO serviceProvider;
    protected IdDTO state;
    protected String transactionId;

    /**
     * Gets the value of the authorizationNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    /**
     * Sets the value of the authorizationNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthorizationNumber(String value) {
        this.authorizationNumber = value;
    }

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusDebited property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusDebited() {
        return bonusDebited;
    }

    /**
     * Sets the value of the bonusDebited property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusDebited(MoneyDTO value) {
        this.bonusDebited = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * @return possible object is
     * {@link DedicatedMoneyDTO }
     */
    public DedicatedMoneyDTO getDedicatedMoneyBalance() {
        return dedicatedMoneyBalance;
    }

    /**
     * Sets the value of the dedicatedMoneyBalance property.
     *
     * @param value allowed object is
     *              {@link DedicatedMoneyDTO }
     */
    public void setDedicatedMoneyBalance(DedicatedMoneyDTO value) {
        this.dedicatedMoneyBalance = value;
    }

    /**
     * Gets the value of the dedicatedMoneyDebited property.
     *
     * @return possible object is
     * {@link DedicatedMoneyDTO }
     */
    public DedicatedMoneyDTO getDedicatedMoneyDebited() {
        return dedicatedMoneyDebited;
    }

    /**
     * Sets the value of the dedicatedMoneyDebited property.
     *
     * @param value allowed object is
     *              {@link DedicatedMoneyDTO }
     */
    public void setDedicatedMoneyDebited(DedicatedMoneyDTO value) {
        this.dedicatedMoneyDebited = value;
    }

    /**
     * Gets the value of the expirationDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the product property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setProduct(IdDTO value) {
        this.product = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realDebited property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealDebited() {
        return realDebited;
    }

    /**
     * Sets the value of the realDebited property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealDebited(MoneyDTO value) {
        this.realDebited = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<ResourceDTO>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the resourceDebited property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceDebited property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceDebited().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceDebited() {
        if (resourceDebited == null) {
            resourceDebited = new ArrayList<ResourceDTO>();
        }
        return this.resourceDebited;
    }

    /**
     * Gets the value of the serviceProvider property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getServiceProvider() {
        return serviceProvider;
    }

    /**
     * Sets the value of the serviceProvider property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setServiceProvider(IdDTO value) {
        this.serviceProvider = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setState(IdDTO value) {
        this.state = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

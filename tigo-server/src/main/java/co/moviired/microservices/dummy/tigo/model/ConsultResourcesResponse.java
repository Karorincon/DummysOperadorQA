package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultResourcesResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="consultResourcesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}provisioningResourcesResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultResourcesResponse", propOrder = {
        "_return"
})
public class ConsultResourcesResponse {

    @XmlElement(name = "return")
    protected ProvisioningResourcesResponseDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link ProvisioningResourcesResponseDTO }
     */
    public ProvisioningResourcesResponseDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link ProvisioningResourcesResponseDTO }
     */
    public void setReturn(ProvisioningResourcesResponseDTO value) {
        this._return = value;
    }

}

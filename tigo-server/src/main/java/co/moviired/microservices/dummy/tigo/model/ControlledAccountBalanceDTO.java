package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for controlledAccountBalanceDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="controlledAccountBalanceDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusBalanceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realBalanceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="resourceCollection" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "controlledAccountBalanceDTO", propOrder = {
        "bonusBalance",
        "bonusBalanceExpiryDate",
        "realBalance",
        "realBalanceExpiryDate",
        "resourceCollection"
})
public class ControlledAccountBalanceDTO {

    protected MoneyDTO bonusBalance;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar bonusBalanceExpiryDate;
    protected MoneyDTO realBalance;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar realBalanceExpiryDate;
    @XmlElement(nillable = true)
    protected List<Object> resourceCollection;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusBalanceExpiryDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getBonusBalanceExpiryDate() {
        return bonusBalanceExpiryDate;
    }

    /**
     * Sets the value of the bonusBalanceExpiryDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setBonusBalanceExpiryDate(XMLGregorianCalendar value) {
        this.bonusBalanceExpiryDate = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realBalanceExpiryDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getRealBalanceExpiryDate() {
        return realBalanceExpiryDate;
    }

    /**
     * Sets the value of the realBalanceExpiryDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setRealBalanceExpiryDate(XMLGregorianCalendar value) {
        this.realBalanceExpiryDate = value;
    }

    /**
     * Gets the value of the resourceCollection property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceCollection property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceCollection().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getResourceCollection() {
        if (resourceCollection == null) {
            resourceCollection = new ArrayList<Object>();
        }
        return this.resourceCollection;
    }

}

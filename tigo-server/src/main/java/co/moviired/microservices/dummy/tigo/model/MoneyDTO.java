package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java class for moneyDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="moneyDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="currencyId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="currencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "moneyDTO", propOrder = {
        "amount",
        "currencyId",
        "currencyName"
})
public class MoneyDTO {

    protected BigDecimal amount;
    protected Integer currencyId;
    protected String currencyName;

    @Override
    public String toString() {
        return "MoneyDTO{" +
                "amount=" + amount +
                ", currencyId=" + currencyId +
                ", currencyName='" + currencyName + '\'' +
                '}';
    }

    /**
     * Gets the value of the amount property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyId property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCurrencyId() {
        return currencyId;
    }

    /**
     * Sets the value of the currencyId property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCurrencyId(Integer value) {
        this.currencyId = value;
    }

    /**
     * Gets the value of the currencyName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCurrencyName() {
        return currencyName;
    }

    /**
     * Sets the value of the currencyName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCurrencyName(String value) {
        this.currencyName = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;
import co.moviired.microservices.dummy.tigo.model.ResourceDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for transferRequestDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="transferRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activeTimeExpiryDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="bonusTransfer" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyTransfer" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" minOccurs="0"/>
 *         &lt;element name="destAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="information" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partitionExpiryDatetime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="realTransfer" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceTransfer" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" minOccurs="0"/>
 *         &lt;element name="transactioType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transferType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferRequestDTO", propOrder = {
        "activeTimeExpiryDateTime",
        "bonusTransfer",
        "dedicatedMoneyTransfer",
        "destAddress",
        "eventTime",
        "information",
        "mobileNumber",
        "partitionExpiryDatetime",
        "realTransfer",
        "resourceTransfer",
        "transactioType",
        "transactionId",
        "transferType"
})
public class TransferRequestDTO {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activeTimeExpiryDateTime;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusTransfer;
    protected co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO dedicatedMoneyTransfer;
    protected String destAddress;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    protected String information;
    protected String mobileNumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar partitionExpiryDatetime;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realTransfer;
    protected co.moviired.microservices.dummy.tigo.model.ResourceDTO resourceTransfer;
    protected String transactioType;
    protected String transactionId;
    protected String transferType;

    /**
     * Gets the value of the activeTimeExpiryDateTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getActiveTimeExpiryDateTime() {
        return activeTimeExpiryDateTime;
    }

    /**
     * Sets the value of the activeTimeExpiryDateTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setActiveTimeExpiryDateTime(XMLGregorianCalendar value) {
        this.activeTimeExpiryDateTime = value;
    }

    /**
     * Gets the value of the bonusTransfer property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusTransfer() {
        return bonusTransfer;
    }

    /**
     * Sets the value of the bonusTransfer property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusTransfer(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusTransfer = value;
    }

    /**
     * Gets the value of the dedicatedMoneyTransfer property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO getDedicatedMoneyTransfer() {
        return dedicatedMoneyTransfer;
    }

    /**
     * Sets the value of the dedicatedMoneyTransfer property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public void setDedicatedMoneyTransfer(DedicatedMoneyDTO value) {
        this.dedicatedMoneyTransfer = value;
    }

    /**
     * Gets the value of the destAddress property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestAddress() {
        return destAddress;
    }

    /**
     * Sets the value of the destAddress property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestAddress(String value) {
        this.destAddress = value;
    }

    /**
     * Gets the value of the eventTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInformation(String value) {
        this.information = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the partitionExpiryDatetime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getPartitionExpiryDatetime() {
        return partitionExpiryDatetime;
    }

    /**
     * Sets the value of the partitionExpiryDatetime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setPartitionExpiryDatetime(XMLGregorianCalendar value) {
        this.partitionExpiryDatetime = value;
    }

    /**
     * Gets the value of the realTransfer property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealTransfer() {
        return realTransfer;
    }

    /**
     * Sets the value of the realTransfer property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealTransfer(MoneyDTO value) {
        this.realTransfer = value;
    }

    /**
     * Gets the value of the resourceTransfer property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ResourceDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ResourceDTO getResourceTransfer() {
        return resourceTransfer;
    }

    /**
     * Sets the value of the resourceTransfer property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ResourceDTO }
     */
    public void setResourceTransfer(ResourceDTO value) {
        this.resourceTransfer = value;
    }

    /**
     * Gets the value of the transactioType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactioType() {
        return transactioType;
    }

    /**
     * Sets the value of the transactioType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactioType(String value) {
        this.transactioType = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the transferType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransferType() {
        return transferType;
    }

    /**
     * Sets the value of the transferType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransferType(String value) {
        this.transferType = value;
    }

}

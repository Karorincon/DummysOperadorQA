package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;
import co.moviired.microservices.dummy.tigo.model.ResourceDTO;
import co.moviired.microservices.dummy.tigo.model.ReturnCodeDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for voucherRechargeResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="voucherRechargeResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusCredited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyCredited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realCredited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceCredited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="returnCode" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}returnCodeDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "voucherRechargeResponseDTO", propOrder = {
        "bonusBalance",
        "bonusCredited",
        "dedicatedMoneyBalance",
        "dedicatedMoneyCredited",
        "msisdn",
        "realBalance",
        "realCredited",
        "resourceBalance",
        "resourceCredited",
        "returnCode",
        "transactionId"
})
public class VoucherRechargeResponseDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusBalance;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusCredited;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyBalance;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyCredited;
    protected String msisdn;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realBalance;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realCredited;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> resourceBalance;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> resourceCredited;
    protected co.moviired.microservices.dummy.tigo.model.ReturnCodeDTO returnCode;
    protected String transactionId;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusCredited property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusCredited() {
        return bonusCredited;
    }

    /**
     * Sets the value of the bonusCredited property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusCredited(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusCredited = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyBalance() {
        if (dedicatedMoneyBalance == null) {
            dedicatedMoneyBalance = new ArrayList<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyBalance;
    }

    /**
     * Gets the value of the dedicatedMoneyCredited property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyCredited property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyCredited().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyCredited() {
        if (dedicatedMoneyCredited == null) {
            dedicatedMoneyCredited = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyCredited;
    }

    /**
     * Gets the value of the msisdn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realCredited property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealCredited() {
        return realCredited;
    }

    /**
     * Sets the value of the realCredited property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealCredited(MoneyDTO value) {
        this.realCredited = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.ResourceDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<co.moviired.microservices.dummy.tigo.model.ResourceDTO>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the resourceCredited property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceCredited property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceCredited().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.ResourceDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> getResourceCredited() {
        if (resourceCredited == null) {
            resourceCredited = new ArrayList<ResourceDTO>();
        }
        return this.resourceCredited;
    }

    /**
     * Gets the value of the returnCode property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ReturnCodeDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ReturnCodeDTO getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ReturnCodeDTO }
     */
    public void setReturnCode(ReturnCodeDTO value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

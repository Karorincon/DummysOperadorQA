package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.IdDTO;
import co.moviired.microservices.dummy.tigo.model.ResourceSellerDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for sellerDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="sellerDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resourceSellerColl" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceSellerDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="sellerId" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sellerDTO", propOrder = {
        "resourceSellerColl",
        "sellerId"
})
public class SellerDTO {

    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.ResourceSellerDTO> resourceSellerColl;
    protected co.moviired.microservices.dummy.tigo.model.IdDTO sellerId;

    /**
     * Gets the value of the resourceSellerColl property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceSellerColl property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceSellerColl().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.ResourceSellerDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.ResourceSellerDTO> getResourceSellerColl() {
        if (resourceSellerColl == null) {
            resourceSellerColl = new ArrayList<ResourceSellerDTO>();
        }
        return this.resourceSellerColl;
    }

    /**
     * Gets the value of the sellerId property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.IdDTO getSellerId() {
        return sellerId;
    }

    /**
     * Sets the value of the sellerId property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public void setSellerId(IdDTO value) {
        this.sellerId = value;
    }

}

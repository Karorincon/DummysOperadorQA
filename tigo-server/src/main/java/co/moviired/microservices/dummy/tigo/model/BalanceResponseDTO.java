package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for balanceResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="balanceResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="controlledAccount" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}controlledAccountBalanceDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyReserved" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="product" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceReserved" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="serviceProvider" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="state" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "balanceResponseDTO", propOrder = {
        "bonusBalance",
        "bonusReserved",
        "controlledAccount",
        "dedicatedMoneyBalance",
        "dedicatedMoneyReserved",
        "expirationDate",
        "msisdn",
        "product",
        "realBalance",
        "realReserved",
        "resourceBalance",
        "resourceReserved",
        "serviceProvider",
        "state",
        "transactionId"
})
public class BalanceResponseDTO {

    protected MoneyDTO bonusBalance;
    protected MoneyDTO bonusReserved;
    protected ControlledAccountBalanceDTO controlledAccount;
    @XmlElement(nillable = true)
    protected List<Object> dedicatedMoneyBalance;
    @XmlElement(nillable = true)
    protected List<Object> dedicatedMoneyReserved;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    protected String msisdn;
    protected IdDTO product;
    protected MoneyDTO realBalance;
    protected MoneyDTO realReserved;
    @XmlElement(nillable = true)
    protected List<Object> resourceBalance;
    @XmlElement(nillable = true)
    protected List<Object> resourceReserved;
    protected IdDTO serviceProvider;
    protected IdDTO state;
    protected String transactionId;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusReserved property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusReserved() {
        return bonusReserved;
    }

    /**
     * Sets the value of the bonusReserved property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusReserved(MoneyDTO value) {
        this.bonusReserved = value;
    }

    /**
     * Gets the value of the controlledAccount property.
     *
     * @return possible object is
     * {@link ControlledAccountBalanceDTO }
     */
    public ControlledAccountBalanceDTO getControlledAccount() {
        return controlledAccount;
    }

    /**
     * Sets the value of the controlledAccount property.
     *
     * @param value allowed object is
     *              {@link ControlledAccountBalanceDTO }
     */
    public void setControlledAccount(ControlledAccountBalanceDTO value) {
        this.controlledAccount = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getDedicatedMoneyBalance() {
        if (dedicatedMoneyBalance == null) {
            dedicatedMoneyBalance = new ArrayList<Object>();
        }
        return this.dedicatedMoneyBalance;
    }

    /**
     * Gets the value of the dedicatedMoneyReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getDedicatedMoneyReserved() {
        if (dedicatedMoneyReserved == null) {
            dedicatedMoneyReserved = new ArrayList<Object>();
        }
        return this.dedicatedMoneyReserved;
    }

    /**
     * Gets the value of the expirationDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the msisdn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the product property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setProduct(IdDTO value) {
        this.product = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realReserved property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealReserved() {
        return realReserved;
    }

    /**
     * Sets the value of the realReserved property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealReserved(MoneyDTO value) {
        this.realReserved = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<Object>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the resourceReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     */
    public List<Object> getResourceReserved() {
        if (resourceReserved == null) {
            resourceReserved = new ArrayList<Object>();
        }
        return this.resourceReserved;
    }

    /**
     * Gets the value of the serviceProvider property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getServiceProvider() {
        return serviceProvider;
    }

    /**
     * Sets the value of the serviceProvider property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setServiceProvider(IdDTO value) {
        this.serviceProvider = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return possible object is
     * {@link IdDTO }
     */
    public IdDTO getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value allowed object is
     *              {@link IdDTO }
     */
    public void setState(IdDTO value) {
        this.state = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

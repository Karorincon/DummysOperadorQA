package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for reservationCancelResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationCancelResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="realReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="reservationGeneralInfo" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationGeneralResponseDTO" minOccurs="0"/>
 *         &lt;element name="resourceReturned" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationCancelResponseDTO", propOrder = {
        "bonusReturned",
        "dedicatedMoneyReturned",
        "realReturned",
        "reservationGeneralInfo",
        "resourceReturned"
})
public class ReservationCancelResponseDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusReturned;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyReturned;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realReturned;
    protected ReservationGeneralResponseDTO reservationGeneralInfo;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceReturned;

    /**
     * Gets the value of the bonusReturned property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusReturned() {
        return bonusReturned;
    }

    /**
     * Sets the value of the bonusReturned property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusReturned(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusReturned = value;
    }

    /**
     * Gets the value of the dedicatedMoneyReturned property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyReturned property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyReturned().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyReturned() {
        if (dedicatedMoneyReturned == null) {
            dedicatedMoneyReturned = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyReturned;
    }

    /**
     * Gets the value of the realReturned property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealReturned() {
        return realReturned;
    }

    /**
     * Sets the value of the realReturned property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealReturned(MoneyDTO value) {
        this.realReturned = value;
    }

    /**
     * Gets the value of the reservationGeneralInfo property.
     *
     * @return possible object is
     * {@link ReservationGeneralResponseDTO }
     */
    public ReservationGeneralResponseDTO getReservationGeneralInfo() {
        return reservationGeneralInfo;
    }

    /**
     * Sets the value of the reservationGeneralInfo property.
     *
     * @param value allowed object is
     *              {@link ReservationGeneralResponseDTO }
     */
    public void setReservationGeneralInfo(ReservationGeneralResponseDTO value) {
        this.reservationGeneralInfo = value;
    }

    /**
     * Gets the value of the resourceReturned property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceReturned property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceReturned().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceReturned() {
        if (resourceReturned == null) {
            resourceReturned = new ArrayList<ResourceDTO>();
        }
        return this.resourceReturned;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for recharge complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="recharge">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}rechargeRequestDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recharge", propOrder = {
        "arg0"
})
public class Recharge {

    protected RechargeRequestDTO arg0;

    /**
     * Gets the value of the arg0 property.
     *
     * @return possible object is
     * {@link RechargeRequestDTO }
     */
    public RechargeRequestDTO getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     *
     * @param value allowed object is
     *              {@link RechargeRequestDTO }
     */
    public void setArg0(RechargeRequestDTO value) {
        this.arg0 = value;
    }

}

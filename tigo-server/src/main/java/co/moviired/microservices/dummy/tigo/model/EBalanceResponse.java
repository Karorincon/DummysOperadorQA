package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eBalanceResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="eBalanceResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}balanceResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eBalanceResponse", propOrder = {
        "_return"
})
public class EBalanceResponse {

    @XmlElement(name = "return")
    protected co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.BalanceResponseDTO }
     */
    public void setReturn(BalanceResponseDTO value) {
        this._return = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for provideXmlMvnoGroupsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="provideXmlMvnoGroupsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}groupsMvnoDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "provideXmlMvnoGroupsResponse", propOrder = {
        "_return"
})
public class ProvideXmlMvnoGroupsResponse {

    @XmlElement(name = "return")
    protected GroupsMvnoDTO _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link GroupsMvnoDTO }
     */
    public GroupsMvnoDTO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link GroupsMvnoDTO }
     */
    public void setReturn(GroupsMvnoDTO value) {
        this._return = value;
    }

}

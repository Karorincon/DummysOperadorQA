package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for returnCodeDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="returnCodeDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subVal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="val" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "returnCodeDTO", propOrder = {
        "subVal",
        "text",
        "val"
})
public class ReturnCodeDTO {

    protected Integer subVal;
    protected String text;
    protected Integer val;

    /**
     * Gets the value of the subVal property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getSubVal() {
        return subVal;
    }

    /**
     * Sets the value of the subVal property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setSubVal(Integer value) {
        this.subVal = value;
    }

    /**
     * Gets the value of the text property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the val property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setVal(Integer value) {
        this.val = value;
    }

}

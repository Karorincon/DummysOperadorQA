package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for reservationResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalanceReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="realReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="reservationGeneralInfo" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationGeneralResponseDTO" minOccurs="0"/>
 *         &lt;element name="resourceReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationResponseDTO", propOrder = {
        "bonusReserved",
        "dedicatedMoneyBalanceReserved",
        "realReserved",
        "reservationGeneralInfo",
        "resourceReserved"
})
public class ReservationResponseDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusReserved;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyBalanceReserved;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realReserved;
    protected co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO reservationGeneralInfo;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceReserved;

    /**
     * Gets the value of the bonusReserved property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusReserved() {
        return bonusReserved;
    }

    /**
     * Sets the value of the bonusReserved property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusReserved(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusReserved = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalanceReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalanceReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalanceReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyBalanceReserved() {
        if (dedicatedMoneyBalanceReserved == null) {
            dedicatedMoneyBalanceReserved = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyBalanceReserved;
    }

    /**
     * Gets the value of the realReserved property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealReserved() {
        return realReserved;
    }

    /**
     * Sets the value of the realReserved property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealReserved(MoneyDTO value) {
        this.realReserved = value;
    }

    /**
     * Gets the value of the reservationGeneralInfo property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO getReservationGeneralInfo() {
        return reservationGeneralInfo;
    }

    /**
     * Sets the value of the reservationGeneralInfo property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO }
     */
    public void setReservationGeneralInfo(ReservationGeneralResponseDTO value) {
        this.reservationGeneralInfo = value;
    }

    /**
     * Gets the value of the resourceReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceReserved() {
        if (resourceReserved == null) {
            resourceReserved = new ArrayList<ResourceDTO>();
        }
        return this.resourceReserved;
    }

}

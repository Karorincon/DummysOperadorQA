package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.PointOfSaleDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for userPromotionDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="userPromotionDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="posMap" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}pointOfSaleDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="promotionDayId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userPromotionDTO", propOrder = {
        "idUser",
        "name",
        "posMap",
        "promotionDayId"
})
public class UserPromotionDTO {

    protected String idUser;
    protected String name;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.PointOfSaleDTO> posMap;
    protected Long promotionDayId;

    /**
     * Gets the value of the idUser property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * Sets the value of the idUser property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdUser(String value) {
        this.idUser = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the posMap property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the posMap property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPosMap().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.PointOfSaleDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.PointOfSaleDTO> getPosMap() {
        if (posMap == null) {
            posMap = new ArrayList<PointOfSaleDTO>();
        }
        return this.posMap;
    }

    /**
     * Gets the value of the promotionDayId property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getPromotionDayId() {
        return promotionDayId;
    }

    /**
     * Sets the value of the promotionDayId property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setPromotionDayId(Long value) {
        this.promotionDayId = value;
    }

}

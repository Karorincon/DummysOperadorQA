package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for adviceOfChargeResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="adviceOfChargeResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusCost" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="controlledAccountBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}controlledAccountBalanceDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyCost" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberOfEvents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realCost" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceCost" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="returncode" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}returnCodeDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adviceOfChargeResponseDTO", propOrder = {
        "bonusBalance",
        "bonusCost",
        "controlledAccountBalance",
        "dedicatedMoneyBalance",
        "dedicatedMoneyCost",
        "msisdn",
        "numberOfEvents",
        "realBalance",
        "realCost",
        "resourceBalance",
        "resourceCost",
        "returncode",
        "transactionId"
})
public class AdviceOfChargeResponseDTO {

    protected MoneyDTO bonusBalance;
    protected MoneyDTO bonusCost;
    protected ControlledAccountBalanceDTO controlledAccountBalance;
    @XmlElement(nillable = true)
    protected List<DedicatedMoneyDTO> dedicatedMoneyBalance;
    @XmlElement(nillable = true)
    protected List<DedicatedMoneyDTO> dedicatedMoneyCost;
    protected String msisdn;
    protected int numberOfEvents;
    protected MoneyDTO realBalance;
    protected MoneyDTO realCost;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceBalance;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceCost;
    protected ReturnCodeDTO returncode;
    protected String transactionId;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusBalance(MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusCost property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getBonusCost() {
        return bonusCost;
    }

    /**
     * Sets the value of the bonusCost property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setBonusCost(MoneyDTO value) {
        this.bonusCost = value;
    }

    /**
     * Gets the value of the controlledAccountBalance property.
     *
     * @return possible object is
     * {@link ControlledAccountBalanceDTO }
     */
    public ControlledAccountBalanceDTO getControlledAccountBalance() {
        return controlledAccountBalance;
    }

    /**
     * Sets the value of the controlledAccountBalance property.
     *
     * @param value allowed object is
     *              {@link ControlledAccountBalanceDTO }
     */
    public void setControlledAccountBalance(ControlledAccountBalanceDTO value) {
        this.controlledAccountBalance = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DedicatedMoneyDTO }
     */
    public List<DedicatedMoneyDTO> getDedicatedMoneyBalance() {
        if (dedicatedMoneyBalance == null) {
            dedicatedMoneyBalance = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyBalance;
    }

    /**
     * Gets the value of the dedicatedMoneyCost property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyCost property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyCost().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DedicatedMoneyDTO }
     */
    public List<DedicatedMoneyDTO> getDedicatedMoneyCost() {
        if (dedicatedMoneyCost == null) {
            dedicatedMoneyCost = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyCost;
    }

    /**
     * Gets the value of the msisdn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the numberOfEvents property.
     */
    public int getNumberOfEvents() {
        return numberOfEvents;
    }

    /**
     * Sets the value of the numberOfEvents property.
     */
    public void setNumberOfEvents(int value) {
        this.numberOfEvents = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealBalance(MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realCost property.
     *
     * @return possible object is
     * {@link MoneyDTO }
     */
    public MoneyDTO getRealCost() {
        return realCost;
    }

    /**
     * Sets the value of the realCost property.
     *
     * @param value allowed object is
     *              {@link MoneyDTO }
     */
    public void setRealCost(MoneyDTO value) {
        this.realCost = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<ResourceDTO>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the resourceCost property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceCost property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceCost().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceCost() {
        if (resourceCost == null) {
            resourceCost = new ArrayList<ResourceDTO>();
        }
        return this.resourceCost;
    }

    /**
     * Gets the value of the returncode property.
     *
     * @return possible object is
     * {@link ReturnCodeDTO }
     */
    public ReturnCodeDTO getReturncode() {
        return returncode;
    }

    /**
     * Sets the value of the returncode property.
     *
     * @param value allowed object is
     *              {@link ReturnCodeDTO }
     */
    public void setReturncode(ReturnCodeDTO value) {
        this.returncode = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.BonusDTO;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for promotionDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="promotionDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusCollection" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}bonusDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="finalDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="initialDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="partition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promotionDayId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="typePartition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "promotionDTO", propOrder = {
        "bonusCollection",
        "description",
        "finalDate",
        "initialDate",
        "partition",
        "promotionDayId",
        "typePartition"
})
public class PromotionDTO {

    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.BonusDTO> bonusCollection;
    protected String description;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar finalDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar initialDate;
    protected String partition;
    protected Long promotionDayId;
    protected String typePartition;

    /**
     * Gets the value of the bonusCollection property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bonusCollection property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBonusCollection().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.BonusDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.BonusDTO> getBonusCollection() {
        if (bonusCollection == null) {
            bonusCollection = new ArrayList<BonusDTO>();
        }
        return this.bonusCollection;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the finalDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getFinalDate() {
        return finalDate;
    }

    /**
     * Sets the value of the finalDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setFinalDate(XMLGregorianCalendar value) {
        this.finalDate = value;
    }

    /**
     * Gets the value of the initialDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getInitialDate() {
        return initialDate;
    }

    /**
     * Sets the value of the initialDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setInitialDate(XMLGregorianCalendar value) {
        this.initialDate = value;
    }

    /**
     * Gets the value of the partition property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPartition() {
        return partition;
    }

    /**
     * Sets the value of the partition property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPartition(String value) {
        this.partition = value;
    }

    /**
     * Gets the value of the promotionDayId property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getPromotionDayId() {
        return promotionDayId;
    }

    /**
     * Sets the value of the promotionDayId property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setPromotionDayId(Long value) {
        this.promotionDayId = value;
    }

    /**
     * Gets the value of the typePartition property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTypePartition() {
        return typePartition;
    }

    /**
     * Sets the value of the typePartition property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTypePartition(String value) {
        this.typePartition = value;
    }

}

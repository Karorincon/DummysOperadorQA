package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO;
import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.IdDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;
import co.moviired.microservices.dummy.tigo.model.ResourceDTO;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for transferResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="transferResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusMoney" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusTransferred" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="controlledAccountBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}controlledAccountBalanceDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyTransferred" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="destAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="product" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realMoney" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realTransferred" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="resourceBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceTransferred" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="serviceProvider" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="state" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}idDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferResponseDTO", propOrder = {
        "bonusBalance",
        "bonusMoney",
        "bonusTransferred",
        "controlledAccountBalance",
        "dedicatedMoneyBalance",
        "dedicatedMoneyTransferred",
        "destAddress",
        "expirationDate",
        "mobileNumber",
        "product",
        "realBalance",
        "realMoney",
        "realTransferred",
        "resourceBalance",
        "resourceTransferred",
        "sequenceNumber",
        "serviceProvider",
        "state",
        "transactionId"
})
public class TransferResponseDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusBalance;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusMoney;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusTransferred;
    protected co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO controlledAccountBalance;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyBalance;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyTransferred;
    protected String destAddress;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    protected String mobileNumber;
    protected co.moviired.microservices.dummy.tigo.model.IdDTO product;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realBalance;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realMoney;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realTransferred;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> resourceBalance;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> resourceTransferred;
    protected Integer sequenceNumber;
    protected co.moviired.microservices.dummy.tigo.model.IdDTO serviceProvider;
    protected co.moviired.microservices.dummy.tigo.model.IdDTO state;
    protected String transactionId;

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusMoney property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusMoney() {
        return bonusMoney;
    }

    /**
     * Sets the value of the bonusMoney property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusMoney(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusMoney = value;
    }

    /**
     * Gets the value of the bonusTransferred property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusTransferred() {
        return bonusTransferred;
    }

    /**
     * Sets the value of the bonusTransferred property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusTransferred(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusTransferred = value;
    }

    /**
     * Gets the value of the controlledAccountBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO getControlledAccountBalance() {
        return controlledAccountBalance;
    }

    /**
     * Sets the value of the controlledAccountBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ControlledAccountBalanceDTO }
     */
    public void setControlledAccountBalance(ControlledAccountBalanceDTO value) {
        this.controlledAccountBalance = value;
    }

    /**
     * Gets the value of the dedicatedMoneyBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyBalance() {
        if (dedicatedMoneyBalance == null) {
            dedicatedMoneyBalance = new ArrayList<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyBalance;
    }

    /**
     * Gets the value of the dedicatedMoneyTransferred property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyTransferred property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyTransferred().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyTransferred() {
        if (dedicatedMoneyTransferred == null) {
            dedicatedMoneyTransferred = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyTransferred;
    }

    /**
     * Gets the value of the destAddress property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDestAddress() {
        return destAddress;
    }

    /**
     * Sets the value of the destAddress property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDestAddress(String value) {
        this.destAddress = value;
    }

    /**
     * Gets the value of the expirationDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the product property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.IdDTO getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public void setProduct(co.moviired.microservices.dummy.tigo.model.IdDTO value) {
        this.product = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realMoney property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealMoney() {
        return realMoney;
    }

    /**
     * Sets the value of the realMoney property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealMoney(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realMoney = value;
    }

    /**
     * Gets the value of the realTransferred property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealTransferred() {
        return realTransferred;
    }

    /**
     * Sets the value of the realTransferred property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealTransferred(MoneyDTO value) {
        this.realTransferred = value;
    }

    /**
     * Gets the value of the resourceBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.ResourceDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> getResourceBalance() {
        if (resourceBalance == null) {
            resourceBalance = new ArrayList<co.moviired.microservices.dummy.tigo.model.ResourceDTO>();
        }
        return this.resourceBalance;
    }

    /**
     * Gets the value of the resourceTransferred property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceTransferred property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceTransferred().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.ResourceDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.ResourceDTO> getResourceTransferred() {
        if (resourceTransferred == null) {
            resourceTransferred = new ArrayList<ResourceDTO>();
        }
        return this.resourceTransferred;
    }

    /**
     * Gets the value of the sequenceNumber property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setSequenceNumber(Integer value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the serviceProvider property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.IdDTO getServiceProvider() {
        return serviceProvider;
    }

    /**
     * Sets the value of the serviceProvider property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public void setServiceProvider(co.moviired.microservices.dummy.tigo.model.IdDTO value) {
        this.serviceProvider = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.IdDTO getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.IdDTO }
     */
    public void setState(IdDTO value) {
        this.state = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

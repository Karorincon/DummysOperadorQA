package co.moviired.microservices.dummy.tigo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for reservationQueryResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationQueryResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nextReservationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responseItem" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationQueryResponseItemDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="returnCode" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}returnCodeDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationQueryResponseDTO", propOrder = {
        "nextReservationId",
        "responseItem",
        "returnCode",
        "transactionId"
})
public class ReservationQueryResponseDTO {

    protected String nextReservationId;
    @XmlElement(nillable = true)
    protected List<ReservationQueryResponseItemDTO> responseItem;
    protected ReturnCodeDTO returnCode;
    protected String transactionId;

    /**
     * Gets the value of the nextReservationId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNextReservationId() {
        return nextReservationId;
    }

    /**
     * Sets the value of the nextReservationId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNextReservationId(String value) {
        this.nextReservationId = value;
    }

    /**
     * Gets the value of the responseItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReservationQueryResponseItemDTO }
     */
    public List<ReservationQueryResponseItemDTO> getResponseItem() {
        if (responseItem == null) {
            responseItem = new ArrayList<ReservationQueryResponseItemDTO>();
        }
        return this.responseItem;
    }

    /**
     * Gets the value of the returnCode property.
     *
     * @return possible object is
     * {@link ReturnCodeDTO }
     */
    public ReturnCodeDTO getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     *
     * @param value allowed object is
     *              {@link ReturnCodeDTO }
     */
    public void setReturnCode(ReturnCodeDTO value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

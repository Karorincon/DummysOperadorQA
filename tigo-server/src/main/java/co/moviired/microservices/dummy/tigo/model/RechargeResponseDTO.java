package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.MoneyDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rechargeResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="rechargeResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bonusBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusCredited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="mobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="realBalance" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realCredited" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rechargeResponseDTO", propOrder = {
        "authorizationNumber",
        "bonusBalance",
        "bonusCredited",
        "mobileNumber",
        "realBalance",
        "realCredited",
        "transactionId"
})
public class RechargeResponseDTO {

    protected String authorizationNumber;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusBalance;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusCredited;
    protected String mobileNumber;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realBalance;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realCredited;
    protected String transactionId;

    /**
     * Gets the value of the authorizationNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    /**
     * Sets the value of the authorizationNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthorizationNumber(String value) {
        this.authorizationNumber = value;
    }

    /**
     * Gets the value of the bonusBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusBalance() {
        return bonusBalance;
    }

    /**
     * Sets the value of the bonusBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusBalance = value;
    }

    /**
     * Gets the value of the bonusCredited property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusCredited() {
        return bonusCredited;
    }

    /**
     * Sets the value of the bonusCredited property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusCredited(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusCredited = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the realBalance property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealBalance() {
        return realBalance;
    }

    /**
     * Sets the value of the realBalance property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealBalance(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realBalance = value;
    }

    /**
     * Gets the value of the realCredited property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealCredited() {
        return realCredited;
    }

    /**
     * Sets the value of the realCredited property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealCredited(MoneyDTO value) {
        this.realCredited = value;
    }

    /**
     * Gets the value of the transactionId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}

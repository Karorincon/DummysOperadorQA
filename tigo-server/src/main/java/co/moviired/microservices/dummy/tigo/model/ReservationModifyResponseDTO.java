package co.moviired.microservices.dummy.tigo.model;

import co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO;
import co.moviired.microservices.dummy.tigo.model.MoneyDTO;
import co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for reservationModifyResponseDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="reservationModifyResponseDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bonusChange" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="bonusReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyChange" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dedicatedMoneyReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}dedicatedMoneyDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numberOfEvents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="realChange" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="realReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}moneyDTO" minOccurs="0"/>
 *         &lt;element name="reservationGeneralInfo" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}reservationGeneralResponseDTO" minOccurs="0"/>
 *         &lt;element name="resourceChange" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="resourceReserved" type="{http://ws.webscp.gatewaytigo.tigo.com.co/}resourceDTO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="validUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationModifyResponseDTO", propOrder = {
        "bonusChange",
        "bonusReserved",
        "dedicatedMoneyChange",
        "dedicatedMoneyReserved",
        "numberOfEvents",
        "realChange",
        "realReserved",
        "reservationGeneralInfo",
        "resourceChange",
        "resourceReserved",
        "validUntil"
})
public class ReservationModifyResponseDTO {

    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusChange;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO bonusReserved;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyChange;
    @XmlElement(nillable = true)
    protected List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> dedicatedMoneyReserved;
    protected int numberOfEvents;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realChange;
    protected co.moviired.microservices.dummy.tigo.model.MoneyDTO realReserved;
    protected co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO reservationGeneralInfo;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceChange;
    @XmlElement(nillable = true)
    protected List<ResourceDTO> resourceReserved;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validUntil;

    /**
     * Gets the value of the bonusChange property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusChange() {
        return bonusChange;
    }

    /**
     * Sets the value of the bonusChange property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusChange(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusChange = value;
    }

    /**
     * Gets the value of the bonusReserved property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getBonusReserved() {
        return bonusReserved;
    }

    /**
     * Sets the value of the bonusReserved property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setBonusReserved(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.bonusReserved = value;
    }

    /**
     * Gets the value of the dedicatedMoneyChange property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyChange property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyChange().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyChange() {
        if (dedicatedMoneyChange == null) {
            dedicatedMoneyChange = new ArrayList<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyChange;
    }

    /**
     * Gets the value of the dedicatedMoneyReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dedicatedMoneyReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDedicatedMoneyReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO }
     */
    public List<co.moviired.microservices.dummy.tigo.model.DedicatedMoneyDTO> getDedicatedMoneyReserved() {
        if (dedicatedMoneyReserved == null) {
            dedicatedMoneyReserved = new ArrayList<DedicatedMoneyDTO>();
        }
        return this.dedicatedMoneyReserved;
    }

    /**
     * Gets the value of the numberOfEvents property.
     */
    public int getNumberOfEvents() {
        return numberOfEvents;
    }

    /**
     * Sets the value of the numberOfEvents property.
     */
    public void setNumberOfEvents(int value) {
        this.numberOfEvents = value;
    }

    /**
     * Gets the value of the realChange property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealChange() {
        return realChange;
    }

    /**
     * Sets the value of the realChange property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealChange(co.moviired.microservices.dummy.tigo.model.MoneyDTO value) {
        this.realChange = value;
    }

    /**
     * Gets the value of the realReserved property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.MoneyDTO getRealReserved() {
        return realReserved;
    }

    /**
     * Sets the value of the realReserved property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.MoneyDTO }
     */
    public void setRealReserved(MoneyDTO value) {
        this.realReserved = value;
    }

    /**
     * Gets the value of the reservationGeneralInfo property.
     *
     * @return possible object is
     * {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO }
     */
    public co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO getReservationGeneralInfo() {
        return reservationGeneralInfo;
    }

    /**
     * Sets the value of the reservationGeneralInfo property.
     *
     * @param value allowed object is
     *              {@link co.moviired.microservices.dummy.tigo.model.ReservationGeneralResponseDTO }
     */
    public void setReservationGeneralInfo(ReservationGeneralResponseDTO value) {
        this.reservationGeneralInfo = value;
    }

    /**
     * Gets the value of the resourceChange property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceChange property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceChange().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceChange() {
        if (resourceChange == null) {
            resourceChange = new ArrayList<ResourceDTO>();
        }
        return this.resourceChange;
    }

    /**
     * Gets the value of the resourceReserved property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceReserved property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceReserved().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceDTO }
     */
    public List<ResourceDTO> getResourceReserved() {
        if (resourceReserved == null) {
            resourceReserved = new ArrayList<ResourceDTO>();
        }
        return this.resourceReserved;
    }

    /**
     * Gets the value of the validUntil property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getValidUntil() {
        return validUntil;
    }

    /**
     * Sets the value of the validUntil property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setValidUntil(XMLGregorianCalendar value) {
        this.validUntil = value;
    }

}

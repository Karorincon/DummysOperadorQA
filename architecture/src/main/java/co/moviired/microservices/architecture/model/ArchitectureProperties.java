package co.moviired.microservices.architecture.model;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@Component
@ConfigurationProperties(prefix = "architecture", ignoreUnknownFields = false)
public class ArchitectureProperties {

    private final Security security = new Security();

    public Security getSecurity() {
        return security;
    }

    public static class Security {

        private final Authentication authentication = new Authentication();

        public Authentication getAuthentication() {
            return authentication;
        }

        public static class Authentication {

            private final Oauth oauth = new Oauth();
            private final AccessToken accessToken = new AccessToken();

            public Oauth getOauth() {
                return oauth;
            }

            public AccessToken getAccessToken() {
                return accessToken;
            }

            public static class Oauth {

                private String clientid;

                private String secret = "4rqu1t3ctur3#2OI8*";

                private int tokenValidityInSeconds = 1800;

                public String getClientid() {
                    return clientid;
                }

                public void setClientid(String clientid) {
                    this.clientid = clientid;
                }

                public String getSecret() {
                    return secret;
                }

                public void setSecret(String secret) {
                    this.secret = secret;
                }

                public int getTokenValidityInSeconds() {
                    return tokenValidityInSeconds;
                }

                public void setTokenValidityInSeconds(int tokenValidityInSeconds) {
                    this.tokenValidityInSeconds = tokenValidityInSeconds;
                }
            }

            public static class AccessToken {

                private String secret = "4rqu1t3ctur3#2OI8*";

                private boolean storeUserInToken = true;

                private int tokenValidityInSeconds = 1800;

                public String getSecret() {
                    return secret;
                }

                public void setSecret(String secret) {
                    this.secret = secret;
                }

                public int getTokenValidityInSeconds() {
                    return tokenValidityInSeconds;
                }

                public void setTokenValidityInSeconds(int tokenValidityInSeconds) {
                    this.tokenValidityInSeconds = tokenValidityInSeconds;
                }

                public boolean isStoreUserInToken() {
                    return storeUserInToken;
                }

                public void setStoreUserInToken(boolean storeUserInToken) {
                    this.storeUserInToken = storeUserInToken;
                }

            }
        }

    }

}

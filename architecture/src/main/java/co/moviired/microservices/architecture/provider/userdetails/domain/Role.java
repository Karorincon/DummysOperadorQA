package co.moviired.microservices.architecture.provider.userdetails.domain;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class Role {
    public Role() {
        super();
    }

    public Role(String name, List<Permission> permissions) {
        super();
        this.name = name;
        this.permissions = permissions;
    }

    private String name;
    private List<Permission> permissions = new ArrayList<>();

    public String getName() {
        return name;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }
}

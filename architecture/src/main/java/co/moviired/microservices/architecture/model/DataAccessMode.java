package co.moviired.microservices.architecture.model;

import co.moviired.microservices.architecture.provider.configure.InMemoryProviderConfig;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public enum DataAccessMode {
    CUSTOM(null),
    IN_MEMORY(InMemoryProviderConfig.class);

    private final Class<?> configClass;

    public Class<?> getConfigClass() {
        return configClass;
    }

    DataAccessMode(Class<?> configClass) {
        this.configClass = configClass;
    }
}

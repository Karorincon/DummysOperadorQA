package co.moviired.microservices.architecture.conexion.repository;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.exception.ORMException;
import org.springframework.data.repository.CrudRepository;

public interface IErrorCodeRepository extends CrudRepository<ErrorCode, Integer> {

    ErrorCode findByThirdPartyIdAndErrorTypeId(Integer thirdPartyId, Integer ErrorTypeId) throws ORMException;
}

package co.moviired.microservices.architecture.exception;

import org.springframework.dao.DataAccessException;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 * <p/>
 * Excepcion para manejar errores a nivel de MANAGERS cuando existen excepciones a nivel de los DAO
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-03-13
 * @see DataAccessException
 * @since 1.0
 */
public class ORMException extends DataAccessException {

    private static final long serialVersionUID = 6144528606058403643L;
    private String mensaje;

    public ORMException(String mensaje) {
        super(mensaje);
        setMensaje(mensaje);
    }

    public ORMException(String mensaje, Throwable cause) {
        super(mensaje, cause);
        setMensaje(mensaje);
    }

    public String getMensaje() {
        return this.mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
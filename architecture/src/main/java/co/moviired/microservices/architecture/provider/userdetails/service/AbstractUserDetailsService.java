package co.moviired.microservices.architecture.provider.userdetails.service;

import co.moviired.microservices.architecture.provider.userdetails.domain.UserSecurityDetailsImpl;
import co.moviired.microservices.architecture.util.Constants;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public abstract class AbstractUserDetailsService implements UserDetailsService {
    private static final int MAX_TIME_IN_CACHE = 10000;
    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

    protected final Cache cache;

    public AbstractUserDetailsService(CacheManager cacheManager) {
        super();
        cache = cacheManager.getCache(Constants.ARQUITECTURE_CACHE_NAME);
    }

    @Override
    public final UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserSecurityDetailsImpl user = cache.get(username, UserSecurityDetailsImpl.class);
        if (user == null || (user != null && user.getLastLoadTime() > MAX_TIME_IN_CACHE)) {
            user = findUser(username);
            cache.put(username, user);
        }

        if (user == null) {
            throw new UsernameNotFoundException("Usuario no encontrado : " + username);
        }

        detailsChecker.check(user);
        return user;
    }

    protected abstract UserSecurityDetailsImpl findUser(String username);

}

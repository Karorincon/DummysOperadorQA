package co.moviired.microservices.architecture.model.input;

import co.moviired.microservices.architecture.base.BaseModel;
import co.moviired.microservices.architecture.exception.DataException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * Esta clase debera ser extendida en cada Microservicio para definir la entrada específica de cada uno de estos
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class Data extends BaseModel {

    private static String replaceOrdinal(String cadena) {
        final String origen = "ABCDEFGHIJ";
        final String replace = "1234567890";
        cadena = cadena.toUpperCase();
        for (int i = 0; i < origen.length(); i++) {
            cadena = cadena.replace(origen.charAt(i), replace.charAt(i));
        }
        return cadena;
    }

    private static String changeTipoOperacion(String number) {
        final String[] codesOrigin = new String[]{"RC", "ON"};
        final String[] codesDest = new String[]{"2202", "2203"};
        for (int i = 0; i < codesOrigin.length; i++) {
            number = number.replace(codesOrigin[i], codesDest[i]);
        }

        return number;
    }

    // Transformar el TransactionID de Mahindra de alfanumérico a numérico
    // Origen - CustomerTxReference: RC180326.1150.C00991
    // Destino - RequestReference: XXXX180326.1150.C00991. Deben quedar 12 dígitos:
    //      4 Se cambia RC, ON por 2202, 2203
    //      2 dígitos del día
    //      1 dígito para el NODO, Se cambia la letra del servidor por su ordinal correspondiente: A=0, B=1, C=2, ...)
    //      5 dígitos del consecutivo
    public static String translateTransactionID(String idTrans) throws DataException {
        if ((idTrans == null) || (idTrans.isEmpty()))
            throw new DataException();

        idTrans = changeTipoOperacion(idTrans);
        idTrans = replaceOrdinal(idTrans);

        // XXXX1803XX.1150.XXXXX
        return idTrans.substring(0, 4) + idTrans.substring(8, 10) + idTrans.substring(16);
    }

}

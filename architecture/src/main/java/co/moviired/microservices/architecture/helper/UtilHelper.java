package co.moviired.microservices.architecture.helper;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-16
 * @since 1.0
 */
@SuppressWarnings({"unused", "unsafe"})
public class UtilHelper {

    private static final Logger LOGGER = LogManager.getLogger(UtilHelper.class.getName());

    public static boolean isNumeric(String str) {
        return ((str != null) && (!str.trim().isEmpty()) && (str.matches("[+-]?\\d*(\\.\\d+)?")));
    }

    public static boolean estaEnArreglo(String[] arreglo, String valor) {
        boolean resultado = false;
        for (String value : arreglo)
            if (value.equals(valor)) {
                resultado = true;
                break;
            }

        return resultado;
    }

    public static String strPad(String source, Integer length, String character, Integer direction) {
        String complete;
        if (direction == 0)
            complete = StringUtils.leftPad(source, length, character);
        else
            complete = StringUtils.rightPad(source, length, character);

        return complete;
    }

    public static String getRealContextPath(HttpServletRequest req) {
        StringBuilder contextPath = new StringBuilder();
        contextPath.append(req.getScheme()); // Protocolo
        contextPath.append("://");
        contextPath.append(req.getServerName());     // Server name
        contextPath.append(":");
        contextPath.append(req.getServerPort());        // port
        contextPath.append(req.getContextPath());   // /mywebapp
        contextPath.append("/");
        contextPath.trimToSize();

        return contextPath.toString();
    }

    public static Properties getPropertiesFile(String rutaArchivo) {
        Properties lproperties = new Properties();
        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(rutaArchivo)) {
            lproperties.load(in);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return lproperties;
    }

    public static byte[] getBytesFromFile(String fileName) throws IOException {
        byte[] bytesArch = null;
        try {
            if (fileName != null) {
                // Obtener el flujo de bytes del archivo
                File file = new File(fileName);
                bytesArch = new byte[(int) file.length()];
                FileInputStream fileInputStream = new FileInputStream(file);
                int lecturaValida = fileInputStream.read(bytesArch);
                fileInputStream.close();
            }

        } catch (Exception e) {
            throw new IOException("ERROR: " + e.getMessage());
        }
        return bytesArch;
    }

    public static void encriptarClave(String clave) {
        PasswordEncoder passwordEncoder;
        Map<String, String> encriptadas = new HashMap<>();

        // BCrypt - PasswordEncoder
        passwordEncoder = new BCryptPasswordEncoder();
        encriptadas.put("BCrypt", passwordEncoder.encode(clave));

        // Md5 - PasswordEncoder
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        encriptadas.put("Md5", md5.encodePassword(clave, ""));

        // Sha - PasswordEncoder
        ShaPasswordEncoder sha = new ShaPasswordEncoder();
        encriptadas.put("Sha", sha.encodePassword(clave, ""));

        System.out.println("CLAVE: " + clave);
        for (String key : encriptadas.keySet())
            System.out.println("\t" + key + ": " + encriptadas.get(key));

    }

    public static String getDateInformation(Date currentDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(currentDate);
    }


    /*public static void main(String[] args) {
        String clave = "M4h1ndr4...!";
        encriptarClave(clave);

    }*/

}

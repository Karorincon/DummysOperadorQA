package co.moviired.microservices.architecture.model;

import org.springframework.core.annotation.AnnotationAttributes;


/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class MicroservicesAnnotationAttributes {

    public MicroservicesAnnotationAttributes(AnnotationAttributes annotationAttributes) {
        super();
        this.annotationAttributes = annotationAttributes;
    }

    private AnnotationAttributes annotationAttributes;

    public AnnotationAttributes getAnnotationAttributes() {
        return annotationAttributes;
    }

}

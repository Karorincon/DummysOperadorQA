package co.moviired.microservices.architecture.exception;

import co.moviired.microservices.architecture.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class WSConfigException extends ServiceException {

    public WSConfigException() {
        super(ErrorType.CONFIGURATION, "500", "La configuración del servicio presenta errores");
    }

    public WSConfigException(String codigo, String mensaje) {
        super(ErrorType.CONFIGURATION, "500", mensaje);
    }

    public WSConfigException(Exception e) {
        super(ErrorType.CONFIGURATION, "500", e.getMessage());
    }

}

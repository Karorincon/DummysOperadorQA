package co.moviired.microservices.architecture.exception;

import co.moviired.microservices.architecture.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class ServiceException extends Exception {

    protected ErrorType tipo;
    protected String codigo;
    protected String mensaje;


    public ServiceException(ErrorType tipo, String codigo, String mensaje) {
        super(mensaje);

        this.tipo = tipo;
        this.codigo = codigo;
        this.mensaje = mensaje;

    }

    public ErrorType getTipo() {
        return tipo;
    }

    public void setTipo(ErrorType tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ServiceException: { tipo=" + tipo + ", codigo=" + codigo + ", mensaje='" + mensaje + "' }";
    }
}

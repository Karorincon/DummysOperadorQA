package co.moviired.microservices.architecture.util;

public enum ThirdPartyEnum {
    CLARO(1),
    MOVISTAR(2),
    TIGO(3),
    DIRECT_TV(4),
    VIRGIN(5),
    VIRGIN_SOAP(6),
    ETB(7);

    private final int id;

    private ThirdPartyEnum(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }


}

package co.moviired.microservices.architecture.auth.accesstoken;

import co.moviired.microservices.architecture.provider.userdetails.domain.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.*;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@SuppressWarnings("unchecked")
public final class TokenHandler {

    private final String secret;
    private final Integer expiration;
    private final UserDetailsService userService;

    public TokenHandler(String secret, Integer expiration, UserDetailsService userService) {
        this.secret = secret;
        this.expiration = expiration;
        this.userService = userService;
    }

    public UserDetails parseUserFromToken(String token) {
        UserDetails user;
        Jws<Claims> jwt = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        String username = (String) jwt.getBody().get("username");
        Calendar expr = Calendar.getInstance();
        expr.setTimeInMillis((Long) jwt.getBody().get("expiration"));


        // Verificar el usuario
        if (jwt.getBody().containsKey("roles")) {
            List map = (ArrayList) jwt.getBody().get("roles");
            user = getUserDetails(username, map);
        } else {
            user = userService.loadUserByUsername(username);
        }

        // Verificar la fecha de expiración
        Calendar cal = Calendar.getInstance();
        if (cal.getTime().after(expr.getTime())) {
            user = null;
            System.out.println("Token expirado");
        }

        return user;
    }

    public String createTokenForUser(Authentication user) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, expiration);

        Map<String, Object> claims = new HashMap<>();
        claims.put("username", user.getName());
        claims.put("expiration", cal.getTime());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public String createTokenForUserStoreInJwt(UserAuthentication user) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, expiration);

        Map<String, Object> claims = new HashMap<>();
        claims.put("username", user.getName());
        claims.put("expiration", cal.getTime());
        claims.put("roles", ((UserSecurityDetails) user.getDetails()).getRoles());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    private UserDetails getUserDetails(String username, List<Map<String, Object>> rolesMap) {
        List<Role> roles = new ArrayList<>();
        for (Map<String, Object> roleMap : rolesMap) {
            String name = (String) roleMap.get("name");
            List<Permission> permissions = new ArrayList<>();
            List<Map<String, Object>> permissionsMap = (List<Map<String, Object>>) roleMap.get("permissions");
            for (Map<String, Object> permissionMap : permissionsMap) {
                Permission permission = new Permission();
                if (permissionMap.containsKey("allowAll")) {
                    permission.setAllowAll((boolean) permissionMap.get("allowAll"));
                }
                if (permissionMap.containsKey("method")) {
                    permission.setMethod((String) permissionMap.get("method"));
                }
                if (permissionMap.containsKey("ownEntities")) {
                    permission.setOwnEntities((boolean) permissionMap.get("ownEntities"));
                }
                if (permissionMap.containsKey("resource")) {
                    permission.setResource((String) permissionMap.get("resource"));
                }
                if (permissionMap.containsKey("allowIds")) {
                    permission.setAllowIds((List) permissionMap.get("allowIds"));
                }
                if (permissionMap.containsKey("denyIds")) {
                    permission.setDenyIds((List) permissionMap.get("denyIds"));
                }
                permissions.add(permission);
            }
            roles.add(new Role(name, permissions));
        }

        return new UserSecurityDetailsImpl(null, roles, null, username, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.moviired.microservices.architecture.conexion.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author dallanmarcelanunezraad
 */
@Entity
@Table(name = "error_codes")
@XmlRootElement
public class ErrorCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "third_party_id")
    private int thirdPartyId;
    @Basic(optional = false)
    @Column(name = "code")
    private String code;
    @Column(name = "status")
    private String status;
    @Column(name = "message")
    private String message;
    @Column(name = "error_type_id")
    private int errorTypeId;

    public ErrorCode() {
    }

    public ErrorCode(Integer id) {
        this.id = id;
    }

    public ErrorCode(Integer id, int thirdPartyId, String code) {
        this.id = id;
        this.thirdPartyId = thirdPartyId;
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(int productId) {
        this.thirdPartyId = thirdPartyId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getErrorTypeId() {
        return errorTypeId;
    }

    public void setErrorTypeId(int errorTypeId) {
        this.errorTypeId = errorTypeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ErrorCode)) {
            return false;
        }
        ErrorCode other = (ErrorCode) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ErrorCode[ id=" + id + " ]";
    }
    
}

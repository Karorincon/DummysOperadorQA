package co.moviired.microservices.architecture.configuration.security;

import co.moviired.microservices.architecture.auth.accesstoken.StatelessAuthenticationFilter;
import co.moviired.microservices.architecture.auth.accesstoken.StatelessLoginFilter;
import co.moviired.microservices.architecture.auth.accesstoken.TokenAuthenticationService;
import co.moviired.microservices.architecture.auth.permission.PermissionEvaluator;
import co.moviired.microservices.architecture.model.ArchitectureProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@ConditionalOnMissingBean(WebSecurityConfigurerAdapter.class)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AccessTokenSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ArchitectureProperties architectureProperties;

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return this.userService;
    }

    @Autowired
    private UserDetailsService userService;

    @Bean(name = "permissions")
    public PermissionEvaluator permissions() {
        return new PermissionEvaluator();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public TokenAuthenticationService tokenAuthenticationService() {
        String secret = architectureProperties.getSecurity().getAuthentication().getAccessToken().getSecret();
        Integer expiration = architectureProperties.getSecurity().getAuthentication().getAccessToken().getTokenValidityInSeconds();
        boolean storeUserInToken = architectureProperties.getSecurity().getAuthentication().getAccessToken().isStoreUserInToken();
        return new TokenAuthenticationService(secret, expiration, userService, storeUserInToken);
    }

    public AccessTokenSecurityConfig() {
        super(Boolean.TRUE);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling().and()
                .anonymous().and()
                .servletApi().and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()

                // Allow anonymous resource requests
                .antMatchers("/").permitAll()
                .antMatchers("/**/*.html").permitAll()
                .antMatchers("/**/*.ico").permitAll()
                .antMatchers("/**/*.ttf").permitAll()
                .antMatchers("/**/*.png").permitAll()
                .antMatchers("/**/*.css").permitAll()
                .antMatchers("/**/*.js").permitAll()

                // Allow anonymous logins
                .antMatchers("/auth**", "/login**").permitAll()
                .antMatchers("/**/ping*").permitAll()
                .antMatchers("/actuator", "/autoconfig", "/beans",
                        "/configprops", "/docs", "/dump", "/env",
                        "/flyway", "/health", "/info", "/liquibase",
                        "/logfile", "/metrics", "/mappings",
                        "/shutdown", "/trace", "/refresh").permitAll()


                // All other request need to be authenticated
                .anyRequest().authenticated().and()

                // Custom Token based authentication based on the header previously given to the client
                .addFilterBefore(new StatelessAuthenticationFilter(this.tokenAuthenticationService()), UsernamePasswordAuthenticationFilter.class)

                // Login to user and add token jwt
                .addFilterBefore(
                        new StatelessLoginFilter(
                                "/login",
                                this.tokenAuthenticationService(),
                                this.userDetailsService(),
                                this.authenticationManager()),
                        UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
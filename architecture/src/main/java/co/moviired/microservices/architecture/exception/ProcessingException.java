package co.moviired.microservices.architecture.exception;

import co.moviired.microservices.architecture.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class ProcessingException extends ServiceException {

    public ProcessingException() {
        super(ErrorType.PROCESSING, "500", "Ha ocurrido un erro al procesar la petición");
    }

    public ProcessingException(String codigo, String mensaje) {
        super(ErrorType.PROCESSING, "500", mensaje);
    }

    public ProcessingException(Exception e) {
        super(ErrorType.PROCESSING, "500", e.getMessage());
    }

}

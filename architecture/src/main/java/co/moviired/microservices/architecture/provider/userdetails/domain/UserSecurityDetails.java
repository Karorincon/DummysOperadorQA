package co.moviired.microservices.architecture.provider.userdetails.domain;

import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.List;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public interface UserSecurityDetails extends UserDetails {
    List<Role> getRoles();

    Serializable getId();
}

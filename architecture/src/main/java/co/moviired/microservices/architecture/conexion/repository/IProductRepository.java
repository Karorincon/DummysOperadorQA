package co.moviired.microservices.architecture.conexion.repository;

import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.exception.ORMException;
import org.springframework.data.repository.CrudRepository;

public interface IProductRepository extends CrudRepository<Product, Integer> {

    Product findByThirdPartyIdAndSubscriberIdAndCode(Integer thirdPartyId, Integer SubscriberId, String code)throws ORMException;

    Product findByThirdPartyIdAndCode (Integer thirdPartyId, String code)throws ORMException;

}

package co.moviired.microservices.architecture.model.output;

import co.moviired.microservices.architecture.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.*;
import org.springframework.http.HttpStatus;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result extends BaseModel {

    private String message;
    private Integer statusCode;
    private ErrorDetail error;

    // CONSTRUCTORES

    public Result() {
        super();
    }

    public Result(HttpStatus statusCode, ErrorDetail error) {
        super();
        this.statusCode = statusCode.value();
        this.message = statusCode.getReasonPhrase();
        this.error = error;
    }

    public Result(int statusCode, ErrorDetail error) {
        super();
        this.error = error;

        switch (statusCode) {
            case 400: {
                this.statusCode = HttpStatus.BAD_REQUEST.value();
                this.message = HttpStatus.BAD_REQUEST.getReasonPhrase();
            }
            break;

            case 401: {
                this.statusCode = HttpStatus.UNAUTHORIZED.value();
                this.message = HttpStatus.UNAUTHORIZED.getReasonPhrase();
            }
            break;

            case 403: {
                this.statusCode = HttpStatus.FORBIDDEN.value();
                this.message = HttpStatus.FORBIDDEN.getReasonPhrase();
            }
            break;

            case 408: {
                this.statusCode = HttpStatus.REQUEST_TIMEOUT.value();
                this.message = HttpStatus.REQUEST_TIMEOUT.getReasonPhrase();
            }
            break;

            case 503: {
                this.statusCode = HttpStatus.SERVICE_UNAVAILABLE.value();
                this.message = HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase();
            }
            break;

            default: {
                this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
                this.message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }
        }
    }

    // MÉTODOS UTILITARIOS

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append(this.statusCode).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Result))
            return false;

        Result otherCast = (Result) o;
        return new EqualsBuilder().append(this.statusCode, otherCast.statusCode).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.statusCode).toHashCode();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Result))
            return -1;

        return this.getStatusCode().compareTo(((Result) o).getStatusCode());
    }

    @Override
    public DiffResult diff(Object obj) {
        Result o = (Result) obj;
        return new DiffBuilder(this, obj, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("StatusCode", this.statusCode, o.statusCode)
                .append("StatusMessage", this.message, o.message)
                .build();
    }

    // MÉTODOS DE ACCESO


    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ErrorDetail getError() {
        return error;
    }

    public void setError(ErrorDetail error) {
        this.error = error;
    }
}

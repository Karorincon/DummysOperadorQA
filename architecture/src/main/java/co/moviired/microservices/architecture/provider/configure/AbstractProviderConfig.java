package co.moviired.microservices.architecture.provider.configure;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public interface AbstractProviderConfig extends AbstractUserDetailsConfig {
}

package co.moviired.microservices.architecture.provider.configure;

import co.moviired.microservices.architecture.configuration.security.AccessTokenSecurityConfig;
import co.moviired.microservices.architecture.provider.userdetails.service.InMemoryUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@ConditionalOnMissingBean(AbstractProviderConfig.class)
@Configuration
public class InMemoryProviderConfig implements AbstractProviderConfig {

    @Autowired
    private CacheManager cacheManager;

    @ConditionalOnBean(value = AccessTokenSecurityConfig.class)
    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsService(cacheManager);
    }

}

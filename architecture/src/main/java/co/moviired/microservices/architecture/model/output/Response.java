package co.moviired.microservices.architecture.model.output;

import co.moviired.microservices.architecture.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.*;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response extends BaseModel {

    private Data data;
    private Result outcome;

    // CONSTRUCTORES

    public Response() {
        super();
    }

    public Response(Result outcome, Data data) {
        super();
        this.outcome = outcome;
        this.data = data;
    }

    // MÉTODOS UTILITARIOS

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append(this.outcome).append(this.data).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Response))
            return false;

        Response otherCast = (Response) o;
        return new EqualsBuilder().append(this.outcome, otherCast.outcome).append(this.data, otherCast.data).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.outcome).append(this.data).toHashCode();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Response))
            return -1;

        return this.outcome.compareTo(((Response) o).outcome);
    }

    @Override
    public DiffResult diff(Object obj) {
        Response o = (Response) obj;
        return new DiffBuilder(this, obj, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("Result", this.outcome.getStatusCode(), o.outcome.getStatusCode())
                .build();
    }

    // MÉTODOS DE ACCESO

    public Result getOutcome() {
        return outcome;
    }

    public void setOutcome(Result outcome) {
        this.outcome = outcome;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}

package co.moviired.microservices.architecture.model;

import co.moviired.microservices.architecture.configuration.security.AccessTokenSecurityConfig;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public enum SecurityMode {
    NONE,
    CUSTOM,
    ACCESS_TOKEN(AccessTokenSecurityConfig.class);

    private final Class<?>[] configClass;

    public Class<?>[] getConfigClass() {
        return configClass;
    }

    SecurityMode(Class<?>... configClass) {
        this.configClass = configClass;
    }
}

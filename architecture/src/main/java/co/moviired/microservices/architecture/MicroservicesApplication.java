package co.moviired.microservices.architecture;

import co.moviired.microservices.architecture.configuration.security.AccessTokenSecurityConfig;
import co.moviired.microservices.architecture.model.DataAccessMode;
import co.moviired.microservices.architecture.model.SecurityMode;
import co.moviired.microservices.architecture.provider.configure.AbstractUserDetailsConfig;
import co.moviired.microservices.architecture.provider.configure.InMemoryProviderConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.lang.annotation.*;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Configuration
@EnableAutoConfiguration
@ComponentScan
@Import({ArchitectureConfig.class, ArchitectureImportSelector.class})
public @interface MicroservicesApplication {
    Class<? extends AbstractUserDetailsConfig> customUserDetailsServiceConfig() default InMemoryProviderConfig.class;

    Class<? extends WebSecurityConfigurerAdapter> customSecurityConfig() default AccessTokenSecurityConfig.class;

    SecurityMode securityMode() default SecurityMode.ACCESS_TOKEN;

    DataAccessMode dataAccessMode() default DataAccessMode.IN_MEMORY;

    boolean enableWebSocket() default true;

    String applicationName() default "";

}
package co.moviired.microservices.architecture.model.input;

import co.moviired.microservices.architecture.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.*;

import java.util.HashMap;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request extends BaseModel {

    private HashMap<String, Object> meta;
    private HashMap<String, Object> data;
    private RequestSignature requestSignature;

    // CONSTRUCTORES

    public Request() {
        super();
    }

    public Request(HashMap<String, Object> meta, RequestSignature requestSignature, HashMap<String, Object> data) {
        super();
        this.meta = meta;
        this.data = data;
        this.requestSignature = requestSignature;
    }

    // MÉTODOS UTILITARIOS

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append(this.meta).append(this.requestSignature).append(this.data).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Request))
            return false;

        Request otherCast = (Request) o;
        return new EqualsBuilder().append(this.meta, otherCast.meta).append(this.requestSignature, otherCast.requestSignature).append(this.data, otherCast.data).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.requestSignature).append(this.meta).append(this.data).toHashCode();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Request))
            return -1;

        // TODO Ajustar el compareTo de la petición
        // return this.meta.compareTo(((Request) o).meta);
        return 0;
    }

    @Override
    public DiffResult diff(Object obj) {
        Request o = (Request) obj;
        return new DiffBuilder(this, obj, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("Identificación", this.meta, o.meta)
                .append("RequestSignature", this.requestSignature, o.requestSignature)
                .append("Parámetros", this.data, o.data)
                .build();
    }

    // MÉTODOS DE ACCESO

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }

    public HashMap<String, Object> getMeta() {
        return meta;
    }

    public void setMeta(HashMap<String, Object> meta) {
        this.meta = meta;
    }

    public RequestSignature getRequestSignature() {
        return requestSignature;
    }

    public void setRequestSignature(RequestSignature requestSignature) {
        this.requestSignature = requestSignature;
    }
}

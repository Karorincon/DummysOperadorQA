package co.moviired.microservices.architecture.util;

import co.moviired.microservices.architecture.exception.ServiceException;
import co.moviired.microservices.architecture.exception.WSConfigException;

import java.io.InputStream;
import java.util.Properties;
import java.util.ResourceBundle;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class PropertiesHelper {

    public static Properties getPropertiesFile(String rutaArchivo) {
        Properties lproperties = new Properties();
        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(rutaArchivo)) {
            lproperties.load(in);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return lproperties;
    }

    public static void validateProperties(ResourceBundle properties, String[] strProperties) throws ServiceException {
        if (strProperties.length > 0) {
            for (String propString : strProperties) {
                String propToValidate = properties.getString(propString);

                if ((propToValidate.trim().isEmpty())) {
                    throw new WSConfigException("-3", "ErrorDetail leyendo archivo de propiedades(Parámetro " + propString + " no encontrado)");
                }
            }
        }
    }

    public static void validateProperties(Properties properties, String[] strProperties) throws ServiceException {
        if (strProperties.length > 0) {
            for (String propString : strProperties) {
                String propToValidate = properties.getProperty(propString);

                if ((propToValidate == null) || (propToValidate.trim().isEmpty())) {
                    throw new WSConfigException("-3", "ErrorDetail leyendo archivo de propiedades(Parámetro " + propString + " no encontrado)");
                }
            }
        }
    }
}

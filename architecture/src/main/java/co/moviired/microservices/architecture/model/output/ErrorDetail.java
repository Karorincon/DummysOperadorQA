package co.moviired.microservices.architecture.model.output;

import co.moviired.microservices.architecture.base.BaseModel;
import co.moviired.microservices.architecture.exception.ServiceException;
import co.moviired.microservices.architecture.model.enums.ErrorType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.*;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorDetail extends BaseModel {

    private String errorMessage;
    private Integer errorType;
    private String errorCode;

    // CONSTRUCTORES

    public ErrorDetail(ErrorType errorType, String errorCode, String errorMessage) {
        super();
        this.errorType = errorType.ordinal();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ErrorDetail(Integer errorType, String errorCode, String errorMessage) {
        super();
        this.errorType = errorType;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ErrorDetail(ServiceException se) {
        this(se.getTipo(), se.getCodigo(), se.getMensaje());
    }


    // MÉTODOS UTILITARIOS

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append(this.errorType).append(this.errorCode).append(this.errorMessage).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ErrorDetail))
            return false;

        ErrorDetail otherCast = (ErrorDetail) o;
        return new EqualsBuilder().append(this.errorType, otherCast.errorType).append(this.errorCode, otherCast.errorCode).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.errorType).append(this.errorCode).toHashCode();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof ErrorDetail))
            return -1;

        return this.errorCode.compareTo(((ErrorDetail) o).errorCode);
    }

    @Override
    public DiffResult diff(Object obj) {
        ErrorDetail o = (ErrorDetail) obj;
        return new DiffBuilder(this, obj, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("Tipo", this.errorType, o.errorType)
                .append("Código", this.errorCode, o.errorCode)
                .append("Detalle", this.errorMessage, o.errorMessage)
                .build();
    }

    // MÉTODOS DE ACCESO

    public Integer getErrorType() {
        return errorType;
    }

    public void setErrorType(Integer errorType) {
        this.errorType = errorType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

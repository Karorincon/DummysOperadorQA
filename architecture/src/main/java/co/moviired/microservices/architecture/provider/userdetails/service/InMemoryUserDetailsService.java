package co.moviired.microservices.architecture.provider.userdetails.service;

import co.moviired.microservices.architecture.provider.userdetails.domain.Permission;
import co.moviired.microservices.architecture.provider.userdetails.domain.Role;
import co.moviired.microservices.architecture.provider.userdetails.domain.UserSecurityDetailsImpl;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class InMemoryUserDetailsService extends AbstractUserDetailsService {
    private static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public InMemoryUserDetailsService(CacheManager cacheManager) {
        super(cacheManager);
        populateCache(cache);
    }

    private void populateCache(Cache cache) {
        ResourceBundle users = ResourceBundle.getBundle("users");

        List<Role> roles = new ArrayList<>();
        List<Permission> permissions = new ArrayList<>();

        String[] permisos = users.getString("permissions").split("\\|");
        for (String perm : permisos) {
            Permission permission = new Permission();
            String[] parts = perm.split("-");
            permission.setResource(parts[0]);
            permission.setMethod(parts[1]);
            permissions.add(permission);
        }

        Role role = new Role(users.getString("role"), permissions);
        roles.add(role);

        UserSecurityDetailsImpl user = new UserSecurityDetailsImpl(
                users.getString("username"),
                passwordEncoder.encode(users.getString("password")),
                roles
        );

        cache.put(users.getString("username"), user);
    }

    protected UserSecurityDetailsImpl findUser(String username) {

        return cache.get(username, UserSecurityDetailsImpl.class);
    }

}
package co.moviired.microservices.architecture;

import co.moviired.microservices.architecture.configuration.WebSocketConfig;
import co.moviired.microservices.architecture.model.DataAccessMode;
import co.moviired.microservices.architecture.model.SecurityMode;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class ArchitectureImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        AnnotationAttributes attributes = AnnotationAttributes
                .fromMap(importingClassMetadata.getAnnotationAttributes(MicroservicesApplication.class.getName()));
        Class<?> customUserDetailsServiceConfig = attributes.getClass("customUserDetailsServiceConfig");
        Class<?> customSegurityConfig = attributes.getClass("customSecurityConfig");

        SecurityMode securityMode = attributes.getEnum("securityMode");
        DataAccessMode dataAccessMode = attributes.getEnum("dataAccessMode");
        boolean enableWebSocket = attributes.getBoolean("enableWebSocket");

        List<String> configRoutes = new ArrayList<>();
        if (SecurityMode.CUSTOM.equals(securityMode)) {
            configRoutes.add(customSegurityConfig.getName());
        } else if (!SecurityMode.NONE.equals(securityMode)) {
            for (Class<?> securityConfigClass : securityMode.getConfigClass()) {
                configRoutes.add(securityConfigClass.getName());
            }
        }

        if (DataAccessMode.CUSTOM.equals(dataAccessMode)) {
            configRoutes.add(customUserDetailsServiceConfig.getName());
        } else {
            configRoutes.add(dataAccessMode.getConfigClass().getName());
        }

        if (enableWebSocket) {
            configRoutes.add(WebSocketConfig.class.getName());
        }

        return configRoutes.toArray(new String[0]);
    }

}

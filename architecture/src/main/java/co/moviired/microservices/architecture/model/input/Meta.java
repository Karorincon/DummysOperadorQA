package co.moviired.microservices.architecture.model.input;

import co.moviired.microservices.architecture.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.*;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Meta extends BaseModel {

    private String requestDate;

    private String customerId;

    private String deviceCode;

    private String userName;

    private String password;

    private String passwordHash;

    private String channel;

    private String systemId;

    private String requestReference;

    private String requestSource;

    private String originAddress;


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append(this.customerId).append(this.userName).append(this.password).append(this.requestDate).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Meta))
            return false;

        Meta otherCast = (Meta) o;
        return new EqualsBuilder().append(this.userName, otherCast.userName).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.userName).toHashCode();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Meta))
            return -1;

        return this.userName.compareTo(((Meta) o).userName);
    }

    @Override
    public DiffResult diff(Object obj) {
        Meta o = (Meta) obj;
        return new DiffBuilder(this, obj, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("Customer", this.customerId, o.customerId)
                .append("Username", this.userName, o.userName)
                .append("Password", this.password, o.password)
                .append("Request date", this.requestDate, o.requestDate)
                .append("Refernce number", this.requestReference, o.requestReference)
                .build();
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getRequestReference() {
        return requestReference;
    }

    public void setRequestReference(String requestReference) {
        this.requestReference = requestReference;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getRequestSource() {
        return requestSource;
    }

    public void setRequestSource(String requestSource) {
        this.requestSource = requestSource;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }
}

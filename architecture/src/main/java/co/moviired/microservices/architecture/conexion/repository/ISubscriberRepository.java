package co.moviired.microservices.architecture.conexion.repository;

import co.moviired.microservices.architecture.conexion.entities.Subscriber;
import co.moviired.microservices.architecture.exception.ORMException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ISubscriberRepository extends JpaRepository<Subscriber, Integer> {

    Subscriber findByThirdPartyIdAndMsisdn(Integer thirdPartyId, String msisdn) throws ORMException;

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Subscriber c SET c.totalNet = :totalNet WHERE c.id = :id")
    int updateTotalNet(@Param("totalNet") Double totalNet, @Param("id") Integer id);
}



package co.moviired.microservices.architecture.base;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.Diffable;

import java.io.Serializable;

/*
 * Copyright @2018. SBD, S.A.S. Todos los derechos reservados.
 *
 * Clase abstracta a ser utilizada como base para todas las clases del MODELO DE DATOS de la solución
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseModel implements Serializable, Comparable, Diffable {

    private static final long serialVersionUID = -595273616214410486L;

    // Utilizado para obtener una representación en String del registro en la base de datos
    public abstract String toString();

    // Utilizado para hacer la comparación de dos registros en la base de datos
    public abstract boolean equals(Object o);

    // Utilizado para generar un código hash para identificar al objeto
    public abstract int hashCode();

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    public DiffResult diff(Object obj) {
        return null;
    }

}

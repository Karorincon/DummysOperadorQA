package co.moviired.microservices.architecture.exception;

import co.moviired.microservices.architecture.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class CommunicationException extends ServiceException {

    public CommunicationException() {
        super(ErrorType.COMMUNICATION, "408", "Servicio provisto por el operador no disponible");
    }

    public CommunicationException(String codigo, String mensaje) {
        super(ErrorType.COMMUNICATION, "408", mensaje);
    }

    public CommunicationException(Exception e) {
        super(ErrorType.COMMUNICATION, "408", e.getMessage());
    }

}

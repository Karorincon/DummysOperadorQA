package co.moviired.microservices.architecture.exception;

import co.moviired.microservices.architecture.util.ErrorTypeEnum;

public class ErrorTypeException extends  Exception{
    private ErrorTypeEnum errorTypeEnum;

    public ErrorTypeException(ErrorTypeEnum errorTypeEnum){
        super(errorTypeEnum.toString());
        this.errorTypeEnum = errorTypeEnum;
    }

    public ErrorTypeEnum getErrorTypeEnum() {
        return errorTypeEnum;
    }
}

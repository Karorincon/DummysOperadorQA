package co.moviired.microservices.architecture.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/*
 * Copyright @2018. SBD, S.A.S. Todos los derechos reservados.
 *
 * Clase a ser utilizada como base para todas las clases de SERVICIO DE NEGOCIO
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@Service
public abstract class BaseService implements ServletContextAware {

    @Autowired
    protected ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

}

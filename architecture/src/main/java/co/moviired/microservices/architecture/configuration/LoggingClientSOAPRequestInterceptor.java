package co.moviired.microservices.architecture.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public class LoggingClientSOAPRequestInterceptor implements SOAPHandler<SOAPMessageContext> {
    private static final Logger LOGGER = LogManager.getLogger(LoggingClientSOAPRequestInterceptor.class.getName());

    public void close(MessageContext context) {
    }

    public boolean handleFault(SOAPMessageContext context) {
        logMessage(context, "SOAP Error is : ");
        return true;
    }

    public boolean handleMessage(SOAPMessageContext context) {
        logMessage(context, "SOAP Message is : ");
        return true;
    }

    public Set<QName> getHeaders() {
        return Collections.EMPTY_SET;
    }

    private boolean logMessage(MessageContext mc, String type) {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.info(type);
                SOAPMessage msg = ((SOAPMessageContext) mc)
                        .getMessage();

                // Print out the Mime Headers
                MimeHeaders mimeHeaders = msg.getMimeHeaders();
                Iterator mhIterator = mimeHeaders.getAllHeaders();
                MimeHeader mh;
                String header;
                LOGGER.info("  Mime Headers:");
                while (mhIterator.hasNext()) {
                    mh = (MimeHeader) mhIterator.next();
                    header = new StringBuffer(" Name: ")
                            .append(mh.getName()).append(" Value: ")
                            .append(mh.getValue()).toString();
                    LOGGER.debug(header);
                }

                LOGGER.info(" SOAP Message: ");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                msg.writeTo(baos);
                LOGGER.info("   " + baos.toString());
                baos.close();
            }

            return true;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error logging SOAP message",
                        e);
            }

            return false;
        }
    }
}

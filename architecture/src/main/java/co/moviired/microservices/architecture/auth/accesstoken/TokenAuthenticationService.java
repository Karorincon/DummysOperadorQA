package co.moviired.microservices.architecture.auth.accesstoken;

import co.moviired.microservices.architecture.provider.userdetails.domain.UserAuthentication;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class TokenAuthenticationService {

    private static final String AUTH_HEADER_NAME = "Authorization";

    private final TokenHandler tokenHandler;
    private final boolean storeUserInToken;

    public TokenAuthenticationService(String secret, Integer expiration, UserDetailsService userService, boolean storeUserInToken) {
        tokenHandler = new TokenHandler(secret, expiration, userService);
        this.storeUserInToken = storeUserInToken;
    }

    public String addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
        String token;
        if (storeUserInToken) {
            token = tokenHandler.createTokenForUserStoreInJwt(authentication);
        } else {
            token = tokenHandler.createTokenForUser(authentication);
        }
        response.addHeader(AUTH_HEADER_NAME, token);

        return token;
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        Authentication auth = null;
        final String token = request.getHeader(AUTH_HEADER_NAME);
        if (token != null) {
            try {
                final UserDetails user = tokenHandler.parseUserFromToken(token);
                if (user == null)
                    throw new UsernameNotFoundException("Token inválido...");

                auth = new UserAuthentication(user);
            } catch (Exception e) {
                System.out.println("Token inválido...");
            }
        }

        return auth;
    }

}
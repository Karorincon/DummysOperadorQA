package co.moviired.microservices.architecture.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/*
 * Copyright @2018. SBD, S.A.S. Todos los derechos reservados.
 *
 * Clase a ser utilizada como base para todas las clases de LOS WEB SERVICES
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@RestController
public class BaseWS implements ServletContextAware {

    @Autowired
    protected ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

}

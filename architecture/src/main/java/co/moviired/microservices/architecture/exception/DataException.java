package co.moviired.microservices.architecture.exception;

import co.moviired.microservices.architecture.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
public class DataException extends ServiceException {

    public DataException() {
        super(ErrorType.DATA, "500", "Los datos provistos en la petición no son correctos o no se proporcionaron");
    }

    public DataException(String codigo, String mensaje) {
        super(ErrorType.DATA, "500", mensaje);
    }

    public DataException(Exception e) {
        super(ErrorType.DATA, "500", e.getMessage());
    }

}

package co.moviired.microservices.architecture.util;

public enum ErrorTypeEnum {
    GREATER_MAXIMUM_VALUE(1),
    LOWER_MINOR_VALUE(2),
    INVALID_AMOUNT(3),
    SUCCESSFULL(4),
    INVALID_MSISDN(5),
    EMPTY_MSISDN(6),
    INVALID_ACCOUNT(7),
    INVALID_PRODUCT(8),
    INVALID_CONTROL_NUMBER(9),
    FAILED_TRANSACTION(10),
    NO_CREDIT(11);


    private final  int id;

    private ErrorTypeEnum(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

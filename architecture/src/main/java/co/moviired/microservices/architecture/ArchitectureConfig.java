package co.moviired.microservices.architecture;

import co.moviired.microservices.architecture.configuration.CacheConfig;
import co.moviired.microservices.architecture.model.ArchitectureProperties;
import co.moviired.microservices.architecture.model.MicroservicesAnnotationAttributes;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.UUID;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@ConditionalOnMissingBean(ArchitectureConfig.class)
@Configuration
@EnableAutoConfiguration
@Import({CacheConfig.class})
public class ArchitectureConfig implements ImportAware {

    public static final UUID APPLICATION_ID = UUID.randomUUID();

    private MicroservicesAnnotationAttributes moviiredMetadataAnotation;

    @Bean
    public MicroservicesAnnotationAttributes moviiredMetadataAnotation() {
        return moviiredMetadataAnotation;
    }

    @Bean
    public ArchitectureProperties architectureProperties() {
        return new ArchitectureProperties();
    }

    @Bean
    public StaticApplicationContextAccess staticApplicationContextAccess() {
        return new StaticApplicationContextAccess();
    }

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        AnnotationAttributes attributes = AnnotationAttributes
                .fromMap(importMetadata.getAnnotationAttributes(MicroservicesApplication.class.getName()));
        moviiredMetadataAnotation = new MicroservicesAnnotationAttributes(attributes);

    }

}

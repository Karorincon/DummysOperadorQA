package co.moviired.microservices.dummy.etb.controller;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.XmlRpcErrorLogger;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.XmlRpcServletServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.moviired.microservices.dummy.etb.handle.IHandler;
import co.moviired.microservices.dummy.etb.handle.SpringHandlerMapping;
import co.moviired.microservices.dummy.etb.handle.SpringRequestProcessorFactoryFactory;

@Controller
public class XmlRpcServerController {

	  @Autowired(required=false) private String encoding = 
			    XmlRpcServerConfigImpl.UTF8_ENCODING;
	  @Autowired(required=false) private boolean enabledForExceptions = true;
	  @Autowired(required=false) private boolean enabledForExtensions = true;
	  @Autowired(required=false) private int maxThreads = 1;
	  @Autowired private Map<String,IHandler> handlers;
	  
	  private XmlRpcServletServer server;
	  
	  @PostConstruct
	  protected void init() throws Exception {
	    XmlRpcServerConfigImpl config = new XmlRpcServerConfigImpl();
	    config.setBasicEncoding(encoding);
	    config.setEnabledForExceptions(enabledForExceptions);
	    config.setEnabledForExtensions(enabledForExtensions);
	    
	    this.server = new XmlRpcServletServer();
	    this.server.setConfig(config);
	    this.server.setErrorLogger(new XmlRpcErrorLogger());
	    this.server.setMaxThreads(maxThreads);

	    SpringHandlerMapping handlerMapping = new SpringHandlerMapping();
	    handlerMapping.setRequestProcessorFactoryFactory(
	      new SpringRequestProcessorFactoryFactory());
	    handlerMapping.setHandlerMappings(handlers);

	    this.server.setHandlerMapping(handlerMapping);
	  }
	
	  @RequestMapping(value="/Air", method=RequestMethod.POST)
	  public void serve(HttpServletRequest request, HttpServletResponse response) 
	      throws XmlRpcException {
	    try {
	      server.execute(request, response);
	    } catch (Exception e) {
	      throw new XmlRpcException(e.getMessage(), e);
	    }
	  }
	
	
}

package co.moviired.microservices.dummy.etb.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import co.moviired.microservices.dummy.etb.handle.IHandler;

@Service("etbService")
public class EtbService implements IHandler{
	
	private IProductRepository productRepository;
    private IErrorCodeRepository errorCodeRepository;
    
    @Autowired
    public EtbService(IProductRepository productRepository,IErrorCodeRepository errorCodeRepository ) {
    	this.productRepository = productRepository;
    	this.errorCodeRepository = errorCodeRepository;
    }
	
	private static final Logger LOGGER = LogManager.getLogger(EtbService.class.getName());
	
	public Map<String, Object> Refill(Map<String, Object> map) {
		ErrorTypeEnum errorTypeEnum=null;
		 Double amount=0.0;
		 Product product=null;

        try{
            /**
             * Validamos que el campo cuente con la cantidad
             */
            if(map.get("transactionAmount")==null)
                throw  new ErrorTypeException(ErrorTypeEnum.INVALID_AMOUNT);
            
            product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.ETB.getId(), "1");
            LOGGER.info("product= "+product);

            if(product==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_PRODUCT);
            
            if(product.getConnectTimeout()!=null)
            	this.delayMethod(product.getConnectTimeout());

            amount = new Double((map.get("transactionAmount").toString()));

            if (amount<product.getMinValue())
                throw  new ErrorTypeException (ErrorTypeEnum.LOWER_MINOR_VALUE);

            if (amount > product.getMaxValue())
                throw  new ErrorTypeException (ErrorTypeEnum.GREATER_MAXIMUM_VALUE);

            String rechargeNumber=map.get("subscriberNumber")!=null ? map.get("subscriberNumber").toString() : null ;
            
            if (rechargeNumber==null ||rechargeNumber.isEmpty())
                throw  new ErrorTypeException (ErrorTypeEnum.EMPTY_MSISDN);

            if (product.getRegex() != null && !product.getRegex().isEmpty()
                    && !Pattern.compile(product.getRegex()).matcher(rechargeNumber).matches())
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_MSISDN);


        }catch (ErrorTypeException ex){
            LOGGER.error("ErrorTypeException: "+ex.getMessage());
            errorTypeEnum = ex.getErrorTypeEnum();

        }catch (Exception ex){
            LOGGER.error("Exception : "+ex.getMessage());
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;


        }

        if (errorTypeEnum==null)
            errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;

        ErrorCode error = this.errorCodeRepository
                .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.ETB.getId(),
                        errorTypeEnum.getId());
        
        if(error!=null) 
        	 LOGGER.info("codigo: "+error.getCode()+" - estatus de la transacción: "+ error.getMessage());
        else 
        	 LOGGER.info("Ha ocurrido un error y no se ha podido determinar cual fue ya que no fue maqueado el error cuyo codigo eso: => "+errorTypeEnum.getId()+ " - valor: "+errorTypeEnum);
        
        Map<String, Object> object= new HashMap<String, Object>();
        object.put("responseCode", error!=null ? error.getCode(): "99");
        object.put("transactionAmount", amount);
        object.put("segmentationID", this.getAutorizationNumber());
        object.put("originTransactionID", this.getAutorizationNumber());
        object.put("currency1", amount);
        object.put("masterAccountNumber", this.getAutorizationNumber());
        
        
        if(product!=null && product.getRequestTimeout()!=null)
        	this.delayMethod(product.getRequestTimeout());

   
        return object;

	}
	
	/**
     * Método que genera un número de autorizado
     * @return
     */
    private String getAutorizationNumber(){
        Calendar cal = Calendar.getInstance();
        String year = (cal.get(Calendar.YEAR)+"").substring(1);
        String month= (cal.get(Calendar.MONTH)+1)+"";
        if (month.length()<2)month= "0"+month;
        String day= cal.get(Calendar.DAY_OF_MONTH)+"";
        if (day.length()<2)day= "0"+day;


        String hour= cal.get(Calendar.HOUR_OF_DAY)+"";
        if (hour.length()<2)hour= "0"+hour;

        String minute= cal.get(Calendar.MINUTE)+"";
        if (minute.length()<2)minute= "0"+minute;

        Random ran = new Random();
        int x = ran.nextInt(999) + 999;

        String resp= year+month+day
                +hour+minute
                +x;

        return resp;
    }
    
    /**
     * Método utilizado para retardar un método
     * @param time0
     */
    private void delayMethod(Integer time0) {
    	 try {
         	TimeUnit.MILLISECONDS.sleep(time0/1000);
         }catch (Exception e) {
				LOGGER.error("error con el tiempo de retardo....");
				LOGGER.error(e);
		}
    }

}

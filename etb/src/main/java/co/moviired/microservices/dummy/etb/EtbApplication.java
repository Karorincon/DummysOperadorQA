package co.moviired.microservices.dummy.etb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import co.moviired.microservices.architecture.MicroservicesApplication;
import co.moviired.microservices.architecture.model.DataAccessMode;
import co.moviired.microservices.architecture.model.SecurityMode;


@MicroservicesApplication(
        applicationName = "EtbApplication",
        securityMode = SecurityMode.NONE,
        dataAccessMode = DataAccessMode.IN_MEMORY)
//@ComponentScan({"com.delivery.request"})
@EntityScan("co.moviired.microservices.architecture.conexion.entities")
@EnableJpaRepositories("co.moviired.microservices.architecture.conexion.repository")
public class EtbApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(EtbApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(EtbApplication.class);
        app.addListeners(new ApplicationPidFileWriter("EtbApplication.pid"));
        app.run(args);
    }
}

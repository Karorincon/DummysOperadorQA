#!/usr/bin/env bash

mvn install:install-file -Dfile=directv/directv-commons-utils-1.0.jar -DgroupId=co.moviired -DartifactId=directv-commons-utils -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=directv/directv-connectws-directv-1.0.jar -DgroupId=co.moviired -DartifactId=directv-connectws-directv -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=virgin/virgin-client-1.0.jar -DgroupId=co.moviired -DartifactId=virgin-client -Dversion=1.0 -Dpackaging=jar


package co.moviired.microservices.dummy.claro.model.response;

import javax.xml.bind.annotation.*;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TXNSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXTREFNUM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TXNID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="REQSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MESSAGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "type",
        "txnstatus",
        "date",
        "extrefnum",
        "txnid",
        "reqstatus",
        "message"
})
@XmlRootElement(name = "COMMAND")
public class ResponseClaro {

    @XmlElement(name = "TYPE", required = true)
    protected String type;

    // Codigo Estado de la Transaccion
    @XmlElement(name = "TXNSTATUS", required = true)
    protected String txnstatus;

    //Fecha y Hora
    @XmlElement(name = "DATE", required = true)
    protected String date;

    //
    @XmlElement(name = "EXTREFNUM", required = true)
    protected String extrefnum;

    // ID de Transacción
    @XmlElement(name = "TXNID", required = true)
    protected String txnid;

    @XmlElement(name = "REQSTATUS")
    protected String reqstatus;

    // Mensaje
    @XmlElement(name = "MESSAGE", required = true)
    protected String message;

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param type allowed object is
     *             {@link String }
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTxnstatus() {
        return txnstatus;
    }

    /**
     * Sets the value of the type property.
     *
     * @param txnstatus allowed object is
     *                  {@link String }
     */
    public void setTxnstatus(String txnstatus) {
        this.txnstatus = txnstatus;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the type property.
     *
     * @param date allowed object is
     *             {@link String }
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getExtrefnum() {
        return extrefnum;
    }

    /**
     * Sets the value of the type property.
     *
     * @param extrefnum allowed object is
     *                  {@link String }
     */
    public void setExtrefnum(String extrefnum) {
        this.extrefnum = extrefnum;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTxnid() {
        return txnid;
    }

    /**
     * Sets the value of the type property.
     *
     * @param txnid allowed object is
     *              {@link String }
     */
    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the type property.
     *
     * @param message allowed object is
     *                {@link String }
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String getReqstatus() {
        return reqstatus;
    }


    public void setReqstatus(String reqstatus) {
        this.reqstatus = reqstatus;
    }
}

package co.moviired.microservices.dummy.claro.controller;

import co.moviired.microservices.architecture.base.BaseWS;
import co.moviired.microservices.architecture.model.enums.ErrorType;
import co.moviired.microservices.architecture.model.output.ErrorDetail;
import co.moviired.microservices.architecture.model.output.Response;
import co.moviired.microservices.architecture.model.output.Result;
import co.moviired.microservices.dummy.claro.model.request.RequestClaro;
import co.moviired.microservices.dummy.claro.model.response.ResponseClaro;
import co.moviired.microservices.dummy.claro.service.IClaroService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author NUÑEZ, Dallan
 * @version 1, 2018-01-28
 * @since 1.0
 */
@CrossOrigin
@RestController("claroWS")
public class ClaroWS extends BaseWS {


    @Value("${configuration_external_path}")
    private String configuration_external_path;

    private static final Logger LOGGER = LogManager.getLogger(ClaroWS.class.getName());
    private static final String FEATURE_NAMESPACES = "http://xml.org/sax/features/namespaces";
    private static final String FEATURE_NAMESPACE_PREFIXES = "http://xml.org/sax/features/namespace-prefixes";

    private final IClaroService claroService;

    private static JAXBContext context;
    private static JAXBContext contextResponse;

    static {
        try {
            context = JAXBContext.newInstance(RequestClaro.class);
            contextResponse = JAXBContext.newInstance(ResponseClaro.class);
        } catch (Exception ex) {
            LOGGER.error(ex);
            System.exit(0);
        }
    }
   // @XmlHeader("<?xml-stylesheet type='text/xsl' href='${baseuri}foo.xsl' ?>")


    @Autowired
    public ClaroWS(IClaroService claroService) {
        super();
        this.claroService = claroService;
    }

    // Método: PING
    @RequestMapping(value = "${spring.application.methods.ping}", method = RequestMethod.GET)
    public Response ping() {
        Result result;

        try {
            LOGGER.info("**** Ping CLARO: Iniciado ****");
            LOGGER.info("loogogg " );

            // Establecer la respuesta exitosa
            result = new Result(HttpStatus.OK, null);
            result.setError(null);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            // Capturar cualquier no esperado
            result = new Result(HttpStatus.INTERNAL_SERVER_ERROR, new ErrorDetail(ErrorType.PROCESSING, "-1", e.getMessage()));
        }
        LOGGER.info("**** Ping CLARO: Finalizado ****");

        // Devolver la respuesta
        return new Response(result, null);
    }




    @RequestMapping(value = "${spring.application.methods.topup}", method = RequestMethod.POST, consumes = "application/xml")
    @PreAuthorize("@permissions.allowAny('request', 'execute')")
    public String recarga(@RequestBody String request ) {
        try{

            LOGGER.info("request => "+request);
            RequestClaro requestClaro = this.passXmlToRequestClaro(request);
            ResponseClaro responseClaro= this.claroService.processTransaction(requestClaro);
            return this.getString(responseClaro);

        }catch (Exception ex){
            LOGGER.error("error => ", ex);
        }

        return "esto es una prueba";

    }

    /**
     * Método utilizado para pasar el xml y retorna un objeto
     * de tipo {@link RequestClaro}
     *
     * @param request String que contiene la forma de un xml
     *
     * @return
     * @throws Exception
     */
    private RequestClaro passXmlToRequestClaro(String request) throws Exception{

        Unmarshaller unmarshaller = context.createUnmarshaller();

        XMLReader xmlreader = XMLReaderFactory.createXMLReader();
        xmlreader.setFeature(FEATURE_NAMESPACES, true);
        xmlreader.setFeature(FEATURE_NAMESPACE_PREFIXES, true);
        xmlreader.setEntityResolver(new EntityResolver() {
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                return new InputSource(configuration_external_path+"/xml/command.dtd");
            }
        });

        InputSource input = new InputSource(new StringReader(request));
        Source source = new SAXSource(xmlreader, input);

        return (RequestClaro)unmarshaller.unmarshal(source);

    }

    /**
     * Método utilizado para retornar un string de un objeto java
     *
     * @param object objeto a convertir
     * @return
     * @throws Exception
     */
    private String getString(ResponseClaro object )throws  Exception{
        Marshaller marshallerRequest = contextResponse.createMarshaller();
        marshallerRequest.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter sw = new StringWriter();
        marshallerRequest.marshal(object, sw);
        return sw.toString();
    }

}

package co.moviired.microservices.dummy.claro.model.request;

import javax.xml.bind.annotation.*;
import java.util.Date;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXTNWCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LOGINID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PASSWORD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXTCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXTREFNUM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MSISDN2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TXNID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LANGUAGE1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LANGUAGE2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SELECTOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INFO1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INFO2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INFO3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INFO4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "type",
        "date",
        "extnwcode",
        "msisdn",
        "pin",
        "loginid",
        "password",
        "extcode",
        "extrefnum",
        "msisdn2",
        "amount",
        "txnid",
        "language1",
        "language2",
        "selector",
        "info1",
        "info2",
        "info3",
        "info4"
})
@XmlRootElement(name = "COMMAND")
public class RequestClaro {

    @XmlElement(name = "TYPE", required = true)
    protected String type;

    //Fecha y Hora
    @XmlElement(name = "DATE", required = true)
    protected String date;

    // Codigo de Red
    @XmlElement(name = "EXTNWCODE", required = true)
    protected String extnwcode;

    @XmlElement(name = "MSISDN", required = true)
    protected String msisdn;

    // PIN del Usuario de Canal
    @XmlElement(name = "PIN", required = true)
    protected String pin;

    // ID de inicio de sesión
    @XmlElement(name = "LOGINID")
    protected String loginid;

    // Contraseña
    @XmlElement(name = "PASSWORD")
    protected String password;

    // Código Externo del Usuario de Canal
    @XmlElement(name = "EXTCODE")
    protected String extcode;

    // Número de referencia externo
    @XmlElement(name = "EXTREFNUM")
    protected String extrefnum;

    // Numero MSISDN del Subscriptor Final
    @XmlElement(name = "MSISDN2")
    protected String msisdn2;

    // ID de Transacción
    @XmlElement(name = "TXNID")
    protected String txnid;

    @XmlElement(name = "AMOUNT")
    protected String amount;

    // Lenguaje del Usuario de Canal
    @XmlElement(name = "LANGUAGE1")
    protected String language1;

    //Lenguaje del Subscriptor Final
    @XmlElement(name = "LANGUAGE2")
    protected String language2;

    @XmlElement(name = "SELECTOR")
    protected String selector;

    // Código de Municipio
    @XmlElement(name = "INFO1")
    protected String info1;

    // Código de comercio
    @XmlElement(name = "INFO2")
    protected String info2;

    // Código Sucursal
    @XmlElement(name = "INFO3")
    protected String info3;

    // Código Punto de venta
    @XmlElement(name = "INFO4")
    protected String info4;


    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param type allowed object is
     *             {@link String }
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the type property.
     *
     * @param date allowed object is
     *             {@link Date }
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getExtnwcode() {
        return extnwcode;
    }

    /**
     * Sets the value of the type property.
     *
     * @param extnwcode allowed object is
     *                  {@link String }
     */
    public void setExtnwcode(String extnwcode) {
        this.extnwcode = extnwcode;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the type property.
     *
     * @param msisdn allowed object is
     *               {@link String }
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPin() {
        return pin;
    }

    /**
     * Sets the value of the type property.
     *
     * @param pin allowed object is
     *            {@link String }
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLoginid() {
        return loginid;
    }

    /**
     * Sets the value of the type property.
     *
     * @param loginid allowed object is
     *                {@link String }
     */
    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the type property.
     *
     * @param password allowed object is
     *                 {@link String }
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getExtcode() {
        return extcode;
    }

    /**
     * Sets the value of the type property.
     *
     * @param extcode allowed object is
     *                {@link String }
     */
    public void setExtcode(String extcode) {
        this.extcode = extcode;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getExtrefnum() {
        return extrefnum;
    }

    /**
     * Sets the value of the type property.
     *
     * @param extrefnum allowed object is
     *                  {@link String }
     */
    public void setExtrefnum(String extrefnum) {
        this.extrefnum = extrefnum;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsisdn2() {
        return msisdn2;
    }

    /**
     * Sets the value of the type property.
     *
     * @param msisdn2 allowed object is
     *                {@link String }
     */
    public void setMsisdn2(String msisdn2) {
        this.msisdn2 = msisdn2;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the value of the type property.
     *
     * @param amount allowed object is
     *               {@link String }
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLanguage1() {
        return language1;
    }

    /**
     * Sets the value of the type property.
     *
     * @param language1 allowed object is
     *                  {@link String }
     */
    public void setLanguage1(String language1) {
        this.language1 = language1;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLanguage2() {
        return language2;
    }

    /**
     * Sets the value of the type property.
     *
     * @param language2 allowed object is
     *                  {@link String }
     */
    public void setLanguage2(String language2) {
        this.language2 = language2;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSelector() {
        return selector;
    }

    /**
     * Sets the value of the type property.
     *
     * @param selector allowed object is
     *                 {@link String }
     */
    public void setSelector(String selector) {
        this.selector = selector;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * Sets the value of the type property.
     *
     * @param info1 allowed object is
     *              {@link String }
     */
    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * Sets the value of the type property.
     *
     * @param info2 allowed object is
     *              {@link String }
     */
    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * Sets the value of the type property.
     *
     * @param info3 allowed object is
     *              {@link String }
     */
    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * Sets the value of the type property.
     *
     * @param info4 allowed object is
     *              {@link String }
     */
    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }
}

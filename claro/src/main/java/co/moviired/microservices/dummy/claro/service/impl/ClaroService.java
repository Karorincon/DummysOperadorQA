package co.moviired.microservices.dummy.claro.service.impl;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.entities.Subscriber;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.conexion.repository.ISubscriberRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import co.moviired.microservices.dummy.claro.model.request.RequestClaro;
import co.moviired.microservices.dummy.claro.model.response.ResponseClaro;
import co.moviired.microservices.dummy.claro.service.IClaroService;
import co.moviired.microservices.dummy.claro.util.MethodEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;


@Service
public class ClaroService implements IClaroService{


    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private ISubscriberRepository subscriberRepository;
    @Autowired
    private IErrorCodeRepository errorCodeRepository;

    private final SimpleDateFormat simpleDateFormat;


    private static final Logger LOGGER = LogManager.getLogger(ClaroService.class.getName());


    @Autowired
    public ClaroService(){
        this.simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }


    /**
     * Método utilizado para validar las peticiones de los servicios
     *
     * @param requestClaro petición
     * @param methodEnum Método que se ejecuta
     *
     * @return
     */
    private ResponseClaro validateParams(RequestClaro requestClaro,MethodEnum methodEnum){
        ErrorTypeEnum errorTypeEnum=null;
        ResponseClaro responseClaro = new ResponseClaro();
        String date = this.simpleDateFormat.format(new Date());

        try{
            /**
             * Validamos que el campo cuente con la cantidad
             */
            if(requestClaro.getAmount()==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_AMOUNT);
            
            Double amount = new Double(requestClaro.getAmount());
            Product product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.CLARO.getId(),requestClaro.getSelector());
            
            if(product==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_PRODUCT);
            
            
            if (product.getSubscriberId()!=null) {
            	Subscriber subscriber = this.subscriberRepository
                        .findByThirdPartyIdAndMsisdn(ThirdPartyEnum.CLARO.getId(),requestClaro.getMsisdn());
            	
            	 if(subscriber==null || !subscriber.getId().equals(product.getSubscriberId()))
                     throw  new ErrorTypeException (ErrorTypeEnum.INVALID_ACCOUNT);
            	 
            	  if (subscriber.getTotalNet()< amount)
                      throw  new ErrorTypeException(ErrorTypeEnum.NO_CREDIT);
            	
            }


            if (amount<product.getMinValue())
                throw  new ErrorTypeException (ErrorTypeEnum.LOWER_MINOR_VALUE);

            if (amount > product.getMaxValue())
                throw  new ErrorTypeException (ErrorTypeEnum.GREATER_MAXIMUM_VALUE);


            if (requestClaro.getMsisdn2()==null || requestClaro.getMsisdn2().isEmpty())
                throw  new ErrorTypeException (ErrorTypeEnum.EMPTY_MSISDN);

            if (product.getRegex() != null && !product.getRegex().isEmpty()
                    && !Pattern.compile(product.getRegex()).matcher(requestClaro.getMsisdn2()).matches())
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_MSISDN);

          
        }catch (ErrorTypeException ex){
            LOGGER.error("ErrorTypeException: "+ex.getMessage());
            errorTypeEnum = ex.getErrorTypeEnum();

        }catch (Exception ex){
            LOGGER.error("Exception : "+ex.getMessage());
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;


        }finally {

            if (errorTypeEnum == null)
                errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;


            ErrorCode error = this.errorCodeRepository
                    .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.CLARO.getId(),
                                    errorTypeEnum.getId());


            responseClaro.setType(methodEnum.getResponse());
            requestClaro.setDate(date);
            responseClaro.setTxnstatus(error.getStatus());
            responseClaro.setMessage(error.getMessage());


            if (error.getErrorTypeId() == ErrorTypeEnum.SUCCESSFULL.getId()){
                responseClaro.setExtrefnum(requestClaro.getExtrefnum());
                responseClaro.setTxnid(this.getAutorizationNumber(methodEnum.getPrefix()));
            }

            return responseClaro;

        }
    }

    /**
     * Método que genera un número de autorizado
     * @param prefix
     * @return
     */
    private String getAutorizationNumber(String prefix){
        Calendar cal = Calendar.getInstance();
        String year = (cal.get(Calendar.YEAR)+"").substring(1);
        String month= (cal.get(Calendar.MONTH)+1)+"";
        if (month.length()<2)month= "0"+month;
        String day= cal.get(Calendar.DAY_OF_MONTH)+"";
        if (day.length()<2)day= "0"+day;


        String hour= cal.get(Calendar.HOUR_OF_DAY)+"";
        if (hour.length()<2)hour= "0"+hour;

        String minute= cal.get(Calendar.MINUTE)+"";
        if (minute.length()<2)minute= "0"+minute;

        Random ran = new Random();
        int x = ran.nextInt(999) + 999;

        String resp= prefix+year+month+day+"."
                +hour+minute+"."
                +x+"cf";

        return resp;
    }

    @Override
    public ResponseClaro processTransaction(RequestClaro requestClaro){
        return this.validateParams(requestClaro,MethodEnum.RECHARGE);
    }

    @Override
    public ResponseClaro processBuyPackage(RequestClaro requestClaro){
        return this.validateParams(requestClaro, MethodEnum.BUY_PACKAGE);
    }

    @Override
    public ResponseClaro processQuery(RequestClaro requestClaro){
        double index = Math.random()*1+1;
        String status="200";
        String message="El número de transacción R160412.0123.100003 para recargar $ 10000 al número " +
                "3204578956 fue exitosa.";

        if(index==0.0){
            status="206";
            message="Ultimo estado de la transferencia: R170405.0056.100001, hora y fecha de transferencia: " +
                    " 05/04/17 00:56:17, MSISDN: 3800000001, estado de transferencia: FALLIDO, tipo de servicio: " +
                    " Customer Recharge, producto: prepago, valor: 1000";
        }

        ResponseClaro responseClaro = new ResponseClaro();
        responseClaro.setType(MethodEnum.QUERY.getResponse());
        requestClaro.setDate(this.simpleDateFormat.format(new Date()));
        responseClaro.setExtrefnum(requestClaro.getExtrefnum());
        responseClaro.setTxnid(requestClaro.getTxnid());
        responseClaro.setTxnstatus(status);
        responseClaro.setReqstatus(status);
        responseClaro.setMessage(message);

        return responseClaro;
    }

}

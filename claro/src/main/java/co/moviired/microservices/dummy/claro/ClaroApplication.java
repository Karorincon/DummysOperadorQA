package co.moviired.microservices.dummy.claro;

import co.moviired.microservices.architecture.MicroservicesApplication;
import co.moviired.microservices.architecture.model.DataAccessMode;
import co.moviired.microservices.architecture.model.SecurityMode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author NUNEZ, Dallan
 * @version 1, 2018-07-03
 * @since 1.0
 */
@MicroservicesApplication(
        applicationName = "ClaroApplication",
        securityMode = SecurityMode.NONE,
        dataAccessMode = DataAccessMode.IN_MEMORY)
public class ClaroApplication extends SpringBootServletInitializer {


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ClaroApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ClaroApplication.class);
        app.addListeners(new ApplicationPidFileWriter("ClaroApplication.pid"));
        app.run(args);
    }

}

package co.moviired.microservices.dummy.claro.service;

import co.moviired.microservices.dummy.claro.model.request.RequestClaro;
import co.moviired.microservices.dummy.claro.model.response.ResponseClaro;

public interface IClaroService {

    ResponseClaro processTransaction(RequestClaro requestClaro);

    ResponseClaro processBuyPackage(RequestClaro requestClaro);

    ResponseClaro processQuery(RequestClaro requestClaro);
}

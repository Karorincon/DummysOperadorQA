package co.moviired.microservices.dummy.claro.util;

public enum  MethodEnum {
    RECHARGE            ("EXRCTRFREQ",  "EXRCTRFRESP",  "R"),
    BUY_PACKAGE         ("VASEXTRFREQ", "VASEXTRFRESP", "V"),
    QUERY               ("EXRCSTATREQ", "EXRCSTATRESP", "R");

    private final String request;
    private final String response;
    private final String prefix;

    private MethodEnum (String request, String response, String prefix){
        this.request = request;
        this.response = response;
        this.prefix = prefix;
    }

    public String getRequest() {
        return request;
    }

    public String getResponse() {
        return response;
    }

    public String getPrefix() {
        return prefix;
    }
}

package co.moviired.microservices.dummy.movistar;

import co.moviired.microservices.architecture.MicroservicesApplication;
import co.moviired.microservices.architecture.model.DataAccessMode;
import co.moviired.microservices.architecture.model.SecurityMode;
import co.moviired.microservices.dummy.movistar.util.LoggingOutputStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import java.io.PrintStream;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Ronel
 * @version 1, 2018-01-28
 * @since 1.0
 */
@MicroservicesApplication(
        applicationName = "MovistarApplication",
        securityMode = SecurityMode.NONE,
        dataAccessMode = DataAccessMode.IN_MEMORY)
@ComponentScan(basePackages = "co.moviired")
public class MovistarApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        System.setErr(new PrintStream(new LoggingOutputStream(LogManager.getLogger("file-log"), Level.ERROR)));
        System.setOut(new PrintStream(new LoggingOutputStream(LogManager.getLogger("file-log"), Level.INFO), false));
        SpringApplication app = new SpringApplication(MovistarApplication.class);
        app.addListeners(new ApplicationPidFileWriter("MovistarApplication.pid"));
        app.run(args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MovistarApplication.class);
    }

}




package co.moviired.microservices.dummy.movistar.service;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.conexion.repository.ISubscriberRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;


@Service
@EntityScan("co.moviired.microservices.architecture")
@EnableJpaRepositories("co.moviired.microservices.architecture")
public class MovistarService {

    private static final Logger LOGGER = LogManager.getLogger(MovistarService.class.getName());

    private static IProductRepository productRepository;
    private static ISubscriberRepository subscriberRepository;
    private static IErrorCodeRepository errorCodeRepository;

    private static String MOVISTAR_RECHARGE="1";

    @Autowired
    public MovistarService(IProductRepository productRepository,
                           ISubscriberRepository subscriberRepository,
                           IErrorCodeRepository errorCodeRepository) {
        this.productRepository = productRepository;
        this.subscriberRepository = subscriberRepository;
        this.errorCodeRepository = errorCodeRepository;
    }

    /**
     * Metodo que realiza las validaciones correspondientes a la trama enviada
     *
     * @param receivedISOmsg Mensaje tipo ISOMsg
     * @return Mensaje tipo ISOMsg
     * @throws ISOException
     */
    public static ISOMsg validateParams(ISOMsg receivedISOmsg) throws ISOException {
        ErrorTypeEnum errorTypeEnum = null;
        String resultCode = "";

        try {
            if (receivedISOmsg.hasFields(new int[]{0, 3, 4, 7, 11, 14, 17, 25, 32, 37, 41, 42, 43, 48, 49})) {
                LOGGER.info("entro acá en el if");
                // PROCESSING CODE
                String processingCode = receivedISOmsg.getString(3);

                // CANTIDAD
                String amountString = receivedISOmsg.getString(4);

                // TRANSMISSION DATE AND TIME
                String transmissionDate = receivedISOmsg.getString(7);

                // SYSTEM TRACE AUDIT NUMBER
                String controlNumber = receivedISOmsg.getString(11);

                if (controlNumber == null
                        || controlNumber.equals(""))
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_CONTROL_NUMBER);

                // DATE, EXPIRATION
                String expirationDate = receivedISOmsg.getString(14);

                // DATE, CAPTURE
                String captureDate = receivedISOmsg.getString(17);

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("MMdd");
                    Date fechaISO = formato.parse(captureDate);
                } catch (Exception e) {
                    throw new ErrorTypeException(ErrorTypeEnum.FAILED_TRANSACTION);
                }

                // POINT OF SERVICE CONDITION CODE
                String poingService = receivedISOmsg.getString(25);

                // ACQUIRING INSTITUTION IDENT CODE
                String institutionCode = receivedISOmsg.getString(32);

                // RETRIEVAL REFERENCE NUMBER
                String retrievalNumber = receivedISOmsg.getString(37);

                // CARD ACCEPTOR TERMINAL IDENTIFICACION
                String cardAcceptorTerminal = receivedISOmsg.getString(41);

                // CARD ACCEPTOR IDENTIFICATION CODE
                String cardAcceptorIdentification = receivedISOmsg.getString(42);

                // ADITIONAL DATA - PRIVATE
                String additionalData = receivedISOmsg.getString(48);

                // CURRENCY CODE, TRANSACTION
                String currencyCode = receivedISOmsg.getString(49);

                // CARD ACCEPTOR NAME/LOCATION
                String numCel = String.valueOf(Long.parseLong(receivedISOmsg.getString(43)));

                Product product = productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.MOVISTAR.getId(),MOVISTAR_RECHARGE);

                if (product == null)
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_PRODUCT);

                /**
                 * Se le quitan los 2 ceros al final
                 */
                int value = amountString.length()-2;
                LOGGER.info("antes de quitar los dos ceros=> "+amountString);
                amountString = amountString.substring(0,value);
                LOGGER.info("despues de quitar los dos ceros=> "+amountString);
                
                Double amount = new Double(amountString);

                if (amount < product.getMinValue())
                    throw new ErrorTypeException(ErrorTypeEnum.LOWER_MINOR_VALUE);

                else if (amount > product.getMaxValue())
                    throw new ErrorTypeException(ErrorTypeEnum.GREATER_MAXIMUM_VALUE);

                else if (product.getRegex() != null && !product.getRegex().isEmpty()
                        && !Pattern.compile(product.getRegex()).matcher(numCel).matches())
                    throw new ErrorTypeException(ErrorTypeEnum.INVALID_MSISDN);


            } else if (!receivedISOmsg.hasFields(new int[]{0, 7, 11, 32, 70})) {
                LOGGER.info("entro acá en el else");
                ISOMsg reqMsg = new ISOMsg();
                reqMsg.setMTI("0800");
                throw new ErrorTypeException(ErrorTypeEnum.FAILED_TRANSACTION);
            }

        } catch (ErrorTypeException ex) {
            LOGGER.error("ErrorTypeException: " + ex.getMessage());
            LOGGER.error(ex);
            errorTypeEnum = ex.getErrorTypeEnum();

        } catch (Exception ex) {
            LOGGER.error("Exception : " + ex.getMessage());
            LOGGER.error(ex);
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;

        } finally {

            if (errorTypeEnum == null)
                errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;

            ErrorCode error = errorCodeRepository
                    .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.MOVISTAR.getId(),
                            errorTypeEnum.getId());

            resultCode = error.getCode();
            receivedISOmsg.set(39, resultCode);
            receivedISOmsg.set(38, getAutorizationNumber());
            receivedISOmsg.setResponseMTI();
        }

        return receivedISOmsg;
    }

    /**
     * Método que genera un número de autorizado
     * @return
     */
    private static String getAutorizationNumber(){
        Random ran = new Random();
        int x = ran.nextInt(999) + 9999;
        return x+"";
    }
}

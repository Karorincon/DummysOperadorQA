package co.moviired.microservices.dummy.movistar.server;

import co.moviired.microservices.dummy.movistar.service.MovistarService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.q2.Q2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Clase que permite la creacion del servicio ISO para enviar recibir y procesar mensajes
 * tipo ISO 8583
 */
@Component
public class ISOserver implements ISORequestListener {

    @Value("${configuration_external_path}")
    private String configuration_external_path;

    private static final Logger LOGGER = LogManager.getLogger(ISOserver.class.getName());

    /**
     * Metodo que permite el inicio del servidor q2 para recibir mensajes ISO 8583
     *
     * @throws Exception
     */
    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() throws Exception {
        Q2 q2 = new Q2(configuration_external_path + "/deploy");
        q2.start();
    }

    /**
     * Metodo que captura la trama que contiene el mensaje tipo ISO 8583
     *
     * @param isoSource
     * @param receivedISOmsg
     * @return
     */
    @Override
    public boolean process(ISOSource isoSource, ISOMsg receivedISOmsg) {
        try {
            isoSource.send(MovistarService.validateParams(receivedISOmsg));
            LOGGER.info("------------receivedISOmsg------------");
            return true;
        } catch (ISOException e) {
            e.printStackTrace();
            LOGGER.info("error1 ", e);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.info("error2 ", e);
        }
        return false;
    }

}
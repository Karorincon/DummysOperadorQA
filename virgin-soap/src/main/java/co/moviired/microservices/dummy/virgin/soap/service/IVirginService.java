package co.moviired.microservices.dummy.virgin.soap.service;

import com.inew_cs.mvno.integration.topupservice.QueryTopupRequestType;
import com.inew_cs.mvno.integration.topupservice.QueryTopupResponseType;
import com.inew_cs.mvno.integration.topupservice.TopupRequestType;
import com.inew_cs.mvno.integration.topupservice.TopupResponseType;

public interface IVirginService {

    TopupResponseType topup (TopupRequestType request);

    QueryTopupResponseType query (QueryTopupRequestType request);

}

package co.moviired.microservices.dummy.virgin.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;

import java.io.File;

@EnableWs
@Configuration
@PropertySource(value = "file:${configuration_external_path}/participant.properties", ignoreResourceNotFound = true)
public class WebServiceConfig extends WsConfigurerAdapter {

    @Value("${configuration_external_path}")
    private String configurationExternalPath;

    @Value("${SERVICE_NAME}")
    private String contextPath;

    private static final String TARGET_NAMESPACE ="http://www.inew-cs.com/mvno/integration/TopupService/";
    private static final Logger LOGGER = LogManager.getLogger(WebServiceConfig.class.getName());


    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/"+contextPath+"/*");
    }
    
    @Bean(name = "TopupServiceHttp_8005Soap11Endpoint")
    public SimpleWsdl11Definition simpleWsdl11Definition() {
    	 SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
         File f= new File(this.configurationExternalPath+"/wsdl/virgin_wsdl.xml");
         wsdl11Definition.setWsdl(new FileSystemResource(f));
         return wsdl11Definition;
    }


}
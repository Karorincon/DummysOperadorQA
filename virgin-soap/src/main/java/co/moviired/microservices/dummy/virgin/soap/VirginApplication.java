package co.moviired.microservices.dummy.virgin.soap;

import co.moviired.microservices.architecture.MicroservicesApplication;
import co.moviired.microservices.architecture.model.DataAccessMode;
import co.moviired.microservices.architecture.model.SecurityMode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.support.SpringBootServletInitializer;
/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author NUNEZ, Dallan
 * @version 1, 2018-07-03
 * @since 1.0
 */
@MicroservicesApplication(
        applicationName = "VirginApplication",
        securityMode = SecurityMode.NONE,
        dataAccessMode = DataAccessMode.IN_MEMORY)
public class VirginApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(VirginApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(VirginApplication.class);
        app.addListeners(new ApplicationPidFileWriter("VirginApplication.pid"));
        app.run(args);
    }
}

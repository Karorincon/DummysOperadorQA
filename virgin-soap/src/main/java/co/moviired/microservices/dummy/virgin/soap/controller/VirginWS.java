package co.moviired.microservices.dummy.virgin.soap.controller;

import co.moviired.microservices.dummy.virgin.soap.service.IVirginService;
import com.inew_cs.mvno.integration.topupservice.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class VirginWS {

    private static final String TARGET_NAMESPACE ="http://www.inew-cs.com/mvno/integration/TopupService/";
    private static final Logger LOGGER = LogManager.getLogger(VirginWS.class.getName());
    private final IVirginService virginService;

    @Autowired
    public VirginWS(IVirginService virginService){
    	LOGGER.info("iniciandooooo    VirginWS");
        this.virginService = virginService;
    }

    @PayloadRoot(namespace = TARGET_NAMESPACE, localPart = "TopupRequest")
    @ResponsePayload()
    public JAXBElement<TopupResponseType> topup(@RequestPayload JAXBElement<TopupRequestType> request){
    	try {
    		LOGGER.info("request=> "+request);
        	TopupResponseType type= this.virginService.topup(request.getValue());
        	LOGGER.info("response=> "+type);
        	
        	LOGGER.info("escribiendo una respuesta: "+type); //TopupResponse
        	
        	JAXBElement<TopupResponseType> jaxbElement =  new JAXBElement( 
                    new QName(TopupResponseType.class.getSimpleName()), TopupResponseType.class, type);
        	
        	LOGGER.info("jaxbElement: "+jaxbElement);
        	return jaxbElement;
        	
    	}catch (Exception e) {
    		LOGGER.error("--------------este es un error-------------");
			LOGGER.error(e);
		}
    	return null;
    	
    
    }

    @PayloadRoot(namespace = TARGET_NAMESPACE, localPart = "QueryTopupRequest")
    @ResponsePayload
    public JAXBElement<QueryTopupResponseType> query(@RequestPayload JAXBElement<QueryTopupRequestType> request){
    	QueryTopupResponseType type= this.virginService.query(request.getValue());
        
    	
        JAXBElement<QueryTopupResponseType> jaxbElement =  new JAXBElement( 
                new QName(QueryTopupResponseType.class.getSimpleName()), QueryTopupResponseType.class, type);
    	
    	return jaxbElement;
    }



}

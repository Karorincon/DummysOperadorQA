package co.moviired.microservices.dummy.virgin.soap.service.impl;

import co.moviired.microservices.dummy.virgin.soap.service.IVirginService;
import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import com.inew_cs.mvno.integration.topupservice.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Service
public class VirginService implements IVirginService {

    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IErrorCodeRepository errorCodeRepository;


    private static final Logger LOGGER = LogManager.getLogger(VirginService.class.getName());


    public TopupResponseType topup(TopupRequestType request) {
        ErrorTypeEnum errorTypeEnum=null;
        Product product=null;
        try{

            /**
             * Validamos que el campo cuente con la cantidad
             */
        	LOGGER.info("SUSCRIPTOR "+request.getSubscriber());
            if(request.getAmount()==0)
                throw  new ErrorTypeException(ErrorTypeEnum.INVALID_AMOUNT);

            product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.VIRGIN_SOAP.getId(),"1");
           

            if(product==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_PRODUCT);
            
            if(product.getConnectTimeout()!=null)
            	this.delayMethod(product.getConnectTimeout());
            

            if (request.getAmount()<product.getMinValue())
                throw  new ErrorTypeException (ErrorTypeEnum.LOWER_MINOR_VALUE);

            if (request.getAmount() > product.getMaxValue())
                throw  new ErrorTypeException (ErrorTypeEnum.GREATER_MAXIMUM_VALUE);

            if (request.getSubscriber()==null ||
                    request.getSubscriber().getMsisdn()==null ||
                    request.getSubscriber().getMsisdn().getValue()==null ||
                    request.getSubscriber().getMsisdn().getValue().isEmpty())
                throw  new ErrorTypeException (ErrorTypeEnum.EMPTY_MSISDN);

            if (product.getRegex() != null && !product.getRegex().isEmpty()
                    && !Pattern.compile(product.getRegex()).matcher(request.getSubscriber().getMsisdn().getValue()).matches())
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_MSISDN);


        }catch (ErrorTypeException ex){
            LOGGER.error("ErrorTypeException: "+ex.getMessage());
            errorTypeEnum = ex.getErrorTypeEnum();

        }catch (Exception ex){
            LOGGER.error("Exception : "+ex.getMessage());
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;


        }finally {

            if (errorTypeEnum == null)
                errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;


            ErrorCode error = this.errorCodeRepository
                    .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.VIRGIN_SOAP.getId(),
                            errorTypeEnum.getId());
            
            ObjectFactory factory = new ObjectFactory();
            TopupResponseType responseType = factory.createTopupResponseType();
            responseType.setTransactionId(this.getAutorizationNumber());
            responseType.setSubscriber(request.getSubscriber());
            responseType.setReferenceId(request.getReferenceId());
            responseType.setTransactionDate(this.getCurrentDate());
            responseType.setResponseCode(error!=null && error.getCode()!=null && !error.getCode().isEmpty() ?  new BigInteger(error.getCode()) : new BigInteger("99"));
            responseType.setResponseDetail(error!=null ? error.getMessage():"TRANSACCIÓN FALLIDA");
            
            
            
            if(product!=null && product.getRequestTimeout()!=null)
            	this.delayMethod(product.getRequestTimeout());
	            
            

            return responseType;
        }
    }
    
    /**
     * Método utilizado para retardar un método
     * @param time0
     */
    private void delayMethod(Integer time0) {
    	 try {
         	TimeUnit.MILLISECONDS.sleep(time0/1000);
         }catch (Exception e) {
				LOGGER.error("error con el tiempo de retardo....");
				LOGGER.error(e);
		}
    }

    @Override
    public QueryTopupResponseType query(QueryTopupRequestType request) {
        ErrorCode error = this.errorCodeRepository
                .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.VIRGIN_SOAP.getId(),
                        ErrorTypeEnum.SUCCESSFULL.getId());

        ObjectFactory factory = new ObjectFactory();
        QueryTopupResponseType responseType = factory.createQueryTopupResponseType();
        responseType.setTransactionDate(this.getCurrentDate());
        responseType.setResponseCode(new BigInteger(error.getCode()));
        responseType.setResponseDetail(error.getMessage());
        responseType.setTransactionId(this.getAutorizationNumber());

        return responseType;

    }

    private XMLGregorianCalendar getCurrentDate(){
        try{
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(new Date());
            XMLGregorianCalendar xml = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            xml.setYear(gc.get(Calendar.YEAR));
            xml.setMonth(gc.get(Calendar.MONTH) + 1);
            xml.setDay(gc.get(Calendar.DAY_OF_MONTH));
            xml.setHour(gc.get(Calendar.HOUR_OF_DAY));
            xml.setMinute(gc.get(Calendar.MINUTE));
            xml.setSecond(gc.get(Calendar.SECOND));

            return xml;

        }catch (Exception ex){
            LOGGER.error("Ha ocurrido un error calculado la fecha: ", ex);
        }

        return null;
    }


    /**
     * Método que genera un número de autorizado
     * @return
     */
    private String getAutorizationNumber(){
        Calendar cal = Calendar.getInstance();
        String year = (cal.get(Calendar.YEAR)+"").substring(1);
        String month= (cal.get(Calendar.MONTH)+1)+"";
        if (month.length()<2)month= "0"+month;
        String day= cal.get(Calendar.DAY_OF_MONTH)+"";
        if (day.length()<2)day= "0"+day;


        String hour= cal.get(Calendar.HOUR_OF_DAY)+"";
        if (hour.length()<2)hour= "0"+hour;

        String minute= cal.get(Calendar.MINUTE)+"";
        if (minute.length()<2)minute= "0"+minute;

        Random ran = new Random();
        int x = ran.nextInt(999) + 999;

        String resp= year+month+day
                +hour+minute
                +x;

        return resp;
    }

}

package co.moviired.microservices.dummy.virgin.rest.service;

import java.util.Map;

public interface IVirginService {

    Map<String, Object> activateBundle (String msisdn, String bundleCode);

    Map<String, Object> activatePlan (String msisdn, String tariffPlanId);

}

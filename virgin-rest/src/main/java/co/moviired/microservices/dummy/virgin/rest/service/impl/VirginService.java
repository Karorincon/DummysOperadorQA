package co.moviired.microservices.dummy.virgin.rest.service.impl;

import co.moviired.microservices.architecture.conexion.entities.ErrorCode;
import co.moviired.microservices.architecture.conexion.entities.Product;
import co.moviired.microservices.architecture.conexion.repository.IErrorCodeRepository;
import co.moviired.microservices.architecture.conexion.repository.IProductRepository;
import co.moviired.microservices.architecture.exception.ErrorTypeException;
import co.moviired.microservices.architecture.util.ErrorTypeEnum;
import co.moviired.microservices.architecture.util.ThirdPartyEnum;
import co.moviired.microservices.dummy.virgin.rest.service.IVirginService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Service
public class VirginService implements IVirginService {

    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IErrorCodeRepository errorCodeRepository;


    private static final Logger LOGGER = LogManager.getLogger(VirginService.class.getName());


    public Map<String, Object> activateBundle (String msisdn, String bundleCode){
        return this.validateParams(msisdn, bundleCode);
    }

    public Map<String, Object> activatePlan (String msisdn, String tariffPlanId){
        return this.validateParams(msisdn, tariffPlanId);
    }

    private Map<String, Object> validateParams(String msisdn, String packageCode){
        ErrorTypeEnum errorTypeEnum=null;
        Product product=null;
        try{

            product = this.productRepository.findByThirdPartyIdAndCode(ThirdPartyEnum.VIRGIN.getId(),packageCode);

            if(product==null)
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_PRODUCT);
            
            if(product.getConnectTimeout()!=null)
            	this.delayMethod(product.getConnectTimeout());

            if (msisdn==null || msisdn.isEmpty())
                throw  new ErrorTypeException (ErrorTypeEnum.EMPTY_MSISDN);

            if (product.getRegex() != null && !product.getRegex().isEmpty()
                    && !Pattern.compile(product.getRegex()).matcher(msisdn).matches())
                throw  new ErrorTypeException (ErrorTypeEnum.INVALID_MSISDN);


        }catch (ErrorTypeException ex){
            LOGGER.error("ErrorTypeException: "+ex.getMessage());
            errorTypeEnum = ex.getErrorTypeEnum();

        }catch (Exception ex){
            LOGGER.error("Exception : "+ex.getMessage());
            errorTypeEnum = ErrorTypeEnum.FAILED_TRANSACTION;


        }finally {

            if (errorTypeEnum == null)
                errorTypeEnum = ErrorTypeEnum.SUCCESSFULL;


            ErrorCode error = this.errorCodeRepository
                    .findByThirdPartyIdAndErrorTypeId(ThirdPartyEnum.VIRGIN.getId(),
                            errorTypeEnum.getId());


            Map<String, Object> map = new HashMap<String, Object>();
            map.put("correlationId","1");
            if(errorTypeEnum == ErrorTypeEnum.SUCCESSFULL){
                Map<String, String> map2 = new HashMap<String, String>();
                map2.put("amount","0");
                map2.put("unitRelation","0");
                map.put("payload",map2);
            }

            map.put("message",error.getCode()+" - "+error.getMessage());
            map.put("responseCode", new Double(error.getCode()));
            map.put("responseDetail", error.getMessage());
            map.put("transactionId", "000000000");
            
            if(product!=null && product.getRequestTimeout()!=null)
            	this.delayMethod(product.getRequestTimeout());

            return map;
        }
    }
    
    /**
     * Método utilizado para retardar un método
     * @param time0
     */
    private void delayMethod(Integer time0) {
    	 try {
         	TimeUnit.MILLISECONDS.sleep(time0/1000);
         }catch (Exception e) {
				LOGGER.error("error con el tiempo de retardo....");
				LOGGER.error(e);
		}
    }

}

package co.moviired.microservices.dummy.virgin.rest.controller;

import co.moviired.microservices.architecture.base.BaseWS;
import co.moviired.microservices.architecture.model.enums.ErrorType;
import co.moviired.microservices.architecture.model.output.ErrorDetail;
import co.moviired.microservices.architecture.model.output.Response;
import co.moviired.microservices.architecture.model.output.Result;
import co.moviired.microservices.dummy.virgin.rest.service.IVirginService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin
@RestController("virginWS")
public class VirginWS  extends BaseWS {

    private static final Logger LOGGER = LogManager.getLogger(VirginWS.class.getName());

    private final IVirginService virginService;

    @Autowired
    public VirginWS(IVirginService virginService){
        this.virginService = virginService;
    }

    // Método: PING
    @RequestMapping(value = "${spring.application.methods.ping}", method = RequestMethod.GET)
    public Response ping() {
        Result result;

        try {
            LOGGER.info("**** Ping CLARO: Iniciado ****");
            LOGGER.info("loogogg " );

            // Establecer la respuesta exitosa
            result = new Result(HttpStatus.OK, null);
            result.setError(null);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            // Capturar cualquier no esperado
            result = new Result(HttpStatus.INTERNAL_SERVER_ERROR, new ErrorDetail(ErrorType.PROCESSING, "-1", e.getMessage()));
        }
        LOGGER.info("**** Ping CLARO: Finalizado ****");

        // Devolver la respuesta
        return new Response(result, null);
    }


    // COMPRA DE BOLSA CON TRANSACTION-ID
    @PostMapping("/{msisdn}/availableBundles/{bundleCode}/activate")
    @PreAuthorize("@permissions.allowAny('request', 'execute')")
    public Map<String, Object> activateBundle(@PathVariable("msisdn") String msisdn,
                                 @PathVariable("bundleCode") String bundleCode,
                                 @RequestBody Map body
                                 ) {


        LOGGER.info("msisdn "+msisdn);
        LOGGER.info("bundleCode "+bundleCode);
        LOGGER.info("body "+body);

        return this.virginService.activateBundle(msisdn, bundleCode);
    }


    // COMPRA DE ANTIPLAN CON TRANSACTION-ID
    @PostMapping("/{msisdn}/availablePlans/{tariffPlanId}/activate")
    @PreAuthorize("@permissions.allowAny('request', 'execute')")
    public Map<String, Object> activatePlan(@PathVariable("msisdn") String msisdn,
                                 @PathVariable("tariffPlanId") String tariffPlanId,
                                 @RequestBody Map body
    ) {


        LOGGER.info("msisdn "+msisdn);
        LOGGER.info("tariffPlanId "+tariffPlanId);
        LOGGER.info("body "+body);

        return this.virginService.activatePlan(msisdn, tariffPlanId);


    }



}

PARA REALIZAR LA CONFIGURACION DEL SERVICIO ES NECESARIO REALIZAR LO SIGUIENTE
1.- COPIAR EL ARCHIVO service-dummy-virgin-rest QUE SE ENCUENTRA EN LA CARPETA conf EN LA RUTA DE LINUX /etc/init.d

2.- ABRIR EL ARCHIVO microservice-virgin Y MODIFICAR LAS SIGUIENTES VARIABLES
    SCRIPT_PATH: RUTA DE LA CARPETA PATH_DESCOMPRESION/bin EN EL SISTEMA OPERATIVO

3.- ABRIR EL ARCHIVO service-dummy-virgin-rest.sh QUE SE ENCUENTRA EN LA CARPETA bin DEL ARCHIVO DESCOMPRIMIDO

4.- MODIFICAR LAS SIGUIENTES VARIABLES
    SYSTEM_PATH: RUTA DE LA CARPETA service-dummy-virgin-rest DESCOMPRIMIDA
    JAVA_HOME: RUTA DE LA INSTALACION DE JAVA EN EL SISTEMA OPERATIVO

5.- EJECUTAR LOS SIGUIENTES COMANDOS (CON USUARIO root)
        chmod +x /etc/init.d/service-dummy-virgin-rest
        chkconfig --add service-dummy-virgin-rest
        systemctl daemon-reload

COMANDOS:
# SUBIR EL SERVICIO
service service-dummy-virgin-rest start

# VALIDAR ESTADO DEL SERVICIO
service service-dummy-virgin-rest status

# DETENER SERVICIO
service service-dummy-virgin-rest stop

